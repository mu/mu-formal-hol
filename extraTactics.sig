open HolKernel bossLib

signature extraTactics =
sig
  val pair_case_tac : tactic

  val multi_case_tac : tactic

  val rw_assums : thm list -> tactic

  val rw_assums_once : thm list -> tactic
end

