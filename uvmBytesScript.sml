open HolKernel Parse boolLib bossLib;

open arithmeticTheory
open optionTheory
open sptreeTheory
open wordsTheory
open uvmTypesTheory

val _ = new_theory "uvmBytes"

(* This file is a dumping ground for incomplete binary-representation-related
   code. A lot of the code here isn't useful for anything yet.
*)

val _ = Datatype`
  machine = <|
    atomic_sizes: num set;
    min_int_bytes: num;
    pointer_bytes: num;
    float_bits: num;
    double_bits: num;
    num_to_bytes: num -> num -> word8 list;
    bytes_to_num: word8 list -> num;
    alignment: num -> num -> num
  |>
`

val num_to_bytes_ok_def = Define`
  num_to_bytes_ok (n2b : num -> num -> word8 list)
                  (b2n : word8 list -> num) ⇔
    (∀sz b. n2b sz (b2n b) = b)
  ∧ (∀sz n. b2n (n2b sz n) = n)
  ∧ (∀sz n. sz ≤ LENGTH (n2b sz n))
  ∧ (∀sz n1 n2. n1 ≠ n2 ⇒ n2b sz n1 ≠ n2b sz n2)
  ∧ (∀sz n1 n2. n1 < n2 ⇒ LENGTH (n2b sz n1) ≤ LENGTH (n2b sz n2))
`

val machine_ok_def = Define`
  machine_ok m ⇔
    m.atomic_sizes ≠ ∅
  ∧ 0 < m.min_int_bytes
  ∧ m.min_int_bytes ≤ m.pointer_bytes
  ∧ m.min_int_bytes * 8 ≤ m.float_bits
  ∧ m.float_bits ≤ m.double_bits
  ∧ num_to_bytes_ok m.num_to_bytes m.bytes_to_num
`

(*
val align_cells_def = Define`
  align_cells (alignment : num -> num -> num) (start : num) (szs : num list) =
    FOLDL (λ(n, ns) sz. let n' = n + alignment n sz in (n' + sz, SNOC ns n'))
          (start, []) szs
`
*)

(*
val ceil_div_def = Define`
  ceil_div num den = num DIV den + if num MOD den = 0 then 0 else 1
`

val ceil_div_0 = store_thm("ceil_div_0",
  ``∀n d. 0 < d ∧ ceil_div n d = 0 ⇒ n = 0``,
  rw[ceil_div_def] >> rfs[DIV_EQ_X] >> fs[MOD_EQ_0_DIVISOR] >>
  rw[] >> Cases_on `d'` >> fs[])

val ceil_div_le = store_thm("ceil_div_le",
  ``∀n d. 0 < d ⇒ ceil_div n d ≤ n``,
  rw[ceil_div_def, DIV_LESS_EQ] >>
  `n DIV d < n` suffices_by simp[] >> match_mp_tac DIV_LESS >>
  `n ≠ 0 ∧ d ≠ 0 ∧ d ≠ 1` suffices_by simp[] >>
  `0 MOD d = 0` by simp[] >> fs[] >> metis_tac[MOD_1])
*)

val size_of_type_def = Define`
    size_of_type _ Void = 0
  ∧ size_of_type _ TagRef64 = 8
  ∧ size_of_type m (Int n) =
      LENGTH (m.num_to_bytes m.min_int_bytes (2 ** n - 1))
  ∧ size_of_type m Float = (m.float_bits + 7) DIV 8
  ∧ size_of_type m Double = (m.double_bits + 7) DIV 8
  ∧ size_of_type _ (Array _ _) = 0
  ∧ size_of_type _ (Vector _ _) = 0
  ∧ size_of_type _ (Hybrid _ _) = 0
  ∧ size_of_type _ (Struct _ _) = 0
  ∧ size_of_type m _ = m.pointer_bytes
`

(* canconvert M convtype (src : uvm_type) (tgt : uvm_type) *)

val (canconvert_rules, canconvert_ind, canconvert_cases) = Hol_reln`

    (∀m n. m ≤ n ⇒ canconvert M TRUNC (Int n) (Int m))
  ∧ (∀m n. m ≤ n ⇒ canconvert M ZEXT (Int m) (Int n))
  ∧ (∀m n. m ≤ n ⇒ canconvert M SEXT (Int m) (Int n))
  ∧ canconvert M FPTRUNC Double Float
  ∧ canconvert M FPEXT Float Double
  ∧ (∀ty n. fpType ty ⇒ canconvert M FPTOUI ty (Int n))
  ∧ (∀ty n. fpType ty ⇒ canconvert M FPTOSI ty (Int n))
  ∧ (∀ty n. fpType ty ⇒ canconvert M UITOFP (Int n) ty)
  ∧ (∀ty n. fpType ty ⇒ canconvert M SITOFP (Int n) ty)
  ∧ canconvert M BITCAST (Int M.float_bits) Float
  ∧ canconvert M BITCAST Float (Int M.float_bits)
  ∧ canconvert M BITCAST (Int M.double_bits) Double
  ∧ canconvert M BITCAST Double (Int M.double_bits)

  (* ptrcast cases *)
  ∧ (∀n ty.
      M.pointer_bytes = n * 8
    ⇒ canconvert M BITCAST (Int n) (IRef PTR ty))
  ∧ (∀n ty.
      M.pointer_bytes = n * 8 (* tell KW *)
    ⇒ canconvert M BITCAST (IRef PTR ty) (Int n))

  (* refcast cases *)
  ∧ (∀ty. canconvert M REFCAST (Ref (Type Void)) (Ref ty))
  ∧ (∀ty. canconvert M REFCAST (Ref ty) (Ref (Type Void)))
  ∧ (∀ty1 ty2. prefix_of ty1 ty2
             ⇒ canconvert M REFCAST (Ref (Type ty1)) (Ref (Type ty2)))
  (*
  ∧ (∀fx1 fx2 tys1 tys2.
      (* FIXME: Equivalence is MUCH harder than this w.r.t. fixpoints *)
      (tys1 <<= tys2 ∨ tys2 <<= tys1)
    ⇒ canconvert M REFCAST (Ref (Type (Struct fx1 tys1)))
                           (Ref (Type (Struct fx2 tys2))))
  *)
`

val _ = type_abbrev("bytes", ``:num # word8 spt``)

val new_bytes_def = Define `new_bytes : bytes = (0, LN)`

val append_bytes_def = Define`
  append_bytes : bytes -> word8 option list -> bytes =
    FOLDL (λ(n, t) b. (SUC n, case b of SOME b => insert n b t | NONE => t))
`

val read_bytes_def = Define`
  read_bytes ((_, t) : bytes) (start : num) (len : num) : word8 list option =
    FOLDR (OPTION_MAP2 CONS) (SOME []) (GENLIST (λn. lookup (start + n) t) len)
`

val write_bytes_def = Define`
  write_bytes ((n, t) : bytes) (start : num) (bs : word8 option list) : bytes =
    let t' = FOLDL (λt b. case b of SOME b => insert n b t | NONE => t) t bs in
    (MAX n (start + LENGTH bs), t')
`

val append_aligned_bytes_def = Define`
  append_aligned_bytes (*           start    len    offset *)
                       (alignment : num   -> num -> num     )
                       (old_bytes : bytes)
                       (new_bytes : word8 list)
                       : bytes # num =
    let start = FST old_bytes in
    let offset = alignment start (LENGTH new_bytes) in
    let padding = GENLIST (K NONE) offset in
    (append_bytes old_bytes (padding ++ MAP SOME new_bytes), start + offset)
`

(*
val pack_int_def = Define`
  pack_int m sz n =
    let bits = m.max_atomic_bytes * 8 in (
    members_of_type m NONE (Int sz) :> LENGTH :>
    GENLIST (IntV (MIN sz bits) o n2w o $DIV n o $** 2 o $* bits) :>
    if m.little_endian then I else REVERSE)
`

val unpack_int_def = Define`
  unpack_int m words =
    let words = if m.little_endian then words else REVERSE words in
    let ext word (n, e) =
      case word of
      | IntV sz w => return (n + w2n w * (2 ** e), e + sz)
      | _ => Fail (UndefinedBehavior "not an int") in
    FST <$> FOLDL (λs w. s >>= ext w) (return (0, 0)) words
`
*)

val _ = export_theory()

