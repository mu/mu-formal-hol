structure EvalUtils :> EvalUtils =
struct
  open HolKernel Parse Term boolLib bossLib
  open listTheory llistTheory optionTheory pred_setTheory finite_mapTheory sptreeTheory
  open listSyntax stringSyntax sptreeSyntax
  open monadsTheory uvmTypesTheory uvmErrorsTheory uvmMemoryTheory

  val lhdtl_lunfold = prove(
    ``LHD (LUNFOLD f x) = OPTION_MAP SND (f x) ∧
      LTL (LUNFOLD f x) = OPTION_MAP (LUNFOLD f o FST) (f x)``,
    rw[] >> rw[Once LUNFOLD, LHDTL_CONS_THM] >>
    Cases_on `f x` >> simp[OPTION_MAP_DEF] >> Cases_on `x'` >> simp[])

  fun add_extra_eval_thms() : unit = (
    sptreeSyntax.temp_add_sptree_printer();
    computeLib.add_funs[
      SING, CHOICE_SING, EMPTY_DEF, IN_APP, INSERT_applied,
      OPTION_MAP_DEF, OPTION_MAP2_DEF, OPTION_BIND_def, OPTION_CHOICE_def, OPTION_GUARD_def,
      lhdtl_lunfold, prefix_type_cases,
      bind_left_monad_left_id, bind_left_monad_right_id, bind_left_monad_assoc]
      )

  fun parens q = [QUOTE "("] @ q @ [QUOTE ")"]

  fun to_env (bundle : term) : term =
    EVAL ``lift_type_error (^bundle empty_env)`` |> concl |> rhs

  fun start_at (func_name : string) (args : term frag list) (env : term) : term =
    Term(`
      ^env >>= λenv.
      new_sched_state env (Func ^(fromMLstring func_name)) >>=
      start_with_args ` @ parens args) |> EVAL |> concl |> rhs

  fun run_schedule (schedule : term frag list) (state : term) : term =
    Term(`^state >>= run_schedule` @ parens schedule) |> EVAL |> concl |> rhs

  fun run_finite_schedule (schedule : term frag list) (state : term) : term =
    Term(`^state >>= run_finite_schedule` @ parens schedule) |> EVAL |> concl |> rhs

  fun expect_result (expected : term frag list) (state : term) : thm =
    let val actual = EVAL ``^state >>= get_return_values`` |> concl |> rhs
    in prove(Term(parens expected @ ` = ^actual`), simp[]) end

  fun expect_global_value (name : string) (value : term frag list) (state : term) : thm =
    let val actual = EVAL ``^state >>= get_global_value ^(fromMLstring name)`` |> concl |> rhs
    in prove(Term(parens value @ ` = ^actual`), simp[]) end

  fun take_step (step : term frag list) (state : term) : term =
    Term(`^state >>= take_step` @ parens step) |> EVAL |> concl |> rhs

  fun take_steps (steps : term frag list) (state : term) : term =
    Term(`^state >>= take_steps` @ parens steps) |> EVAL |> concl |> rhs
end

