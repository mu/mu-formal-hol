open HolKernel Parse boolLib bossLib

open uvmValuesTheory
open uvmErrorsTheory
open monadsTheory
open monadsLib
open syntaxSugarLib
open syntaxSugarTheory

open alistTheory
open listTheory
open llistTheory
open numpairTheory
open optionTheory
open pairTheory
open pred_setTheory
open sptreeTheory

open extraTactics

val _ = new_theory "uvmMemory"
val _ = monadsyntax.add_monadsyntax()
val _ = reveal "C"

val _ = define_num_newtype("thread_id", "ThreadID", "t")
val _ = define_num_newtype("frame_id",  "FrameID",  "f")
val _ = define_num_newtype("stack_id",  "StackID",  "s")
(*val _ = define_num_newtype("action_id", "ActionID", "a")*)

val _ = Datatype `ssavar = SSA string`
val _ = overload_on("%", ``SSA``)

val _ = Datatype `func_name = Func string`
val _ = Datatype `func_version = FuncVersion func_name num`
val _ = Datatype `alloc = Global | Heap | Stack stack_id frame_id`

val _ = type_abbrev("addr", ``:alloc # num``)
val _ = type_abbrev("value", ``:(addr, func_name, stack_id, thread_id) gen_value``)
val _ = type_abbrev("iaddr", ``:addr gen_iaddr``)

val alloc_lt_def = Define`
    (alloc_lt Global Heap ⇔ T)
  ∧ (alloc_lt Global (Stack _ _) ⇔ T)
  ∧ (alloc_lt Heap (Stack _ _) ⇔ T)
  ∧ (alloc_lt (Stack sid1 fid1) (Stack sid2 fid2) ⇔
     (stack_id_dest sid1 ⊗ frame_id_dest fid1) <
     (stack_id_dest sid2 ⊗ frame_id_dest fid2))
  ∧ (alloc_lt _ _ ⇔ F)
`

val addr_lt_def = Define`
  addr_lt ((a1, n1) : addr) ((a2, n2) : addr) ⇔
    a1 = a2 ∧ n1 < n2 ∨ alloc_lt a1 a2
`

val _ = Datatype`
  mem_error_message =
  | Unallocated
  | BadOffset      (uvm_type list) num
  | LoadWrongType  (uvm_type list) uvm_type
  | StoreWrongType (uvm_type list) value
`

val _ = type_abbrev("mem_error", ``:('addr # mem_error_message) error``)

(* A memory cell is a triple representing a typed location holding a single
   scalar value. The elements of the triple are:

   1. The value itself.

   2. A list of the location's possible types, from smallest to largest---e.g.,
      the first cell of an "array<vector<float 2> 4>" would have the types
      "float", "vector<float 2>", "array<vector<float 2> 4>".

   3. A list of offsets, not including offset 0 (the current location). These
      are the offsets of the components of the largest type in 2. Smaller types
      in 2 should be prefixes of the largest type, and will use some prefix of
      this list as their component list.
*)
val _ = type_abbrev("mem_cell", ``:value # uvm_type list # num list``)
val _ = type_abbrev("mem_region", ``:mem_cell spt``)

val max_key_def = Define`max_key = foldi (λn _ m. MAX n m) 0 0`

val next_key_def = Define`next_key t = if isEmpty t then 0 else SUC (max_key t)`

val mem_cell_types_def = Define`
  mem_cell_types (sz : num option)
                 (ty : uvm_type)
                 (next_loc : α -> num # α)
                 (loc_src : α)
                 : (num, uvm_type list # num list) alist # α =
  let ts = component_types sz ty;
      (locs, loc_src) = FOLDL
        (λ(ns, s) _. (C SNOC ns ## I) (next_loc s)) ([], loc_src) ts
  in (GENLIST
        (λn. let sz = if n = 0 then sz else NONE; locs' = DROP n locs;
                 offsets = TL (TAKE (LENGTH (component_types sz ty)) locs')
             in (HD locs', EL n ts, offsets))
        (LENGTH ts), loc_src)
`

val new_mem_cells_def = Define`
  new_mem_cells (sz : num option)
                (ty : uvm_type)
                (next_loc : α -> num # α)
                (loc_src : α)
                : ((num, mem_cell) alist # α) + ε error =
  let (ctys, loc_src') = mem_cell_types sz ty next_loc loc_src in
  C $, loc_src' <$>
  mapM (λ(n, t). $, n o C $, t <$> zero_value (HD (FST t))) ctys
`

val region_alloc_def = Define`
  region_alloc (region : mem_region)
               (sz : num option)
               (ty : uvm_type)
               (next_loc : α -> num # α)
               (loc_src : α)
               : (num # α # mem_region) + ε error = do
    (cells, loc_src) <- new_mem_cells sz ty next_loc loc_src;
    assert (¬EXISTS (domain region o FST) cells)
      or_fail Impossible "double allocation";
    OK (FST (HD cells), loc_src, union region (fromAList cells))
  od
`

val mem_region_set_value_def = Define`
  mem_region_set_value (n : num) (v : value) (r : mem_region)
                       : mem_region + num mem_error = do
    (_, tys, ns) <- lookup n r or_fail UndefBehavior (n, Unallocated);
    assert (type_of v = HD tys) or_fail UndefBehavior (n, StoreWrongType tys v);
    return (insert n (v, tys, ns) r)
  od
`

val _ = Datatype`
  resume_values =
  | NOR (α list)
  | EXC (α list) α
`

val _ = Datatype`
  address_space = <|
    globals : α spt;
    heap : α spt;
    stack : α spt spt
  |>
`

val _ = type_abbrev("memory", ``:mem_cell address_space``)
val _ = type_abbrev("buffer", ``:value list address_space``)

val address_space_ok_def = Define`
  address_space_ok (as : α address_space) ⇔
    wf as.globals
  ∧ wf as.heap
  ∧ wf as.stack
  ∧ EVERY wf (toList as.stack)
`

val mem_cell_ok_def = Define`
  mem_cell_ok ((v, tys, offs) : mem_cell) ⇔
      (* At least one type *)
      ¬NULL tys
      (* Smallest type is scalar *)
    ∧ scalar_type (HD tys)
      (* Each type is the prefix of the next largest type *)
    ∧ EVERY2 prefix_type (TL tys) (FRONT tys)
      (* At least enough offsets for the largest type *)
    ∧ LENGTH (component_types (SOME 0) (LAST tys)) ≤ SUC (LENGTH offs)
      (* Value should be of the smallest (scalar) type *)
    ∧ type_of v = HD tys
`

val mem_region_ok_def = Define`
  mem_region_ok (region : mem_region) ⇔ (
    let offset_ok off expected_tys =
          (OPTION_MAP (FST o SND) (lookup off region) = SOME expected_tys);
        offsets_ok (_, tys, offs) =
          let offset_tys = TL (component_types (SOME (LENGTH offs)) (LAST tys))
          in EVERY2 offset_ok offs offset_tys
    in wf region ∧ EVERY (mem_cell_ok ∩ offsets_ok) (toList region))
`

val memory_ok_def = Define`
  memory_ok mem ⇔
    mem_region_ok mem.globals
  ∧ wf mem.stack
  ∧ mem_region_ok mem.heap 
  ∧ EVERY mem_region_ok (toList mem.stack)
`

val new_address_space_def = Define`
  new_address_space = <| globals := LN; heap := LN; stack := LN |>
`

val new_memory_def = Define`
  new_memory (globals : (string # uvm_type) list) : memory + ε error =
    let global_locs = GENLIST I (LENGTH globals);
        loc_gen = LUNFOLD (λn. SOME (SUC n, n)) (LENGTH globals)
    in do
      (_, globals) <-
        FOLDL (λaccum (loc, ty). do
          (loc_gen, region) <- accum;
          SND <$> region_alloc region NONE ty
            (λl. THE (LHD l), THE (LTL l)) (loc:::loc_gen)
        od) (OK (loc_gen, LN)) (ZIP (global_locs, (MAP SND globals)));
      return (new_address_space with globals := globals)
    od
`

val addrs_of_def = Define`
  addrs_of (as : α address_space) : addr list =
    MAP ($, Global o FST) (toAList as.globals) ++
    MAP ($, Heap o FST) (toAList as.heap) ++ do
      (sn, region) <- toAList as.stack;
      (n, _) <- toAList region;
      return (Stack (StackID (nfst sn)) (FrameID (nsnd sn)), n)
    od
`

val get_region_def = Define`
    get_region as Global = as.globals
  ∧ get_region as Heap = as.heap
  ∧ get_region as (Stack sid fid) =
      THE (OPTION_CHOICE
        (lookup (stack_id_dest sid ⊗ frame_id_dest fid) as.stack)
        (SOME LN))
`

val put_region_def = Define`
    put_region as Global r = as with globals := r
  ∧ put_region as Heap r = as with heap := r
  ∧ put_region as (Stack sid fid) r =
      as with stack updated_by
        insert (stack_id_dest sid ⊗ frame_id_dest fid) r
`

val get_addr_def = Define `get_addr as (a, n) = lookup n (get_region as a)`

val put_addr_def = Define`
  put_addr as (a, n) v = put_region as a (insert n v (get_region as a))
`

val map_with_keys_def = Define`
  map_with_keys (f : num -> α -> β) (t : α spt) : β spt =
    fromAList (MAP (λ(k, v). k, f k v) (toAList t))
`

val merge_def = Define`
  merge f t1 t2 =
    union (map_with_keys (λk v1. case lookup k t2 of
                                 | SOME v2 => f v1 v2
                                 | NONE => v1) t1)
          t2
`

val address_space_union_def = Define`
  address_space_union a1 a2 = a2 with <|
    globals updated_by union a1.globals;
    heap updated_by union a1.heap;
    stack updated_by merge union a1.stack
  |>
`

val memory_heap_alloc_def = Define`
  memory_heap_alloc (mem : memory)
                    (sz : num option)
                    (ty : uvm_type)
                    : (memory # addr) + ε error = do
    (allocated, _, heap) <-
      region_alloc mem.heap sz ty (λn. n, SUC n) (next_key mem.heap);
    return (mem with heap := heap, (Heap, allocated))
  od
`

val memory_stack_alloc_def = Define`
  memory_stack_alloc (mem : memory)
                     (sid : stack_id)
                     (fid : frame_id)
                     (sz : num option)
                     (ty : uvm_type)
                    : (memory # addr) + ε error =
    let key = stack_id_dest sid ⊗ frame_id_dest fid;
        s = THE (OPTION_CHOICE (lookup key mem.stack) (SOME LN));
    in do
      (allocated, _, s') <- region_alloc s sz ty (λn. n, SUC n) (next_key s);
      return ((mem with stack updated_by insert key s'),
              (Stack sid fid, allocated))
    od
`

val free_stack_def = Define`
  free_stack sid fid as =
    as with stack updated_by delete (stack_id_dest sid ⊗ frame_id_dest fid)
`

val buffer_enqueue_def = Define`
  buffer_enqueue ((a, n) : addr) (v : value) (buf : buffer) : buffer =
    let ins t = insert n (v::option_CASE (lookup n t) [] I) t in
    case a of
    | Global => buf with globals updated_by ins
    | Heap => buf with heap updated_by ins
    | Stack sid fid =>
        let key = stack_id_dest sid ⊗ frame_id_dest fid in
        buf with stack updated_by λs.
          insert key (ins (THE (OPTION_CHOICE (lookup key s) (SOME LN)))) s
`

val buffer_dequeue_def = Define`
  buffer_dequeue (buf : buffer) ((a, n) : addr) : (buffer # value) option =
    let deq t = do
      l <- lookup n t;
      return ((case FRONT l of [] => delete n | l' => insert n l') t, LAST l)
    od in
    case a of
    | Global =>
        ((λt. buf with globals := t) ## I) <$> deq buf.globals
    | Heap =>
        ((λt. buf with heap := t) ## I) <$> deq buf.heap
    | Stack sid fid =>
        let key = stack_id_dest sid ⊗ frame_id_dest fid in do
          fr <- lookup key buf.stack;
          (t, old) <- deq fr;
          return (buf with stack updated_by insert key t, old)
        od
`

val get_iaddr_cell_addrs_def = Define`
  get_iaddr_cell_addrs (mem : memory) (ia : iaddr) : addr list + addr mem_error =
    let region = get_region mem (FST ia.base); base = SND ia.base
    in do
      (* Look up type + offsets for the base location *)
      (_, base_tys, base_offs_tl) <- lookup base region
        or_fail UndefBehavior (ia.base, Unallocated);
      base_offs <- return (base::base_offs_tl);
      assert (prefix_type (LAST base_tys) ia.base_ty)
        or_fail UndefBehavior (ia.base, LoadWrongType base_tys ia.base_ty);
      assert (ia.offset < LENGTH base_offs)
        or_fail UndefBehavior (ia.base, BadOffset base_tys ia.offset);
      off <- return (EL ia.offset base_offs);

      (* Get a slice of the component's offsets *)
      (_, tys, offs_tl) <- lookup off region
        or_fail UndefBehavior ((FST ia.base, ia.offset), Unallocated);
      offs <- return (off::offs_tl);
      assert (prefix_type (LAST tys) ia.ty)
        or_fail UndefBehavior ((FST ia.base, ia.offset), LoadWrongType tys ia.ty);
      return (MAP ($, (FST ia.base))
                  (TAKE (LENGTH (component_types NONE ia.ty)) offs))
    od
`

val memory_get_def = Define`
    memory_get mem a : value + addr mem_error =
      FST <$> (get_addr mem a or_fail UndefBehavior (a, Unallocated))
`

val memory_put_def = Define`
    memory_put mem (a, n) v : memory + addr mem_error = (
      get_region mem a :>
      mem_region_set_value n v :>
      lift_err_msg ($, a ## I) :>
      liftM (put_region mem a))
`

val memory_load_def = Define`
  memory_load (mem : memory) (sb : buffer) (ib : buffer) (a : addr)
              : value + addr mem_error =
    case HD <$> get_addr sb a of
    | SOME v => return v
    | NONE => (
        case LAST <$> get_addr ib a of
        | SOME v => return v
        | NONE => memory_get mem a)
`

(******************************************************************************)

val lookup_map_with_keys = store_thm("lookup_map_with_keys",
  ``∀t f k. lookup k (map_with_keys f t) = OPTION_MAP (f k) (lookup k t)``,
  rw[map_with_keys_def, lookup_fromAList] >>
  Cases_on `OPTION_MAP (f k) (lookup k t)` >> fs[OPTION_MAP_DEF] >| [
    rw[ALOOKUP_FAILS, MEM_MAP] >> Cases_on `y` >> simp[MEM_toAList] >>
    Cases_on `k = q` >> rw[],

    `∀al. ALOOKUP (MAP (λ(k,v). (k,f k v)) al) k
      = OPTION_MAP (f k) (ALOOKUP al k)` suffices_by rw[ALOOKUP_toAList] >>
    Induct >- rw[FUN_EQ_THM] >> Cases >> rw[FUN_EQ_THM]
  ])

val wf_map_with_keys = store_thm("wf_map_with_keys",
  ``∀f t. wf (map_with_keys f t)``,
  rw[map_with_keys_def] >> qmatch_rename_tac `wf (fromAList xs)` >>
  Induct_on `xs` >- rw[fromAList_def, wf_def] >> Cases >>
  rw[fromAList_def, wf_insert])

val lookup_merge = store_thm("lookup_merge",
  ``∀t1 t2 f k. lookup k (merge f t1 t2) =
      let v1 = lookup k t1; v2 = lookup k t2
      in OPTION_CHOICE (OPTION_MAP2 f v1 v2) (OPTION_CHOICE v1 v2)``,
  rw[merge_def, lookup_union, lookup_map_with_keys] >>
  ntac 2 CASE_TAC >> fs[OPTION_MAP_DEF, OPTION_MAP2_DEF])

val wf_merge = store_thm("wf_merge",
  ``∀t1 t2 f. wf t2 ⇒ wf (merge f t1 t2)``,
  rw[merge_def, wf_union, wf_map_with_keys])

val insert_union_commute = store_thm("insert_union_commute",
  ``∀t t' n v. wf t ∧ wf t' ∧ n ∉ domain t'
      ⇒ insert n v (union t' t) = union t' (insert n v t)``,
  rw[FUN_EQ_THM, wf_insert, wf_union, spt_eq_thm, lookup_union, lookup_insert] >>
  ntac 2 CASE_TAC >> fs[domain_lookup])

val merge_empty = store_thm("merge_empty",
  ``∀f x. merge f LN x = x``,
  rw[merge_def, map_with_keys_def, toAList_def, fromAList_def, foldi_def])

(*
val address_space_union_assoc = store_thm("address_space_union_assoc",
  ``∀a1 a2 a3. address_space_union a1 (address_space_union a2 a3)
             = address_space_union (address_space_union a1 a2) a3``,
  rw[address_space_union_def, address_space_component_equality, union_assoc] >>
  match_mp_tac spt_eq_thm
*)

val address_space_union_empty = store_thm("address_space_union_empty",
  ``address_space_union new_address_space = I``,
  rw[FUN_EQ_THM, new_address_space_def, address_space_union_def,
     theorem "address_space_component_equality", merge_empty])

val mem_region_set_value_ok = store_thm("mem_region_set_value_ok",
  ``∀k v r r'. mem_region_ok r ∧ mem_region_set_value k v r = OK r'
             ⇒ mem_region_ok r'``,
  rw[mem_region_ok_def, mem_region_set_value_def, EVERY_MEM, MEM_toList, IN_APP] >>
  multi_case_tac >> fs[wf_insert, lookup_insert] >> TRY (Cases_on `r'`) >>
  multi_case_tac >> res_tac >> fs[mem_cell_ok_def] >>
  first_x_assum(fn x => mp_tac x >> match_mp_tac LIST_REL_mono) >> rw[])

val mem_region_set_value_foldr = store_thm("mem_region_set_value_foldr",
  ``∀rs m1 m2 m1' m2'.
      (∀r. lookup r m1 = lookup r m2)
    ∧ FOLDR (λr s. s >>= (λ(r,v) rs. mem_region_set_value r v rs) r) (OK m1) rs = OK m1'
    ∧ FOLDR (λr s. s >>= (λ(r,v) rs. mem_region_set_value r v rs) r) (OK m2) rs = OK m2'
    ⇒ ∀r. lookup r m1' = lookup r m2'``,
  Induct >> rw[] >> fs[UNCURRY] >>
  REPEAT (qpat_x_assum `mem_region_set_value _ _ _ = _`
    (assume_tac o ONCE_REWRITE_RULE[mem_region_set_value_def])) >>
  fs[UNCURRY] >> rw[lookup_insert] >> res_tac >| [fs[], rfs[]])

val mem_region_set_value_foldr_fail = store_thm("mem_region_set_value_foldr_fail",
  ``∀rs m1 m2 e.
      (∀r. lookup r m1 = lookup r m2)
    ∧ FOLDR (λr s. s >>= (λ(r,v) rs. mem_region_set_value r v rs) r) (OK m1) rs = Fail e
    ⇒ FOLDR (λr s. s >>= (λ(r,v) rs. mem_region_set_value r v rs) r) (OK m2) rs = Fail e``,
  Induct >> rw[] >> Cases_on `h` >> fs[] >>
  ONCE_REWRITE_TAC[bind_left_case] >>
  pop_assum (assume_tac o ONCE_REWRITE_RULE[bind_left_case]) >> multi_case_tac >>
  TRY (qpat_x_assum `mem_region_set_value _ _ _ = _`
    (assume_tac o ONCE_REWRITE_RULE[mem_region_set_value_def])) >>
  ONCE_REWRITE_TAC[mem_region_set_value_def] >> fs[] >| [
    `lookup q x' = lookup q x` by prove_tac[mem_region_set_value_foldr] >>
      fs[] >> Cases_on `lookup q x` >> fs[UNCURRY] >>
      Cases_on `type_of r = HD (FST (SND x''))` >> fs[],
    res_tac >> fs[],
    `∀r. lookup r m2 = lookup r m1` by simp[] >> res_tac >> fs[],
    res_tac >> fs[]
  ])

(*
val new_mem_cells_ok = store_thm("new_mem_cells_ok",
  ``∀sz ty f n n' cells.
      new_mem_cells sz ty f n = OK (cells, n')
    ⇒ EVERY (mem_cell_ok o SND) cells``,
  ntac 4 strip_tac >> measureInduct_on `uvm_type_depth ty` >> 
  fs[new_mem_cells_def, mem_cell_types_def, EVERY_MEM] >>
  (* TODO: Continue here *)

  Induct_on `component_types sz ty`
  map_every Cases_on [`e`, `r`, `r'`] >> simp[mem_cell_ok_def] >>
  Induct_on `component_types sz ty`
  Cases_on `ty` >> fs[Once component_types_def]
  *)

(*
val region_alloc_ok = store_thm("region_alloc_ok",
  ``∀r sz ty f n a n' r'.
      mem_region_ok r ∧ region_alloc r sz ty f n = OK (a, n', r')
      ⇒ mem_region_ok r'``,
  rw[mem_region_ok_def, region_alloc_def] >> multi_case_tac >>
  fs[wf_union, wf_fromAList, EVERY_MEM, MEM_toList] >> rw[] >>
  multi_case_tac >> Cases_on `r'` >> fs[lookup_union] >>
  reverse (Cases_on `lookup k r`) >> rfs[] >| [
    res_tac >> fs[] >>
      first_x_assum (fn x => mp_tac x >> match_mp_tac LIST_REL_mono) >>
      rw[] >> simp[],
    fs[lookup_fromAList] >> imp_res_tac alistTheory.ALOOKUP_MEM >>
    rw[]

  
  >| [
    POP_ASSUM_LIST (mp_tac o last) >> Induct_on `q` >> rw[] >> fs[] >>
      Cases_on `h` >> simp[wf_insert],
    fs[new_mem_cells_def, mem_cell_types_def] >> multi_case_tac >>
      qmatch_assum_rename_tac `FOLDL _ _ tys = _`
  ]
  

local open rich_listTheory in
val new_memory_ok = store_thm("new_memory_ok",
  ``∀gs m. new_memory gs = OK m ⇒ memory_ok m``,
  rw[new_memory_def, new_address_space_def, memory_ok_def, mem_region_ok_def] >>
  multi_case_tac >> simp[wf_def, toList_def, toListA_def] >| [
    POP_ASSUM_LIST (mp_tac o last) >>
    qmatch_rename_tac `FOLDL _ (OK (ll, _)) (ZIP (_, MAP SND globals)) = _ ⇒ _` >>
    map_every qid_spec_tac [`q`, `r`, `globals`] >>
    listLib.SNOC_INDUCT_TAC >> fs[wf_def] >> rw[] >>
    fs[MAP_SNOC, GENLIST, ZIP_SNOC, FOLDL_SNOC, region_alloc_def] >>
    multi_case_tac >> fs[] >> POP_ASSUM_LIST (mp_tac o last) >>
    Induct_on `q''` >> rw[] >> fs[] >> Cases_on `h` >> simp[wf_insert],

    

  ]
end
*)
(*
(* wf and memory_ok propagate through all memory manipulation fns. *)

val region_alloc_is_ptree = store_thm("region_alloc_wf",
  ``∀r sz ty f n a n' r'.
      wf r ∧ region_alloc r sz ty f n = OK (a, n', r') ⇒ wf r'``,
  rw[region_alloc_def] >> multi_case_tac)

val new_memory_ok = store_thm("new_memory_ok",
  ``∀gx m. new_memory gx = OK m ⇒ memory_ok m``,
  rw[new_memory_def, memory_ok_def, new_address_space_def] >>
  multi_case_tac >> simp[EVERY_LEAF_def] >>
  
  (* Proving IS_PTREE r is the hardest part of this proof.
  
     Start by removing the (LENGTH gx) from the start value of the fold, to make
     induction easier. *)
  qmatch_assum_abbrev_tac `FOLDL fn (OK (locs, Empty)) list = _` >>
  `∃locs'. FOLDL fn (OK (locs', Empty)) list = OK (q, r)` by metis_tac[] >>
  map_every qunabbrev_tac [`fn`, `list`, `locs`] >>
  pop_assum mp_tac >> pop_assum (K all_tac) >>

  (* Induct on gx, to break down FOLDL and let us use region_alloc_is_ptree. *)
  map_every qid_spec_tac [`r`, `q`, `gx`] >> listLib.SNOC_INDUCT_TAC >>
  rw[] >> fs[GENLIST, FOLDL_SNOC, MAP_SNOC, rich_listTheory.ZIP_SNOC, UNCURRY] >>
  multi_case_tac >> prove_tac[region_alloc_is_ptree])

val put_region_memory_ok = store_thm("put_region_memory_ok",
  ``∀a m r. IS_PTREE r ∧ memory_ok m ⇒ memory_ok (put_region m a r)``,
  Cases >> rw[memory_ok_def, put_region_def, EVERY_LEAF_ADD] >| [
    Induct_on `r` >> fs[TRAVERSE_def, EVERY_LEAF_def] >> ntac 3 strip_tac >>
    pop_assum (assume_tac o ONCE_REWRITE_RULE [IS_PTREE_def]) >> fs[] >> rw[] >>
    match_mp_tac EVERY_LEAF_MONO >| [
      qexists_tac `λk _. k < SUC (FOLDR MAX 0 (TRAVERSE r))` >> fs[] >> rw[]
      >> (* TODO: continue... *)
    ]
  ])
*)

(*
val memory_heap_alloc_ok = store_thm("memory_heap_alloc_ok",
  ``∀m sz ty m' a. memory_ok m ∧ memory_heap_alloc m sz ty = OK (m', a)
                 ⇒ memory_ok m'``,
  rw[memory_ok_def, memory_heap_alloc_def] >> fs[UNCURRY] >> multi_case_tac >>
  Cases_on `r` >> fs[] >>
  TRY (imp_res_tac region_alloc_is_ptree >> fs[] >> NO_TAC) >>
  Induct_on `r'` >> fs[region_alloc_def, new_mem_cells_def, UNCURRY] >>
  rw[] >> fs[EVERY_LEAF_def] >| [
    


    pop_assum mp_tac >> ntac 2 (pop_assum (K all_tac)) >> pop_assum mp_tac >>
    map_every qid_spec_tac [`q`, `sz`, `ty`, `cells`] >>
    Induct >> rw[] >- fs[ADD_LIST_def] >>
  ]
  *)

val _ = export_theory()

