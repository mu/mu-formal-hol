open HolKernel Parse boolLib bossLib
open listTheory
open syntaxSugarLib

val _ = new_theory "syntaxSugar"
val _ = ParseExtras.tight_equality()

val _ = app prefix_op ["%", "∁", "⍽", "✔", "✘"]
val _ = set_fixity "≺" (Infix(NONASSOC, 450))
val _ = set_fixity "≼" (Infix(NONASSOC, 450))

val _ = Unicode.unicode_version{tmnm="COMPL", u="∁"}

(* Unicode ∷ disabled because it prevented [] list syntax from working... *)
(* val _ = Unicode.unicode_version{tmnm="CONS", u="∷"} *)

val _ = TeX_notation {hol="%", TeX=("\\%", 1)}
(* For some reason HOL doesn't define this symbol by default... *)
val _ = TeX_notation{hol="⊌", TeX=("\\ensuremath{\\uplus}", 1)}

val _ = export_theory()

