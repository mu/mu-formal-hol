# Formal Specification of the MicroVM

This project aims to give a formal specification of important aspects of the MicroVM, mechanised in the HOL4 theorem prover.

In particular, we aim to handle the semantics of the mu IR.
