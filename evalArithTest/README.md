# Evaluation function testing on toy expression language
## Alexander Soen

exprEvalScript.sml
  - defines the grammer of the expression language and the standard evaluation function for it

exprScheduleScript.sml
  - defines a scheduler function which finds valids schedules for a defined program
  - defines an evaluation function which evaluates a program by n steps

exprTestScript.sml
  - a set of test programs and schedules for the standard evaluation function

ml\_hol\_exprProgScript.sml
  - translation of all functions from the above scripts

ppExprScript.sml
  - pretty prints the translated functions into a sml format in a text file

### References
Expression language - $(HOLDIR)/examples/opsemTools/newOpsemScript.sml

Translation and pretty printing - $(CMLDIR)/candle/standard/ml\_kernel
