(* 
   Expression Based Language small scale test
   Alexander Soen

   Things to do:
     - Extend the language (or start a new one)
       - Delayed to fix other bugs
       - Start new script

     - Include a verification step when computing expressions
       That is make sure that the expression is valid if and only if an
       evaluation terminates
      o- Might be useful to restrict the elements of the lookup table to have
         expressions which are only valid
      o- SEEMS to do this automatically as any expr not of the form defined in
         the datastructure throws an error
         - Most problems are covered by the definition of the datastructures.
           Potentially useful in more complicated languages

    x- Make the lookup table arbritrary in contents (expr vs Const c)
       - Okay so this works only for the stepwise/multistep evaluation
         functions. The full evaluation function has a non-termination error
      x- Need to figure out how to do this for the full_evaluation case
         - Decided fix is to make the env only lookup naturals

    x- A possible add would be to create two more HOL functions which are
       similar to full_eval but goes to the step where it ends at SOME Const c 
       for some c; and another one which simply maps from SOME Const c -> SOME
       c. This could possibly make it easier to prove "equivalence" in the
       different eval functions
   

   Notes:
     - A lot of the eval functions do not have a general case and will error
       out

     - May have "repeated/redudent" cases on some of the functions
*)


(*
   Imports

*)
load "stringTheory";
(* The Interpreted line
open stringTheory;
*)
load "alistTheory";

(*
   Defining the basic data structure of the language
*)
Datatype`
  expr = Var string
  | Const num
  | Plus expr expr
  | Times expr expr
`


(*
   Setting fixities
   Let "Negate" have the strongest binding followed by "Times" and "Plus"
*)
set_fixity "Plus" (Infixl 1100);
set_fixity "Times" (Infixl 1200);
(* Need to set fixity of Negation ?????
set_fixity "Negate" (); *)

(* abbreviations *)
val _ = type_abbrev ("env", ``:(string # expr) list``);
val _ = type_abbrev ("posn", ``:bool list``);

(* Correctness Checker *)
(*
   NOT needed for the current language due to its simplicity
   The plan is to scan through the parsed expression to check if there are
   malformed expressions
   This is similar to the evaluation functions but instead of evaluating
   anything we just recursive call the function down the tree
*)
val form_correct_def = Define`
  (form_correct (Var v) = T) /\
  (form_correct (Const c) = T) /\
  (form_correct (x Plus y) = case form_correct x
    of F => F
    |  T => (case form_correct y
      of F => F
      |  T => T)) /\
  (form_correct (x Times y) = case form_correct x
    of F => F
    |  T => (case form_correct y
      of F => F
      |  T => T)) /\
  (form_correct _ = F)
`


(*
   The evaluation functions
*)

(* Trying to get abritrary look up *)
val test_eval_def = Define`
  (test_eval (Var v) e = case ALOOKUP e v
    of NONE     => NONE
    |  SOME res => test_eval res e)
`

(* Basic evaluation with a look up structure using the alistTheory *)
val full_eval_def = Define`
  (full_eval (Var v) e = ALOOKUP e v) /\
  (full_eval (Const c) e = SOME c) /\
  (full_eval (x Plus y) e = case full_eval x e
    of NONE    => NONE
    |  SOME c1 => case full_eval y e
      of NONE    => NONE
      |  SOME c2 => (SOME (c1 + c2))) /\
  (full_eval (x Times y) e = case full_eval x e
    of NONE    => NONE
    |  SOME c1 => case full_eval y e
      of NONE    => NONE
      |  SOME c2 => (SOME (c1 * c2)))
`
(* --- *)


(* Test cases for full_eval functions *)
EVAL ``full_eval ((Const 5) Plus (Const 2)) []``;
EVAL ``full_eval ((Const 5) Times (Var "Temp")) [("Temp", 3)]``;
EVAL ``full_eval ((Const 5) Times (Var "Temp")) [("Not", 3)]``;
EVAL ``full_eval (((Const 5) Plus (Const 2)) Times (Var "Temp")) [("Temp", 3)]``;


(* Partial application *)
(* Left to Right Once *)
val evalLR1_def = Define`
  (evalLR1 (Var v) e = case ALOOKUP e v
    of NONE     => NONE
    |  SOME res => SOME (Const res)) /\
  (* return NONE if trying to simplify when already simplified *)
  (evalLR1 (Const c) e = NONE) /\
  (evalLR1 (x Plus y) e = case x
    of Const c1 => (case y
      of Const c2 => SOME (Const (c1 + c2))
      |  res1     => (case evalLR1 res1 e
        of NONE      => NONE
        |  SOME res2 => SOME (x Plus res2)))
    | res1      => (case evalLR1 res1 e
      of NONE      => NONE
      |  SOME res2 => SOME (res2 Plus y))) /\
  (evalLR1 (x Times y) e = case x
    of Const c1 => (case y
      of Const c2 => SOME (Const (c1 * c2))
      |  res1     => (case evalLR1 res1 e
        of NONE      => NONE
        |  SOME res2 => SOME (x Times res2)))
    | res1      => (case evalLR1 res1 e
      of NONE      => NONE
      |  SOME res2 => SOME (res2 Times y)))
`
(* --- *)

(* Test cases *)
EVAL ``evalLR1 ((Const 5) Plus (Const 2)) []``;
EVAL ``evalLR1 (((Const 5) Plus (Const 3)) Plus (Const 2)) []``;
EVAL ``evalLR1 (Const 5) []``;
EVAL ``evalLR1 (((Var "foo") Plus (Const 3)) Plus (Const 2)) [("foo", 7)]``;
EVAL ``evalLR1 (((Var "foo") Plus (Const 3)) Plus (Const 2)) [("bar", 7)]``;
EVAL ``evalLR1 (((Const 5) Plus ((Const 5) Times (Const 3))) Plus (Const 2)) []``;


(* Right to Left Once *)
val evalRL1_def = Define`
  (evalRL1 (Var v) e = case ALOOKUP e v
    of NONE     => NONE
    |  SOME res => SOME (Const res)) /\
  (* return NONE if trying to simplify when already simplified *)
  (evalRL1 (Const c) e = NONE) /\
  (evalRL1 (y Plus x) e = case x
    of Const c1 => (case y
      of Const c2 => SOME (Const (c1 + c2))
      |  res1     => (case evalRL1 res1 e
        of NONE      => NONE
        |  SOME res2 => SOME (res2 Plus x)))
    | res1      => (case evalRL1 res1 e
      of NONE      => NONE
      |  SOME res2 => SOME (y Plus res2))) /\
  (evalRL1 (y Times x) e = case x
    of Const c1 => (case y
      of Const c2 => SOME (Const (c1 * c2))
      |  res1     => (case evalRL1 res1 e
        of NONE      => NONE
        |  SOME res2 => SOME (res2 Times x)))
    | res1      => (case evalRL1 res1 e
      of NONE      => NONE
      |  SOME res2 => SOME (y Times res2)))
`
(* --- *)

(* Test cases *)
EVAL ``evalRL1 ((Const 5) Plus (Const 2)) []``;
EVAL ``evalRL1 (((Const 5) Plus (Const 3)) Plus (Const 2)) []``;
EVAL ``evalRL1 (Const 5) []``;
EVAL ``evalRL1 (((Var "foo") Plus (Const 3)) Plus (Const 2)) [("foo", 7)]``;
EVAL ``evalRL1 (((Var "foo") Plus (Const 3)) Plus (Const 2)) [("bar", 7)]``;
EVAL ``evalRL1 (((Const 5) Plus ((Const 5) Times (Const 3))) Plus (Const 2)) []``;
EVAL ``evalRL1 (((Const 5) Plus ((Const 5) Times (Const 3))) Plus ((Const 2) Plus (Const 3))) []``;


(* Multiple partial applications *)
(*
   Error Cases: - Unbound Variable
                - Too Many Applications

   Type: :num -> expr -> (string # expr) list -> expr option
*)
(* Left to Right multiple application version *)
val evalLRn_def = Define`
  (evalLRn 0 xpr _ = SOME xpr) /\
  (evalLRn (SUC n) xpr e = case evalLR1 xpr e
    of NONE     => NONE
    |  SOME res => evalLRn n res e)
`

(* Test cases *)
EVAL ``evalLRn 1 ((Const 5) Plus (Const 2)) []``;
EVAL ``evalLRn 0 ((Const 5) Plus (Const 2)) []``;
EVAL ``evalLRn 0 (Plus (Const 2)) []``;
EVAL ``evalLRn 0 (((Const 5) Plus (Const 3)) Plus (Const 2)) []``;
EVAL ``evalLRn 1 (((Const 5) Plus (Const 3)) Plus (Const 2)) []``;
EVAL ``evalLRn 2 (((Const 5) Plus (Const 3)) Plus (Const 2)) []``;
EVAL ``evalLRn 3 (((Const 5) Plus (Const 3)) Plus (Const 2)) []``;
EVAL ``evalLRn 1 (Const 5) []``;
EVAL ``evalLRn 3 (((Var "foo") Plus (Const 3)) Plus (Const 2)) [("foo", 7)]``;
EVAL ``evalLRn 3 (((Var "foo") Plus (Const 3)) Plus (Const 2)) [("bar", 7)]``;
EVAL ``evalLRn 3 (((Const 5) Plus ((Const 5) Times (Const 3))) Plus (Const 2)) []``;
EVAL ``evalLRn 4 (((Const 5) Plus ((Const 5) Times (Const 3))) Plus ((Const 2) Plus (Const 3))) []``;


(* Positition Evaluation *)
(* Redefine expr for this part *)
(* Datatype `expr' = Var string
               | Const num
               | Plus expr expr
               | Times expr expr`;
*)

(* Helper Single Application no Recurse and NONE on invalid input *)
val eval_once_def = Define`
  (eval_once (Var v) e = case ALOOKUP e v
    of NONE     => NONE
    |  SOME res => case form_correct res
      of F => NONE
      |  T => SOME res) /\
  (eval_once ((Const x) Plus (Const y))  _ = SOME (Const (x + y))) /\
  (eval_once ((Const x) Times (Const y)) _ = SOME (Const (x * y))) /\
  (eval_once _ _ = NONE)
`

(* Single Application *)
(*
   Error Cases: Unbound Variable
                Tries to evaluate to a non-terminal
                Invalid move
                
   Type: expr -> :env -> :posn -> expr option

   Position Def: T = right
                 F = left
*)
val evalLRp_def = Define`
  (evalLRp xpr e [] = eval_once xpr e) /\
  (evalLRp xpr e (x::rst) = case xpr
    of (l Plus r)  => (case x
      of T => (case evalLRp r e rst
        of NONE     => NONE
        |  SOME res => SOME (l Plus res))
      |  F => (case evalLRp l e rst
        of NONE     => NONE
        |  SOME res => SOME (res Plus r)))
    |  (l Times r) => (case x
      of T => (case evalLRp r e rst
        of NONE     => NONE
        |  SOME res => SOME (l Times res))
      |  F => (case evalLRp l e rst
        of NONE     => NONE
        |  SOME res => SOME (res Times r)))
    | _            => NONE)
`

(* Test Cases *)
EVAL ``evalLRp ((Const 5) Plus (Const 3)) [] []``;
EVAL ``evalLRp ((Plus 5) Plus (Const 3)) [] []``;
EVAL ``evalLRp ((Plus 5) Plus (Const 3)) [] []``;
EVAL ``evalLRp (((Const 5) Plus (Const 2)) Times (Const 3)) [] []``;
EVAL ``evalLRp (((Const 5) Plus (Const 2)) Times (Const 3)) [] [F]``;
EVAL ``evalLRp (((Const 5) Plus (Const 2)) Times (Const 3)) [] [T]``;
EVAL ``evalLRp (((Const 5) Plus (Const 2)) Times (Var "foo")) [("foo", Const 3)] [T]``;


(* Schedule/Multiple application step *)
val evalS_def = Define `
  (* Empty list of list indicate done so just return value if VALID *)
  (evalS xpr _ [] = SOME xpr) /\
  (evalS xpr e (s::rst) = case s
    of [] => (case evalLRp xpr e []
      of NONE     => NONE
      |  SOME res => evalS res e rst)
    | ls  => (case evalLRp xpr e ls
      of NONE     => NONE
      |  SOME res => evalS res e rst))
`

(* Test Cases *)
EVAL ``evalS ((Const 5) Plus (Const 3)) [] []``;
EVAL ``evalS ((Const 5) Plus (Const 3)) [] [N]``;
EVAL ``evalS (((Const 5) Plus (Const 2)) Times (Const 3)) [] []``;
EVAL ``evalS (((Const 5) Plus (Const 2)) Times (Const 3)) [] [blist [F]]``;
EVAL ``evalS (((Const 5) Plus (Const 2)) Times (Const 3)) [] [blist [T]]``;
EVAL ``evalS (((Const 5) Plus (Const 2)) Times (Const 3)) [] [blist []]``;
EVAL ``evalS (((Const 5) Plus (Const 2)) Times (Const 3)) 
       [("foo", Const 42)] [blist [F]; N]``;
EVAL ``evalS (((Var "foo") Plus (Const 2)) Times (Const 3)) 
       [("foo", Const 42)] [blist [F;F]; blist [F]; N]``;
(* --- *)
