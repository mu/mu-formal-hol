(* 
   The script file which will nicely package the evaluation functions
   into a translated term ready to be printed to a file
*)

open HolKernel bossLib Parse boolLib;
open ml_progTheory; (* for the ML_code_def *)
open ml_progLib; (* for the get_thm function *)
open ml_translatorTheory ml_translatorLib; (* Translator *)
open alistTheory pairTheory listTheory optionTheory;
open exprEvalTheory exprScheduleTheory exprTestTheory exprSexpTheory;
open simpleSexpTheory fromSexpTheory;

val _ = new_theory "ml_hol_exprProg";

(* Translatation Step *)
(* Basic definitions *)
val _ = translate ALOOKUP_def
val _ = translate APPEND
val _ = translate FST
val _ = translate SND

(* Basic evaluation functions*)
val _ = translate N_def
val _ = translate update_env_def
val _ = translate remove_env_def
val _ = translate eval_expr_step_def
val _ = translate eval_bexpr_step_def
val _ = translate eval_step_def
val _ = translate eval_scheduler_def

(* Scheduler steps *)
val _ = translate find_expr_steps_def
val _ = translate find_bexpr_steps_def
val _ = translate find_steps_def
val _ = translate eval_n_steps_def

(* Testing functions *)
(* Fibonacci *)
val _ = translate fib_prog_def
val _ = translate fib_init_def
val _ = translate fib_cond_def
val _ = translate fib_body_def
val _ = translate fib_free_def

val _ = translate fib_n_schedule_help_def
val _ = translate fib_n_schedule_def
val _ = translate fib_n_def

(* Factorial *)
val _ = translate fac_prog_def
val _ = translate fac_init_def
val _ = translate fac_cond_def
val _ = translate fac_body_def
val _ = translate fac_free_def

val _ = translate fac_n_schedule_help_def
val _ = translate fac_n_schedule_def
val _ = translate fac_n_def
(* --- *)

(* Sexp *)
val _ = translate strstrip_sexp_def
val _ = translate numstrip_sexp_def


(* Helper functions from already done Sexp files *)
val _ = translate OPTION_MAP_DEF
val _ = translate IS_SOME_DEF
val _ = translate THE_DEF
val _ = translate OPTION_MAP2_DEF
val _ = translate OPTION_MAP3_def
val _ = translate OPTION_BIND_def
val _ = translate MAP
val _ = translate FOLDR
val _ = translate strip_sxcons_def
val _ = translate dstrip_sexp_def

(* Sexp parsers *)
val _ = translate sexp2expr_def
val _ = translate sexp2bexpr_def
val _ = translate sexp2stmt_def

val _ = translate sexptuple2envtuple_def
val _ = translate sexp2env_def

val _ = translate sexp2dir_def
val _ = translate sexp2step_def
val _ = translate sexp2schedule_def

(* Sexp Eval *)
val _ = translate seval_scheduler_def

(* --- *)


(* Gets prog current state, not sure what it does completely *)
fun get_curr_prog_state () = let
  val k = ref init_state
  val _ = ml_prog_update (fn s => (k := s; s))
  in !k end

fun define_abbrev_conv name tm = let
  val def = define_abbrev true name tm
  in GSYM def |> SPEC_ALL end

val expr_prog_thm =
  get_thm (get_curr_prog_state ())
  |> REWRITE_RULE [ML_code_def]
  |> CONV_RULE ((RATOR_CONV o RATOR_CONV o RAND_CONV)
                (EVAL THENC define_abbrev_conv "expr_code"))
  |> CONV_RULE ((RATOR_CONV o RAND_CONV)
                (EVAL THENC define_abbrev_conv "expr_init_env"))
  |> CONV_RULE ((RAND_CONV)
                (EVAL THENC define_abbrev_conv "expr_init_state"))
  |> curry save_thm "expr_prog_thm"

val _ = export_theory ();
