(*
   Test script for the evaluation language
   Alexander Soen
*)

open HolKernel bossLib Parse boolLib;
open stringTheory listTheory; 
open exprEvalTheory;

val _ = new_theory "exprTest";

(* Test scripts *)
val test1 = Q.store_thm (
  "test1",
  `eval_scheduler (Parallel
           (Assign "foo" (Plus (Const 2) (Const 1)))
           (Assign "foo" (Times (Const 2) (Const 3))))
          []
          [[L;L];[R;R];[L];[R];[]]
          = SOME(Skip, [("foo", 6)])`,
  EVAL_TAC);

(* Start Fibonacci Test *)
(*
   Gets the nth value of the fibonacci sequence
*)
(* Start Define *)
val fib_prog_def = Define`
  fib_prog = 
      (Seq
          (Assign "counter"
              (Const 0)
          )
          (Seq
              (Assign "res" 
                  (Const 0)
              )
              (Seq
                  (Assign "calc"
                      (Const 1)
                  )
                  (Seq
                      (Assign "tmp"
                          (Const 0)
                      )
                      (Seq
                          (While
                              (Lt
                                  (Var "counter")
                                  (Var "n")
                              )
                              (Seq
                                  (Assign "tmp"
                                      (Plus
                                          (Var "res")
                                          (Var "calc")
                                      )
                                  )
                                  (Seq
                                      (Assign "res"
                                          (Var "calc")
                                      )
                                      (Seq
                                          (Assign "calc"
                                              (Var "tmp")
                                          )
                                          (Assign "counter"
                                              (Plus
                                                  (Var "counter")
                                                  (Const 1)
                                              )
                                          )
                                      )
                                  )
                              )
                          )
                          (Seq
                              (Free "n")
                              (Seq
                                  (Free "counter")
                                  (Seq
                                      (Free "calc")
                                      (Free "tmp")
                                  )
                              )
                          )
                      )
                  )
              )
          )
      )
`
(* End Define *)

(* Start Scheduler instructions *)
val fib_init_def = Define`
  fib_init = [
    [N];[];[N];[];[N];[];[N];[] (* Assign all vars *)
  ]
`

val fib_cond_def = Define`
  fib_cond = [
    (* loop check *)
    [N] (* resolve while *)
    ;[N;N;L] (* resolve counter *)
    ;[N;N;R] (* resolve n *)
    ;[N;N] (* resolve counter < n *)
    ;[N] (* resolve Elf *)
    ]
`

val fib_body_def = Define`
  fib_body = [
    (* inc part *)
    [N;N;N;N;L] (* resolve res *)
    ;[N;N;N;N;R] (* resolve calc *)
    ;[N;N;N;N] (* resolve res plus calc *)
    ;[N;N;N] (* assign tmp *)
     (* - *)
    ;[N;N] (* resolve seq *)
    ;[N;N;N;N] (* resolve calc *)
    ;[N;N;N] (* assign res *)
    (* - *)
    ;[N;N] (* resolve seq *)
    ;[N;N;N;N] (* resolve tmp *)
    ;[N;N;N] (* assign calc *)
    (* - *)
    ;[N;N] (* resolve seq *)
    ;[N;N;N;L] (* resolve counter *)
    ;[N;N;N] (* resolve plus (inc by one) *)
    ;[N;N] (* assign counter *)
    (* - *)
    ;[N] (* resolve seq *)
    ] ++ fib_cond
`;

val fib_free_def = Define`
  fib_free = [
    [];[N];[];[N];[];[N];[];[]
    ] (* Frees all the variables appart from "res" *)
`
(* End Scheduler instructions *)

(* Start Helper nth schedule *)
val fib_n_schedule_help_def = Define`
  (fib_n_schedule_help 0 = fib_free) ∧
  (fib_n_schedule_help (SUC n) = fib_body ++ (fib_n_schedule_help n))
`

val fib_n_schedule_def = Define`
  fib_n_schedule n = fib_init ++ fib_cond ++ (fib_n_schedule_help n)
`

val fib_n_def = Define`
  (fib_n 0 = 0) ∧
  (fib_n (SUC 0) = 1) ∧
  (fib_n (SUC (SUC n)) = fib_n n + fib_n (SUC n))
`
(* End Helper nth schedule *)

(* Start nth Value *)
val fib_testn = Q.store_thm (
  "fib_testn",
  `eval_scheduler fib_prog [("n", 23)]
    (fib_n_schedule 23) = SOME (Skip, [("res", 28657)])`,
  EVAL_TAC);
(* End nth Value *)

(* End Fibonacci Test *)


(* Start Factorial Test *)

(* Start Define *)
val fac_prog_def = Define`
  fac_prog = 
    (Seq
        (Assign "counter"
            (Const 0)
        )
        (Seq
            (Assign "res"
                (Const 1)
            )
            (Seq
                (While
                    (Gt
                        (Var "n")
                        (Var "counter")
                    )
                    (Seq
                        (Assign "counter"
                            (Plus
                                (Var "counter")
                                (Const 1)
                            )
                        )
                        (Assign "res"
                            (Times
                                (Var "res")
                                (Var "counter")
                            )
                        )
                    )
                )
                (Seq
                    (Free "counter")
                    (Free "n")
                )
            )
        )
    )
`
(* End Define *)

(* Start Scheduler instructions *)
val fac_init_def = Define`
  fac_init = [
    (* Initialization *)
    [N]; (* assign counter to init val 0 *)
    []; (* resolve seq *)
    [N]; (* assign res to init val 1 *)
    [] (* resolve seq *)
    ]
`
val fac_cond_def = Define`
  fac_cond = [
    (* Cond check *)
    [N]; (* resolve while to elf *)
    [N;N;L]; (* resolve Var "n" *)
    [N;N;R]; (* resolve Var "counter" *)
    [N;N]; (* resolve n > counter *)
    [N] (* resolve elf conditional *)
    ]
`
val fac_body_def = Define`
  fac_body = [
    (* Body *)
    [N;N;N;N;L]; (* resolve Var "counter" *)
    [N;N;N;N]; (* resolve "counter" Plus 1 *)
    [N;N;N]; (* assign "counter" to "counter" + 1 *)
    [N;N]; (* resolve seq *)
    [N;N;N;L]; (* resolve var "res" *)
    [N;N;N;R]; (* resolve var "counter" *)
    [N;N;N]; (* resolve res times counter *)
    [N;N]; (* resolve res equal res times counter *)
    [N] (* resolve seq *)
    ]
    ++ fac_cond
`
val fac_free_def = Define`
  fac_free = [
    (* Free *)
    []; (* resolve seq *)
    [N]; (* Free counter *)
    []; (* resolve seq *)
    [] (* free "n" *)
    ]
`
(* End Scheduler instructions *)

(* Start Helper nth schedule *)
val fac_n_schedule_help_def = Define`
  (fac_n_schedule_help 0 = fac_free) ∧
  (fac_n_schedule_help (SUC n) = fac_body ++ (fac_n_schedule_help n))
`

val fac_n_schedule_def = Define`
fac_n_schedule n = fac_init ++ fac_cond ++ (fac_n_schedule_help n)
`

val fac_n_def = Define`
  (fac_n 0 = 1) ∧
  (fac_n (SUC n) = (SUC n) * fac_n n)
`
(* End Helper nth schedule *)

(* Start nth value *)
val fac_testn = Q.store_thm (
  "fac_testn",
  `eval_scheduler fac_prog [("n", 5)]
    (fac_n_schedule 5) = SOME (Skip, ["res", fac_n 5])`,
  EVAL_TAC);
(* End nth value *)

(* End Factorial Test *)

val _ = export_theory()
