(*
  Parse script to turn S-expressions into the defined language's datatype
  Alexander Soen
*)

open HolKernel bossLib Parse boolLib;
open stringTheory;
open exprEvalTheory;
open simpleSexpTheory fromSexpTheory;
open monadsyntax;

val _ = new_theory "exprSexp";

val _ = temp_add_monadsyntax ();
val _ = temp_overload_on ("return", ``SOME``);

(* Stripping functions to remove the S-sexpression syntax *)
val strstrip_sexp_def = Define`
  (strstrip_sexp (SX_STR str) = SOME str) ∧
  (strstrip_sexp _            = NONE)
`

val numstrip_sexp_def = Define`
  (numstrip_sexp (SX_NUM num) = SOME num) ∧
  (numstrip_sexp _            = NONE)
`

(* Termination proof required due to recursion *)
val sexp2expr_def = tDefine "sexp2expr" `
  sexp2expr s =
    case dstrip_sexp s of
      | SOME ("Var", [str]) =>
          do
            sl <- strstrip_sexp str;
            return (Var sl)
          od
      | SOME ("Const", [num]) =>
          do
            nl <- numstrip_sexp num;
            return (Const nl)
          od
      | SOME ("Plus", [expr1;expr2]) => (
          case (sexp2expr expr1, sexp2expr expr2) of
            | (SOME e1, SOME e2) => SOME (Plus e1 e2)
            | _                  => NONE)
      | SOME ("Times", [expr1;expr2]) => (
          case (sexp2expr expr1, sexp2expr expr2) of
            | (SOME e1, SOME e2) => SOME (Times e1 e2)
            | _                  => NONE)
      | _ => NONE
` (WF_REL_TAC `measure sexp_size` >> rw [] >> simp [dstrip_sexp_size])
(* Uses the measure on the size of the s-expression and then uses an
   already proven property that dstrip_sexp will always decrease the
   size of an sexpression *)

val sexp2bexpr_def = Define`
  sexp2bexpr s = 
    case dstrip_sexp s of
      | SOME ("True", []) => SOME True
      | SOME ("False", []) => SOME False
      | SOME ("Lt", [expr1; expr2]) => (
          case (sexp2expr expr1, sexp2expr expr2) of
            | (SOME e1, SOME e2) => SOME (Lt e1 e2)
            | _                  => NONE)
      | SOME ("Gt", [expr1; expr2]) => (
          case (sexp2expr expr1, sexp2expr expr2) of
            | (SOME e1, SOME e2) => SOME (Gt e1 e2)
            | _                  => NONE)
      | SOME ("Eq", [expr1; expr2]) => (
          case (sexp2expr expr1, sexp2expr expr2) of
            | (SOME e1, SOME e2) => SOME (Eq e1 e2)
            | _                  => NONE)
      | _ => NONE
`

val sexp2stmt_def = tDefine "sexp2stmt" `
  sexp2stmt s =
    case dstrip_sexp s of
      | SOME ("Skip", []) => SOME Skip
      | SOME ("Assign", [str; expr]) =>
          do
            sl <- strstrip_sexp str;
            e  <- sexp2expr expr;
            return (Assign sl e)
          od
      | SOME ("Free", [str]) =>
          do
            sl <- strstrip_sexp str;
            return (Free sl)
          od
      | SOME ("Parallel", [stmt1; stmt2]) => (
          case (sexp2stmt stmt1, sexp2stmt stmt2) of
            | (SOME s1, SOME s2) => SOME (Parallel s1 s2)
            | _                  => NONE)
      | SOME ("Elf", [bexpr; stmt1; stmt2]) => (
          case (sexp2bexpr bexpr, sexp2stmt stmt1, sexp2stmt stmt2) of
            | (SOME b, SOME s1, SOME s2) => SOME (Elf b s1 s2)
            | _                          => NONE)
      | SOME ("Seq", [stmt1; stmt2]) => (
          case (sexp2stmt stmt1, sexp2stmt stmt2) of
            | (SOME s1, SOME s2) => SOME (Seq s1 s2)
            | _                  => NONE)
      | SOME ("While", [bexpr; stmt]) => (
          case (sexp2bexpr bexpr, sexp2stmt stmt) of
            | (SOME b, SOME s) => SOME (While b s)
            | _                => NONE)
      | _ => NONE
` (WF_REL_TAC `measure sexp_size` >> rw [] >> simp [dstrip_sexp_size])
(* --- *)

(* Alternative definitions using "do" notation,
   Does not translate
val sexp2expr_do_def = tDefine "sexp2expr_do" `
  sexp2expr_do s =
    case dstrip_sexp s of
      | SOME ("Var", [str]) =>
          do
            sl <- strstrip_sexp str;
            return (Var sl)
          od
      | SOME ("Const", [num]) =>
          do
            nl <- numstrip_sexp num;
            return (Const nl)
          od
      | SOME ("Plus", [expr1;expr2]) =>
          do
            e1 <- sexp2expr_do expr1;
            e2 <- sexp2expr_do expr2;
            return (Plus e1 e2)
          od
      | SOME ("Times", [expr1;expr2]) =>
          do
            e1 <- sexp2expr_do expr1;
            e2 <- sexp2expr_do expr2;
            return (Times e1 e2)
          od
      | _ => NONE
` (WF_REL_TAC `measure sexp_size` >> rw [] >> simp [dstrip_sexp_size])

val sexp2bexpr_do_def = Define`
  sexp2bexpr_do s = 
    case dstrip_sexp s of
      | SOME ("True", []) => SOME True
      | SOME ("False", []) => SOME False
      | SOME ("Lt", [expr1; expr2]) =>
          do
            e1 <- sexp2expr_do expr1;
            e2 <- sexp2expr_do expr2;
            return (Lt e1 e2)
          od
      | SOME ("Gt", [expr1; expr2]) =>
          do
            e1 <- sexp2expr_do expr1;
            e2 <- sexp2expr_do expr2;
            return (Gt e1 e2)
          od
      | SOME ("Eq", [expr1; expr2]) =>
          do
            e1 <- sexp2expr_do expr1;
            e2 <- sexp2expr_do expr2;
            return (Eq e1 e2)
          od
      | _ => NONE
`
val sexp2stmt_do_def = tDefine "sexp2stmt_do" `
  sexp2stmt_do s =
    case dstrip_sexp s of
      | SOME ("Skip", []) => SOME Skip
      | SOME ("Assign", [str; expr]) =>
          do
            sl <- strstrip_sexp str;
            e  <- sexp2expr_do expr;
            return (Assign sl e)
          od
      | SOME ("Free", [str]) =>
          do
            sl <- strstrip_sexp str;
            return (Free sl)
          od
      | SOME ("Parallel", [stmt1; stmt2]) =>
          do
            s1 <- sexp2stmt_do stmt1;
            s2 <- sexp2stmt_do stmt2;
            return (Parallel s1 s2)
          od
      | SOME ("Elf", [bexpr; stmt1; stmt2]) =>
          do
            b  <- sexp2bexpr_do bexpr;
            s1 <- sexp2stmt_do stmt1;
            s2 <- sexp2stmt_do stmt2;
            return (Elf b s1 s2)
          od
      | SOME ("Seq", [stmt1; stmt2]) =>
          do
            s1 <- sexp2stmt_do stmt1;
            s2 <- sexp2stmt_do stmt2;
            return (Seq s1 s2)
          od
      | SOME ("While", [bexpr; stmt]) =>
          do
            b <- sexp2bexpr_do bexpr;
            s <- sexp2stmt_do stmt;
            return (While b s)
          od
      | _ => NONE
` (WF_REL_TAC `measure sexp_size` >> rw [] >> simp [dstrip_sexp_size])
*)

(* Sexp tuple to env tuple *)
val sexptuple2envtuple_def = Define`
  (sexptuple2envtuple (SX_CONS (SX_SYM sym) (SX_NUM num)) = SOME (sym, num)) ∧
  (sexptuple2envtuple _ = NONE)
`

(* Sexp to env *)
val sexp2env_def = Define`
  sexp2env s = 
    case strip_sxcons s of
      | SOME sexp => FOLDR (OPTION_MAP2 CONS) (SOME []) (MAP sexptuple2envtuple sexp)
      | NONE => NONE
`

(* --- *)

(* Sexp list to step list *)
val sexp2dir_def = Define`
  (sexp2dir (SX_SYM "L") = SOME L) ∧
  (sexp2dir (SX_SYM "R") = SOME R) ∧
  (sexp2dir (SX_SYM "N") = SOME L) ∧
  (sexp2dir _            = NONE)
`

val sexp2step_def = Define`
  sexp2step s =
    case strip_sxcons s of
      | SOME sexp => FOLDR (OPTION_MAP2 CONS) (SOME []) (MAP sexp2dir sexp)
      | NONE => NONE
`

val sexp2schedule_def = Define`
  sexp2schedule s =
    case strip_sxcons s of
      | SOME sexp => FOLDR (OPTION_MAP2 CONS) (SOME []) (MAP sexp2step sexp)
      | _ => NONE
`

(* Sexp inputs for evaluation functions *)
val OPTION_MAP3_def = Define`
  OPTION_MAP3 f x y z =
    if IS_SOME x ∧ IS_SOME y ∧ IS_SOME z then SOME (f (THE x) (THE y) (THE z)) else NONE
`

val seval_scheduler_def = Define`
  seval_scheduler stmt env schedule = OPTION_MAP3 eval_scheduler
    (sexp2stmt stmt)  (sexp2env env) (sexp2schedule schedule)
`

(* Some test cases *)
val testsexp1 = Q.store_thm (
  "testsexp1",
  `sexp2expr ⦇ SX_SYM "Plus"; ⦇ SX_SYM "Const"; 3 ⦈; ⦇ SX_SYM "Const"; 3 ⦈ ⦈
    = SOME (Const 3 +_ Const 3)`,
  EVAL_TAC);

val testsexp2 = Q.store_thm (
  "testsexp2",
  `sexp2env ⦇ ⦇ SX_SYM "foo" • 1 ⦈; ⦇ SX_SYM "bar" • 6 ⦈; ⦇ SX_SYM "tar" • 2 ⦈ ⦈
    = SOME [("foo", 1); ("bar", 6); ("tar", 2)]`,
  EVAL_TAC);

val testsexp3 = Q.store_thm (
  "testsexp3",
  `sexp2schedule ⦇ ⦇ SX_SYM "R"; SX_SYM "R"; SX_SYM "R" ⦈ ; ⦇ SX_SYM "L"; SX_SYM "R"; SX_SYM "N" ⦈ ⦈
    = SOME [[R; R; R]; [L; R; L]]`,
  EVAL_TAC);

val _ = export_theory ();
