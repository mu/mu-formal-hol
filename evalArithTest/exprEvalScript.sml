(*
   Extended Expressions Based Language for small scale tests
   Based on "basicExprScript.sml" and examples found in the HOL git repo
   Alexander Soen
*)

open HolKernel bossLib Parse boolLib;
open listTheory alistTheory stringTheory;

val _ = new_theory "exprEval";

(* Language definition *)
val _ = Datatype`
  statement =
  | Skip (* Do nothing *)
  | Assign string expr
  | Free string
  | Parallel statement statement
  | Elf bexpr statement statement
  | Seq statement statement
  | While bexpr statement ;

  expr =
  | Var string
  | Const num
  | Plus expr expr
  | Times expr expr ;

  bexpr =
  | True
  | False
  | Lt expr expr
  | Gt expr expr
  | Eq expr expr
`

(* Fixity of operators *)
val _ = set_mapped_fixity { term_name = "Seq", fixity = Infixl 500, tok = ";;"}
val _ = set_mapped_fixity { term_name = "Parallel", fixity = Infixl 500, tok = "||"}
val _ = set_mapped_fixity { term_name = "Plus", fixity = Infixl 500, tok = "+_"}
val _ = set_mapped_fixity { term_name = "Times", fixity = Infixl 500, tok = "×_"}
val _ = set_mapped_fixity { term_name = "Lt", fixity = Infixl 500, tok = "<_"}
val _ = set_mapped_fixity { term_name = "Gt", fixity = Infixl 500, tok = ">_"}
val _ = set_mapped_fixity { term_name = "Eq", fixity = Infixl 500, tok = "=_"}

(* Tree traversal datatype *)
val _ = Datatype`
  dir =
  | L
  | R
`

(* Do Ning value *)
val N_def = Define`
  N = L
`
(* List helper functions *)
(* Update *)
val update_env_def = Define`
  (update_env v [] = v :: []) ∧
  (update_env (s1, c1) ((s2, c2)::rst) =
    case s1 = s2 of
      | T => (s1, c1) :: rst
      | F => (s2, c2) :: (update_env (s1, c1) rst))
`

(* Remove *)
val remove_env_def = Define`
  (remove_env str [] = NONE) ∧
  (remove_env str ((s, c)::rst) = if str = s then SOME rst else (
    case remove_env str rst of
      | NONE      => NONE
      | SOME env' => SOME ((s, c) :: env')))
`

(* Translate ALOOKUP for use *)

(* Step Evaluation *)
(* start expression level *)
val eval_expr_step_def = Define`
  (eval_expr_step (Var str) env [] =
    case ALOOKUP env str of
      | NONE     => NONE
      | SOME res => SOME (Const res)) ∧
  (eval_expr_step (Plus (Const num1) (Const num2)) env [] =
    SOME (Const (num1 + num2))) ∧
  (eval_expr_step (Plus xpr1 xpr2) env posn =
    case posn of
      | (L::rest) => (case eval_expr_step xpr1 env rest of
                        | NONE       => NONE
                        | SOME xpr1' => SOME (Plus xpr1' xpr2))
      | (R::rest) => (case eval_expr_step xpr2 env rest of
                        | NONE       => NONE
                        | SOME xpr2' => SOME (Plus xpr1 xpr2'))
      | _         => NONE) ∧
  (eval_expr_step (Times (Const num1) (Const num2)) env [] =
    SOME (Const (num1 * num2))) ∧
  (eval_expr_step (Times xpr1 xpr2) env posn =
    case posn of
      | (L::rest) => (case eval_expr_step xpr1 env rest of
                        | NONE       => NONE
                        | SOME xpr1' => SOME (Times xpr1' xpr2))
      | (R::rest) => (case eval_expr_step xpr2 env rest of
                        | NONE       => NONE
                        | SOME xpr2' => SOME (Times xpr1 xpr2'))
      | _         => NONE) ∧
  (eval_expr_step _ _ _ = NONE)
`

(* end expression level *)


(* start boolean level *)
val eval_bexpr_step_def = Define`
  (eval_bexpr_step (Lt (Const num1) (Const num2)) env [] =
    if num1 < num2 then SOME True else SOME False) ∧
  (eval_bexpr_step (Lt xpr1 xpr2) env posn =
    case posn of
      | (L::rest) => (case eval_expr_step xpr1 env rest of
                        | NONE       => NONE
                        | SOME xpr1' => SOME (Lt xpr1' xpr2))
      | (R::rest) => (case eval_expr_step xpr2 env rest of
                        | NONE       => NONE
                        | SOME xpr2' => SOME (Lt xpr1 xpr2'))
      | _         => NONE) ∧
  (eval_bexpr_step (Gt (Const num1) (Const num2)) env [] =
    if num1 > num2 then SOME True else SOME False) ∧
  (eval_bexpr_step (Gt xpr1 xpr2) env posn =
    case posn of
      | (L::rest) => (case eval_expr_step xpr1 env rest of
                        | NONE       => NONE
                        | SOME xpr1' => SOME (Gt xpr1' xpr2))
      | (R::rest) => (case eval_expr_step xpr2 env rest of
                        | NONE       => NONE
                        | SOME xpr2' => SOME (Gt xpr1 xpr2'))
      | _         => NONE) ∧
  (eval_bexpr_step (Eq (Const num1) (Const num2)) env [] =
    if num1 = num2 then SOME True else SOME False) ∧
  (eval_bexpr_step (Eq xpr1 xpr2) env posn =
    case posn of
      | (L::rest) => (case eval_expr_step xpr1 env rest of
                        | NONE       => NONE
                        | SOME xpr1' => SOME (Eq xpr1' xpr2))
      | (R::rest) => (case eval_expr_step xpr2 env rest of
                        | NONE       => NONE
                        | SOME xpr2' => SOME (Eq xpr1 xpr2'))
      | _         => NONE) ∧
  (eval_bexpr_step _ _ _ = NONE)
`
(* end boolean level *)


(* start program level *)
val eval_step_def = Define`
  (eval_step (Assign str xpr) env posn =
    case posn of
      | []        => (case xpr of
                        | Const c => SOME (Skip, update_env (str, c) env)
                        | _       => NONE)
      | (_::rest) => (case eval_expr_step xpr env rest of
                        | NONE      => NONE
                        | SOME xpr' => SOME (Assign str xpr', env))) ∧
  (eval_step (Free str) env [] =
    case remove_env str env of
      | NONE      => NONE
      | SOME env' => SOME (Skip, env')) ∧
  (eval_step (Parallel l r) env posn =
    case posn of
      | []        => if (l = Skip) ∧ (r = Skip) then SOME (Skip, env) else NONE
      | (L::rest) => (case eval_step l env rest of
                        | NONE            => NONE
                        | SOME (l', env') => SOME (Parallel l' r, env'))
      | (R::rest) => (case eval_step r env rest of
                        | NONE            => NONE
                        | SOME (r', env') => SOME (Parallel l r', env'))) ∧
  (eval_step (Elf g stmt1 stmt2) env posn =
    case posn of
      | []        => (case g of
                        | True  => SOME (stmt1, env)
                        | False => SOME (stmt2, env)
                        | _     => NONE)
      | (_::rest) => (case eval_bexpr_step g env rest of
                        | NONE     => NONE
                        | SOME xpr => SOME (Elf xpr stmt1 stmt2, env))) ∧
  (eval_step (While g b) env posn =
    case posn of
      | [] => SOME (Elf g (Seq b (While g b)) Skip, env)
      | _  => NONE) ∧
  (eval_step (Seq stmt1 stmt2) env posn =
    case posn of
      | []        => if stmt1 = Skip then SOME (stmt2, env) else NONE
      | (_::rest) => (case eval_step stmt1 env rest of
                        | NONE                => NONE
                        | SOME (stmt1', env') => SOME (Seq stmt1' stmt2, env'))) ∧
  (eval_step _ _ _ = NONE)
`
(* end program level *)
(* --- *)


(* Schedule evaluation *)
val eval_scheduler_def = Define`
  eval_scheduler stmt env schedule =
   case schedule of
     | []          => SOME (stmt, env)
     | (sch::rest) => (case eval_step stmt env sch of
                         | NONE               => NONE
                         | SOME (stmt', env') => eval_scheduler stmt' env' rest)
`
(* --- *)

val _ = export_theory ()
