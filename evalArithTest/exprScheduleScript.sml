(* 
   Scheduler script which finds valid schedules in a program
   Alexander Soen
*)

open HolKernel bossLib Parse boolLib;
open pairTheory listTheory stringTheory alistTheory;
open exprEvalTheory;

val _ = new_theory "exprSchedule";

(* finds the steps for expr stmts *)
val find_expr_steps_def = Define`
  (find_expr_steps (Var str) env step = if (ALOOKUP env str = NONE) then NONE else SOME [step]) ∧
  (find_expr_steps ((Const _) +_ (Const _)) _ step = SOME [step]) ∧
  (find_expr_steps ((Const _) ×_ (Const _)) _ step = SOME [step]) ∧
  (find_expr_steps (lexpr +_ rexpr) env step =
    case (find_expr_steps lexpr env (step ++ [L]), find_expr_steps rexpr env (step ++ [R])) of
      | (NONE     , NONE     ) => NONE
      | (SOME res1, NONE     ) => SOME res1
      | (NONE     , SOME res2) => SOME res2
      | (SOME res1, SOME res2) => SOME (res1 ++ res2)) ∧
  (find_expr_steps (lexpr ×_ rexpr) env step =
    case (find_expr_steps lexpr env (step ++ [L]), find_expr_steps rexpr env (step ++ [R])) of
      | (NONE     , NONE     ) => NONE
      | (SOME res1, NONE     ) => SOME res1
      | (NONE     , SOME res2) => SOME res2
      | (SOME res1, SOME res2) => SOME (res1 ++ res2)) ∧
  (find_expr_steps _ _ _ = NONE)
`

(* finds the steps for bexpr stmts *)
val find_bexpr_steps_def = Define`
  (find_bexpr_steps ((Const _) <_ (Const _)) _ step = SOME [step]) ∧
  (find_bexpr_steps ((Const _) >_ (Const _)) _ step = SOME [step]) ∧
  (find_bexpr_steps ((Const _) =_ (Const _)) _ step = SOME [step]) ∧
  (find_bexpr_steps (lexpr <_ rexpr) env step =
    case (find_expr_steps lexpr env (step ++ [L]), find_expr_steps rexpr env (step ++ [R])) of
      | (NONE     , NONE     ) => NONE
      | (SOME res1, NONE     ) => SOME res1
      | (NONE     , SOME res2) => SOME res2
      | (SOME res1, SOME res2) => SOME (res1 ++ res2)) ∧
  (find_bexpr_steps (lexpr >_ rexpr) env step =
    case (find_expr_steps lexpr env (step ++ [L]), find_expr_steps rexpr env (step ++ [R])) of
      | (NONE     , NONE     ) => NONE
      | (SOME res1, NONE     ) => SOME res1
      | (NONE     , SOME res2) => SOME res2
      | (SOME res1, SOME res2) => SOME (res1 ++ res2)) ∧
  (find_bexpr_steps (lexpr =_ rexpr) env step =
    case (find_expr_steps lexpr env (step ++ [L]), find_expr_steps rexpr env (step ++ [R])) of
      | (NONE     , NONE     ) => NONE
      | (SOME res1, NONE     ) => SOME res1
      | (NONE     , SOME res2) => SOME res2
      | (SOME res1, SOME res2) => SOME (res1 ++ res2)) ∧
  (find_bexpr_steps _ _ _ = NONE)
`

(* finds the steps for general stmts *)
val find_steps_def = Define`
  (find_steps (Assign _ (Const _)) _ step = SOME [step]) ∧
  (find_steps (Free str) env step = if (remove_env str env = NONE) then NONE else SOME [step]) ∧
  (find_steps (Skip || Skip) _ step = SOME [step]) ∧
  (find_steps (Elf True _ _) _ step = SOME [step]) ∧
  (find_steps (Elf False _ _) _ step = SOME [step]) ∧
  (find_steps (While _ _) _ step = SOME [step]) ∧
  (find_steps (Skip ;; _) _ step = SOME [step]) ∧
  (find_steps (Assign _ expr) env step = find_expr_steps expr env (step ++ [N])) ∧
  (find_steps (stmt ;; _) env step = find_steps stmt env (step ++ [N])) ∧
  (find_steps (Elf bexpr _ _) env step = find_bexpr_steps bexpr env (step ++ [N])) ∧
  (find_steps (lstmt || rstmt) env step =
    case (find_steps lstmt env (step ++ [L]), find_steps rstmt env (step ++ [R])) of
      | (NONE     , NONE     ) => NONE
      | (SOME res1, NONE     ) => SOME res1
      | (NONE     , SOME res2) => SOME res2
      | (SOME res1, SOME res2) => SOME (res1 ++ res2)) ∧
  (find_steps _ _ _ = NONE)
`

(* evaluates the a program by n steps using the scheduler function *)
val eval_n_steps_def = Define`
  (eval_n_steps prog env 0 = SOME (prog, env)) ∧
  (eval_n_steps prog env (SUC n) =
     case find_steps prog env [] of
       | NONE               => NONE
       | SOME (schedule::_) => case eval_step prog env schedule of
                            | NONE     => NONE
                            | SOME res => eval_n_steps (FST res) (SND res) n)
`

(*
Not evaluating the OPTION_BIND function

val eval_n_steps_def = Define`
  (eval_n_steps prog env 0 = SOME (prog, env)) ∧
  (eval_n_steps Skip env _ = NONE) ∧
  (eval_n_steps prog env (SUC n) =
     case find_steps prog env [] of
       | NONE          => NONE
       | SOME schedule => (OPTION_BIND (eval_step prog env (HD schedule)) (λopt . eval_n_steps (FST opt) (SND opt) n)))
`
*)

(*
Example test case: EVAL ``eval_n_steps fac_prog [("n", 2)] 41``;

TODO:
prove ∀ p e. (find_steps p e [] = SOME steps) ⇒ ∃ s. eval_step p e s = SOME res
Trying to prove that the find_steps functions will not produce SOME []


g` ∀ prog env step_acc. (find_expr_steps prog env step_acc = SOME steps) ⇒ steps ≠ []`

e (Induct_on `prog`)
e (EVAL_TAC)
e (Cases_on `ALOOKUP env s`)

e (rw [])
e (rw [])

e (EVAL_TAC)
e (rw [])

e (`find_expr_steps (prog +_ prog') env [] = (
    case (find_expr_steps prog env ([L]), find_expr_steps prog' env ([R])) of
      | (NONE     , NONE     ) => NONE
      | (SOME res1, NONE     ) => SOME res1
      | (NONE     , SOME res2) => SOME res2
      | (SOME res1, SOME res2) => SOME (res1 ++ res2))` by rw [find_expr_steps_def])

e (Cases_on `find_expr_steps prog env ([L])`)
e (Cases_on `find_expr_steps prog' env ([R])`)
e (EVAL_TAC)

(**)

e (Cases_on `find_expr_steps prog env step_acc`)
e (Cases_on `find_expr_steps prog' env step_acc`)

e (metis_tac [find_expr_steps_def])



g` ∀ prog env. (find_steps prog env [] = SOME steps) ⇒ steps ≠ []`

g` ∀ p e. (find_steps p e [] = SOME steps) ⇒ eval_step p e (HD steps) = SOME res`

g` ∀ p e. (find_steps p e [] = SOME steps) ⇒ ∃ s. eval_step p e s = SOME res`


*)
val _ = export_theory ()
