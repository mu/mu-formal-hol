(*
   PP the translated functions
   Alexander Soen
*)

open HolKernel boolLib bossLib;
open ml_hol_exprProgTheory astPP;

val _ = new_theory "ppExpr";

(* Turns the translated functiosn into a single term which can be printed *)
val decls = ml_hol_exprProgTheory.expr_code_def |> concl |> rand

val _ = enable_astPP ()

val f = TextIO.openOut ("expr.ml.txt")

fun appthis tm = let
  val () = TextIO.output (f, term_to_string tm)
  val () = TextIO.output (f, "\n")
in () end

val _ = app appthis (fst (listSyntax.dest_list decls))

val _ = TextIO.closeOut f

val _ = export_theory ();
