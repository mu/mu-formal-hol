structure extraTactics :> extraTactics =
struct
  open HolKernel boolLib bossLib
  open Parse
  open Term

  val pair_case_tac =
    qpat_assum `_ (_: α # β) = _` (fn a =>
      let val v = concl a |> lhs |> dest_comb |> #2 in
        if pairSyntax.is_pair v then NO_TAC else Cases_on `^v`
      end) ORELSE
    qpat_assum `_ = _ (_: α # β)` (fn a =>
      let val v = concl a |> rhs |> dest_comb |> #2 in
        if pairSyntax.is_pair v then NO_TAC else Cases_on `^v`
      end)

  fun multi_case_tac x =
    (REPEAT ((BasicProvers.FULL_CASE_TAC ORELSE pair_case_tac) >> fs[] >> rw[])) x

  fun rw_assums xs =
    POP_ASSUM_LIST (map_every (assume_tac o REWRITE_RULE xs) o rev)

  fun rw_assums_once xs =
    POP_ASSUM_LIST (map_every (assume_tac o ONCE_REWRITE_RULE xs) o rev)
end

