open HolKernel bossLib

signature syntaxSugarLib =
sig
  val prefix_op : string -> unit

  val define_num_newtype : string * string * string -> unit
end

