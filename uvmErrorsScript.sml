open HolKernel Parse boolLib bossLib

open stringTheory
open monadsTheory
open monadsLib
open syntaxSugarTheory

val _ = new_theory "uvmErrors"

(* The "error" type is used with the sum type monad (in monadsTheory) to create
   a short-circuiting error monad used throughout the formalization.
   
   When undefined or impossible behavior occurs, the remainder of the execution
   is undefined, and the error monad short-circuits the execution and reports an
   error message for debugging purposes.
*)

val _ = Datatype`
  error =
  (* Undefined behavior has occurred, and the rest of the execution is unknown.
     The generic parameter allows more information about the undefined behavior
     to be included in the error object, for debugging purposes. *)
  | UndefBehavior 'undefined_type

  (* For code paths that should never be reachable by well-formed programs.
     Should be provable that no well-formed program can produce this error. *)
  | Impossible string

  (* For code paths that are not yet implemented. Should be removed in the final
     version of the formalization.  *)
  | NotImplemented string
`

val _ = overload_on("OK", ``INL : α -> α + ε error``)
val _ = overload_on("Fail", ``INR : ε error -> α + ε error``)
val _ = overload_on("✔", ``OK``)
val _ = overload_on("✘", ``Fail``)

(* The infix "or_fail" operator throws an error if the option on its left side
   is NONE. It can be combined with "assert", which converts a boolean to an
   option.
*)

(* tight_equality (450) < 455 < CONS (490) *)
val _ = set_fixity "or_fail" (Infix (NONASSOC, 455))

val _ = overload_on("or_fail", ``λx e. option_CASE x (Fail e) OK``)
val _ = overload_on("assert", ``OPTION_GUARD``)

(* The UndefBehavior error message can be mapped over, allowing different kinds
   of errors to be merged.
*)

val lift_err_msg_def = Define`
    lift_err_msg (f : 'm1 -> 'm2) (Fail (UndefBehavior msg) : α + 'm1 error)
                 : α + 'm2 error =
      Fail (UndefBehavior (f msg))
  ∧ lift_err_msg _ (Fail (Impossible msg)) = Fail (Impossible msg)
  ∧ lift_err_msg _ (Fail (NotImplemented msg)) = Fail (NotImplemented msg)
  ∧ lift_err_msg _ (OK x) = OK x
`

(* Because assert and or_fail are extremely common, theorems about their basic
   functionality are included in the default simpset.
*)

val or_fail_OK_proves = store_thm("or_fail_OK_proves[simp]",
  ``∀opt e. (opt or_fail e = OK x) = (opt = SOME x)``,
  Cases >> rw[])

val assert_OK_proves = store_thm("assert_OK_proves[simp]",
  ``∀P e. (assert P or_fail e = OK ()) = P``,
  Cases >> rw[])

val assert_Fail_proves = store_thm("assert_Fail_proves[simp]",
  ``∀P e e'. (assert P or_fail e = Fail e') ⇔ (e' = e ∧ ¬P)``,
  Cases >> rw[] >> prove_tac[])

val lift_err_msg_OK = store_thm("lift_err_msg_OK[simp]",
  ``∀x f y. (lift_err_msg f x = OK y) ⇔ (x = OK y)``,
  Cases >| [rw[lift_err_msg_def], Cases_on `y` >> rw[lift_err_msg_def]])

val lift_err_msg_Fail = store_thm("lift_err_msg_Fail[simp]",
  ``∀x y f. (lift_err_msg f x = Fail y)
          ⇔ (case y of
             | UndefBehavior m => (∃m'. m = f m' ∧ x = Fail (UndefBehavior m'))
             | Impossible m => x = Fail (Impossible m)
             | NotImplemented m => x = Fail (NotImplemented m))``,
  Cases >> Cases >> rw[lift_err_msg_def] >> Cases_on `y` >>
  simp[lift_err_msg_def] >> prove_tac[])

val _ = export_theory()

