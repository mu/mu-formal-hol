open HolKernel Parse boolLib bossLib

open monadsTheory
open uvmMemoryTheory
open uvmIRTheory
open uvmInstructionSemanticsTheory
open uvmThreadsStacksTheory
open uvmSchedulerTheory
open extraTactics

open alistTheory
open arithmeticTheory
open combinTheory
open listTheory
open optionTheory
open pairTheory
open pred_setTheory
open rich_listTheory
open sptreeTheory

val _ = new_theory "uvmOptimizations"
val _ = monadsyntax.add_monadsyntax()

(* LEMMAS *)

val sum_eq = store_thm("sum_eq",
  ``∀a b. (case a of INL l => b = INL l | INR r => b = INR r)
        ⇒ a = b``,
  Cases >> rw[])

val converse = ONCE_REWRITE_RULE [EQ_SYM_EQ]

fun with_match_abbrevs abbrevs pat tac x =
  (qmatch_abbrev_tac pat >>
   tac >> map_every qunabbrev_tac abbrevs) x

fun Cases_on_match abbrev pat =
  with_match_abbrevs [abbrev] pat (
    (`∃cases_on_match.` @ abbrev @ `= cases_on_match`) by simp[] >>
    Cases_on `cases_on_match`)

(* STATE DELTAS

   To make proofs simpler, updates to sched_states are described using "state
   delta" records that specify exactly which parts of the state were changed.

   This makes it easier to prove that updates to specific threads or stacks will
   not affect the execution of other threads or stacks.
*)

val _ = type_abbrev("spt_delta", ``:(α option -> α) spt``)

val spt_upd_def = Define`
  spt_upd (Δ : α spt_delta) (t : α spt) : α spt =
    union (map_with_keys (λk f. f (lookup k t)) Δ) t
`

val spt_upd_id = store_thm("spt_upd_id[simp]",
  ``spt_upd LN = I``,
  rw[FUN_EQ_THM, spt_upd_def, map_with_keys_def, toAList_def, fromAList_def,
     foldi_def])

val lookup_spt_upd = store_thm("lookup_spt_upd",
  ``∀Δ n t. lookup n (spt_upd Δ t) =
      OPTION_CHOICE (OPTION_MAP (λf. f (lookup n t)) (lookup n Δ))
                    (lookup n t)``,
  rw[spt_upd_def, lookup_union] >> CASE_TAC >>
  fs[lookup_map_with_keys])

val spt_upd_insert_commute = store_thm("spt_upd_insert_commute",
  ``∀Δ t n. n ∉ domain Δ ∧ wf t
    ⇒ ∀v. insert n v (spt_upd Δ t) = (spt_upd Δ (insert n v t))``,
  rw[] >>
  with_match_abbrevs [`a`, `b`] `a = b` (
    `wf a ∧ wf b` by
      metis_tac[wf_insert, wf_union, wf_map_with_keys, spt_upd_def]) >>
  rw[spt_eq_thm, lookup_insert, lookup_spt_upd] >>
  fs[converse lookup_NONE_domain] >> CASE_TAC >> rw[])

val wf_spt_upd = store_thm("wf_spt_upd",
  ``∀Δ t. wf t ⇒ wf (spt_upd Δ t)``,
  simp[spt_upd_def, wf_union, wf_map_with_keys])

val _ = Datatype`
  state_delta = <|
    threads : thread spt_delta;
    stacks : stack spt_delta;
    writes : memory;
    sched_tid : thread_id option;
    sched_addrs : addr spt
  |>
`

val id_state_delta_def = Define`
  id_state_delta : state_delta = <|
    threads := LN; stacks := LN; writes := new_address_space;
    sched_tid := NONE; sched_addrs := LN
  |>
`

val _ = overload_on("idΔ", ``id_state_delta``)

val state_upd_def = Define`
  state_upd (Δ : state_delta) (st : sched_state) : sched_state =
    st with <|
      threads updated_by spt_upd Δ.threads;
      stacks updated_by spt_upd Δ.stacks;
      memory updated_by address_space_union Δ.writes;
      sched_tid updated_by option_CASE Δ.sched_tid I K;
      sched_addrs updated_by union Δ.sched_addrs
    |>
`

(* Deltas can be composed and tested for equivalence. *)

val _ = set_fixity "delta_compose" (Infixr 800)
val _ = set_fixity "spt_delta_compose" (Infixr 800)
val _ = Unicode.unicode_version{tmnm="delta_compose", u="▵"}

val _ = set_fixity "delta_equiv" (Infix(NONASSOC, 450))
val _ = Unicode.unicode_version{tmnm="delta_equiv", u="≜"}

val spt_delta_compose_def = Define`
  (Δ1 : α spt_delta) spt_delta_compose (Δ2 : α spt_delta) : α spt_delta =
    let compose1 k f1 =
      case lookup k Δ2 of
      | SOME f2 => f1 o SOME o f2
      | NONE => f1
    in union (map_with_keys compose1 Δ1) Δ2
`

val delta_compose_def = Define`
  Δ1 delta_compose Δ2 = <|
    threads := Δ1.threads spt_delta_compose Δ2.threads;
    stacks := Δ1.stacks spt_delta_compose Δ2.stacks;
    writes := address_space_union Δ1.writes Δ2.writes;
    sched_tid := OPTION_CHOICE Δ1.sched_tid Δ2.sched_tid;
    sched_addrs := union Δ1.sched_addrs Δ2.sched_addrs
  |>
`

val delta_equiv_def = Define`
  Δ1 delta_equiv Δ2 ⇔ ∀st. state_ok st ⇒ state_upd Δ1 st = state_upd Δ2 st
`

val spt_delta_compose_upd = store_thm("spt_delta_compose_upd",
  `` ∀Δ1 Δ2 x. wf x ⇒ spt_upd (Δ1 spt_delta_compose Δ2) x = spt_upd Δ1 (spt_upd Δ2 x)``,
  rw[spt_upd_def, spt_delta_compose_def, union_assoc] >>
  with_match_abbrevs [`a`, `b`] `a = b` (
    `wf a ∧ wf b` by prove_tac[wf_union, wf_map_with_keys] >>
    `∀n. lookup n a = lookup n b` suffices_by prove_tac[spt_eq_thm]) >>
  rw[lookup_union, lookup_map_with_keys] >> REPEAT CASE_TAC >>
  fs[OPTION_MAP_DEF] >> rfs[])

(*
val delta_compose_upd = store_thm("delta_compose_upd",
  ``∀Δ1 Δ2 st. state_ok st
      ⇒ state_upd (Δ1 delta_compose Δ2) st = state_upd Δ1 (state_upd Δ2 st)``,
  rw[state_upd_def, delta_compose_def, state_ok_def, spt_delta_compose_upd,
     sched_state_component_equality, wf_union, union_assoc] >| [
    rw[address_space_union_def] >>
    `∃aw1. aw1 = addrs_of Δ1.writes` by simp[] >> pop_assum mp_tac >>
    `∃aw2. aw2 = addrs_of Δ2.writes` by simp[] >> pop_assum mp_tac >>
    qmatch_rename_tac `aw2 = addrs_of w2 ⇒ aw1 = addrs_of w1 ⇒ _` >>
    map_every qid_spec_tac [`w2`, `w1`, `aw2`, `aw1`] >> Induct >| [
      Induct >| [
        REPEAT (STRIP_GOAL_THEN (assume_tac o converse)) >> rw[],
        strip_tac >> REPEAT (STRIP_GOAL_THEN (assume_tac o converse)) >> rw[]
      ],
      strip_tac >> Induct >| [
        REPEAT (STRIP_GOAL_THEN (assume_tac o converse)) >> rw[],

      ]
    ]
  ]
*)

(* STEP DELTAS

   The goal here is to prove that, if two states differ only in one stack (as is
   the case in most optimizations), the states will remain different in only the
   same stack until an ExecInst step is performed on the thread that owns that
   stack.
*)
(*
val stack_delta_step = store_thm("stack_delta_step",
  ``∀step s1 s2 s1' s2' skΔ .
      state_ok s1 ∧ state_ok s2
    ∧ s2 = state_upd (idΔ with stacks := skΔ) s1
    ∧ (∀sid. sid ∈ domain skΔ
         ⇒ ∃tid th. get_thread tid s1 = SOME th
                  ∧ th.sid = StackID sid)
    ∧ (step ≠ ExecInst ∨
       ∀th. get_thread s1.sched_tid s1 = SOME th
         ⇒ stack_id_dest th.sid ∉ domain skΔ)
    ∧ s1' = try_step step s1
    ∧ s2' = try_step step s2
    ⇒ s2' = state_upd (idΔ with stacks := skΔ) <$> s1'``,
  ntac 6 strip_tac >> STRIP_GOAL_THEN assume_tac >>
  match_mp_tac sum_eq >> CASE_TAC >> simp[] >| [
    (* Case 1: OK *)
    CASE_TAC >> Cases_on `step` >> fs[try_step_def, take_step_def] >>
    ntac 2 (BasicProvers.every_case_tac >> fs[] >> rw[]) >>
    Cases_on `(HD sk'.frames).state` >> fs[] >>
    map_every (TRY o Cases_on)
      [`(HD sk.frames).state`, `(HD sk''.frames).state`, `l`, `l'`, `l''`] >>
    fs[]
    TRY (Cases_on `l` >> fs[])
    BasicProvers.every_case_tac >> fs[]

    (* Case 2: Fail *)
  ]
  disch_tac
  fs[state_upd_def, id_state_delta_def, try_step_def,
     take_step_def, addrs_of_def, new_address_space_def,
     toAList_def, foldi_def, nub_def, union_def] >| [
    match_mp_tac sum_eq >> CASE_TAC >> rw[] >| [
      fs[OPTION_BIND_EQUALS_OPTION, get_thread_def, state_upd_def,
         addrs_of_def, toAList_def, foldi_def, nub_def] >>
      BasicProvers.every_case_tac >> fs[] >| [
        BasicProvers.every_case_tac >>
        fs[exec_non_memory_action_def, exec_load_def, exec_store_def,
             exec_commit_def, exec_reconcile_def, exec_command_def] >>
        qexists_tac `s1` >> rw[]
      fs[bind_left_case] >> multi_case_tac
    ]
    fs[sumTheory.sum_CASES]
    fs[get_thread_def, get_stack_def]
    Cases_on `get_thread s1.sched_tid s1` >>
    fs[get_thread_def] >>
    simp[lift_left_def, state_upd_def, addrs_of_def,
         toAList_def, foldi_def, nub_def] >>
    Cases_on `get_stack x.sid s1` >>
    fs[get_stack_def, lookup_spt_upd, converse lookup_NONE_domain] >>
    multi_case_tac >>
    Cases_on_match `it` `_ = it` >> rw[] >>
    fs[state_upd_def, addrs_of_def, toAList_def, foldi_def, nub_def] >| [
      pop_assum (mp_tac o converse) >> pop_assum (assume_tac o converse) >>
      with_match_abbrevs [`a`, `b`] `a = b` (
        `a.stacks = b.stacks` by cheat
      ),
      fs[sched_state_component_equality]
    ]
  ]
*)

val isomorphic_states_def = Define`
    isomorphic_states (Fail (UndefBehavior _)) (Fail (UndefBehavior _)) = T
  ∧ isomorphic_states (Fail e1) (Fail e2) = (e1 = e2)
  ∧ isomorphic_states (OK s1) (OK s2) = (
     has_terminated s1 ∧ has_terminated s2
   ∧ s1.threads = s2.threads
   ∧ s1.memory = s2.memory
   ∧ (MAP (λ(_, sk). (HD sk.frames).state) (toAList s1.stacks)) =
     (MAP (λ(_, sk). (HD sk.frames).state) (toAList s2.stacks)))
  ∧ isomorphic_states _ _ = F
`

val future_states_def = Define`
  future_states st st' ⇔ ∃sc. run_finite_schedule sc st = st'
`

val final_states_def = Define`
  final_states st st' ⇔
    ∃sc. finite_schedule_terminates st sc ∧
         run_finite_schedule sc st = st'
`

val state_where_thread_def = Define`
  state_where_thread (tid : thread_id) (P : thread -> stack -> bool) st ⇔
    ∃th sk. get_thread tid st = SOME th
          ∧ get_stack th.sid st = SOME sk
          ∧ P th sk
`

val state_subset_def = Define`
  state_subset s1 s2 ⇔
    ∀s1'. s1' ∈ final_states s1
    ⇒ ∃s2'. s2' ∈ final_states s2 ∧ isomorphic_states s1' s2'
`

val isomorphic_states_refl = store_thm("isomorphic_states_refl",
  ``∀st. (ISL st ⇒ has_terminated (OUTL st)) ⇒ isomorphic_states st st``,
  Cases >- simp[isomorphic_states_def] >>
  Cases_on `y` >> simp[isomorphic_states_def])

val state_subset_refl = store_thm("state_subset_refl",
  ``∀st. state_subset st st``,
  rw[state_subset_def, IN_APP, final_states_def, finite_schedule_terminates_def,
     merge_left_def] >>
  qexists_tac `run_finite_schedule sc st` >> rw[] >| [
    qexists_tac `sc` >> rw[],
    multi_case_tac >> simp[isomorphic_states_refl]
  ])

val upd_top_frame_def = Define`
  upd_top_frame f sk =
    let fs = sk.frames in do
      hd' <- f (HD fs);
      OK (sk with frames := hd'::TL fs)
    od
`

val equivalent_code_def = Define`
  equivalent_code (program_predicate : program -> opn_block -> bool)
                  (block_predicate : bblock -> bool)
                  (transform_block : bblock -> bblock) ⇔
  ∀(prg : program) (block : bblock) (lbl : block_label -> num).
    let cb = compile_block lbl block;
        block' = transform_block block;
        cb' = compile_block lbl block'
    in program_predicate prg cb
     ∧ block_predicate block
     ∧ (∃fn bl. lookup bl (prg ' fn) = SOME cb)
     ⇒ (∀orig_st st st' th orig_sk sk sk' args exc.
          state_ok orig_st
        ∧ orig_st.program = prg
        ∧ SOME th = get_thread st.sched_tid orig_st
        ∧ SOME orig_sk = get_stack th.sid orig_st
        ∧ OK sk  = upd_top_frame (enter_block cb  args exc) orig_sk
        ∧ OK sk' = upd_top_frame (enter_block cb' args exc) orig_sk
        ∧ st  = (orig_st with stacks updated_by
                   insert (stack_id_dest th.sid) sk)
        ∧ st' = (orig_st with stacks updated_by
                   insert (stack_id_dest th.sid) sk')
        ⇒ state_subset st' st)
`

val mem_inst_types_lemma = prove(
  ``∀m n s xs.
      m < n ∧ n ≤ LENGTH xs ∧ MEM s (MAP FST (inst_types (EL m xs)))
    ⇒ MEM s (MAP FST (TAKE n xs >>= inst_types))``,
  rw[] >> fs[LIST_BIND_def, MEM_FLAT, MEM_MAP] >>
  map_every (fn x => qexists_tac x >> rw[])
    [`y`, `inst_types (EL m xs)`, `EL m xs`] >>
  metis_tac[MEM_TAKE, MEM_EL, EL_TAKE, LENGTH_TAKE])

val every_nub_lemma = prove(
  ``∀xs f. EVERY f (nub xs) ⇔ EVERY f xs``,
  Induct >> rw[nub_def] >> metis_tac[EVERY_MEM])

val mem_prefix_lemma = prove(
  ``∀v m n b ys. 
      m ≤ n ∧ n ≤ LENGTH ys ∧ EVERY inst_ok ys
    ∧ (MEM v (MAP FST (block_arg_types b)) ∨ MEM v (TAKE m ys >>= inst_output_vars))
    ⇒ MEM v (MAP FST (block_arg_types b ++ TAKE n ys >>= inst_types))``,
  rw[] >- simp[] >> DISJ2_TAC >>
  `MEM v (MAP FST (TAKE m ys >>= inst_types))` by
    fs[inst_types_output_vars_bind, EVERY_TAKE] >>
  `m ≤ LENGTH ys` by simp[] >>
  fs[LIST_BIND_def, MEM_FLAT, MEM_MAP] >> rw[] >>
  map_every (fn x => qexists_tac x >> rw[]) [`y'`, `inst_types y''`, `y''`] >>
  fs[MEM_EL] >> rw[] >> qexists_tac `n'''` >> rw[EL_TAKE])

val eq_ssa_map_for_def = Define`
  eq_ssa_map_for (xs : ssavar list) tys1 tys2 ⇔
    ∀s. MEM s xs ⇒ ssa_map tys1 s = ssa_map tys2 s
`

val eq_ssa_map_lemma = prove(
  ``∀m n b is vs. 
      m ≤ n ∧ n ≤ LENGTH is ∧ EVERY inst_ok is
    ∧ EVERY (λv. MEM v (MAP FST (block_arg_types b))
               ∨ MEM v (TAKE m is >>= inst_output_vars)) vs
    ⇒ ∀suf. eq_ssa_map_for vs
              (block_arg_types b ++ TAKE n is >>= inst_types)
              (block_arg_types b ++ TAKE n is >>= inst_types ++ suf)``,
  rw[eq_ssa_map_for_def, EVERY_MEM] >> res_tac >> fs[GSYM EVERY_MEM] >>
  metis_tac[mem_prefix_lemma, ssa_map_prefix])

val eval_expr_eq_ssa_map = prove(
  ``∀e s1 s2. 
      eq_ssa_map_for (expr_vars e >>= var_list) s1 s2
    ⇒ eval_expr (ssa_map s1) e = eval_expr (ssa_map s2) e``,
  Cases >> TRY (Cases_on `c`) >>
  rw[eval_expr_def, eq_ssa_map_for_def, expr_vars_def, nub_def] >>
  map_every (TRY o Cases_on) [`s`, `s0`] >>
  fs[var_list_def, getc_def, get_def, ssa_regs_def])

val compile_inst_eq_ssa_map = prove(
  ``∀i s1 s2. 
      eq_ssa_map_for (inst_all_vars i) s1 s2
    ⇒ compile_inst (ssa_map s1) i = compile_inst (ssa_map s2) i``,
  Cases >>
  rw[eq_ssa_map_for_def, compile_inst_def, inst_all_vars_def, inst_vars_def]
  >- (`MAP (ssa_regs (ssa_map s1)) l = MAP (ssa_regs (ssa_map s2)) l`
        suffices_by metis_tac[eval_expr_eq_ssa_map, eq_ssa_map_for_def,
                              LIST_BIND_def] >>
      match_mp_tac LIST_EQ >> rw[EL_MAP, EL_MEM, ssa_regs_def]) >>
  fs[get_def, ssa_regs_def]
  >- (multi_case_tac >> match_mp_tac LIST_EQ >>
      fs[MAP2_ZIP, EL_MAP, UNCURRY, LENGTH_MAP, LENGTH_ZIP] >>
      rw[lift_reg_reader_def, apply_reg_reader_thm] >> multi_case_tac >>
      rw[FUN_EQ_THM] >> Cases_on `s` >> fs[getc_def, get_def, ssa_regs_def] >>
      rfs[EL_ZIP, LENGTH_MAP, EL_MAP, var_list_def] >> rw[])
  >- (multi_case_tac >> Cases_on `s` >>
      fs[getc_def, get_def, ssa_regs_def, var_list_def])
  >- (multi_case_tac >> Cases_on `s` >>
      fs[getc_def, get_def, ssa_regs_def, var_list_def]) >>
  rw[lift_reg_reader_def, apply_reg_reader_thm] >> multi_case_tac >>
  rw[FUN_EQ_THM] >> Cases_on `s0` >>
  rfs[getc_def, get_def, ssa_regs_def, var_list_def])

val compile_block_take_eq = store_thm("compile_block_take_eq",
  ``∀n block.
      n ≤ LENGTH block.body ∧ block_ok block
    ⇒ ∀suffix.
        TAKE n block.body >>= compile_inst (ssa_map
          (block_arg_types block ++ TAKE n block.body >>= inst_types ++ suffix))
      = TAKE n block.body >>= compile_inst (ssa_map
          (block_arg_types block ++ TAKE n block.body >>= inst_types))``,
  rw[LIST_BIND_def, block_ok_def] >>
  match_mp_tac (prove(``∀x y. x = y ⇒ FLAT x = FLAT y``, simp[])) >>
  match_mp_tac LIST_EQ >> rw[EL_MAP] >> fs[GSYM LIST_BIND_def] >>
  rw[EQ_SYM_EQ] >> match_mp_tac compile_inst_eq_ssa_map >>
  match_mp_tac eq_ssa_map_lemma >> qexists_tac `SUC x` >> rw[] >>
  fs[inst_all_vars_def, inst_input_vars_def, UNCURRY, every_nub_lemma,
     EL_TAKE] >> `x ≤ LENGTH block.body` by simp[] >> rw[] >| [

    `∀v. MEM v (TAKE x block.body) ⇒ MEM v (TAKE (SUC x) block.body)`
      suffices_by (rw[] >> fs[EVERY_MEM, LIST_BIND_def, MEM_FLAT, MEM_MAP] >>
                   metis_tac[FST]) >>
    rw[MEM_EL] >> qexists_tac `n'` >> simp[EL_TAKE],

    fs[EVERY_MEM] >> rw[] >> DISJ2_TAC >> fs[MEM_FLAT,MEM_MAP,LIST_BIND_def] >>
    qexists_tac `inst_output_vars (EL x block.body)` >>
    `inst_ok (EL x block.body)` by (
      `x < LENGTH block.body` by simp[] >> metis_tac[MEM_EL]) >>
    rw[] >| [
      qexists_tac `EL x block.body` >> `SUC x ≤ LENGTH block.body` by simp[] >>
      simp[MEM_EL] >> qexists_tac `x` >> simp[EL_TAKE],

      metis_tac[inst_output_vars_def, inst_types_output_vars]
    ]])

(* When a block is modified by inserting new instructions, the compiled version
   of the unmodified block can be split into two parts (pre- and post-insertion)
   and the compiled version of the modified block can be split into three:

   1. pre-insertion, which is the same as the unmodified pre-insertion code

   2. inserted code
   
   3. post-insertion, which is equal-upto-register-renaming to the unmodified
      post-insertion code
*)
val partition_compile_block_insert = store_thm("partition_compile_block_insert",
  ``∀block lbl is n cb cb'.
      n ≤ LENGTH block.body ∧ block_ok block
    ∧ cb = compile_block lbl block
    ∧ cb' = compile_block lbl
        (block with body updated_by λb. TAKE n b ++ is ++ DROP n b)
    ⇒ ∃pre ins post reg_map.
        cb.opns = pre ++ post
      ∧ cb'.opns = pre ++ ins ++ MAP (map_opn_regs reg_map) post
      ∧ ∀as e f0 f f' os os' rs rs'.
          enter_block cb as e f0 = OK f
        ∧ enter_block cb' as e f0 = OK f'
        ∧ f.state = ACTIVE os rs
        ∧ f'.state = ACTIVE os' rs'
        ⇒ equiv_renamed_spt ($> (LENGTH cb.regs)) reg_map rs rs'``,
  rw[compile_block_def, UNCURRY] >>
  map_every qexists_tac [
    (* pre *)
    `TAKE n block.body >>= compile_inst (ssa_map (block_types block))`,
    (* ins *)
    `is >>= compile_inst (ssa_map (block_types
      (block with body updated_by λb. TAKE n b ++ is ++ DROP n b)))`,
    (* post *)
    `DROP n block.body >>= compile_inst (ssa_map (block_types block)) ++
      compile_terminst lbl (ssa_map (block_types block)) block.exit`,
    (* reg_map *)
    `ARB`
  ] >> rw[GSYM LIST_BIND_APPEND] >| [
    rw[LIST_BIND_def, MAP_FLAT, FLAT_APPEND, MAP_MAP_o, o_DEF] >>
    qmatch_abbrev_tac `a1 ++ b ++ c1 ++ d1 = a2 ++ b ++ c2 ++ d2` >>
    `a1 = a2 ∧ c1 = c2 ∧ d1 = d2` suffices_by simp[] >>
    rw(map Abbr [`a1`, `a2`, `b`, `c1`, `c2`, `d1`, `d2`] @ [GSYM LIST_BIND_def])
    >| [
      `block_arg_types (block with body updated_by
                         (λb. TAKE n b ++ is ++ DROP n b))
       = block_arg_types block` by simp[block_arg_types_def] >>
      rw[block_types_def] >>
      metis_tac[block_types_def, TAKE_DROP, LIST_BIND_APPEND, APPEND_ASSOC,
                compile_block_take_eq],

      (* need equality-upto-renaming for this *)
      cheat,

      (* need equality-upto-renaming for this *)
      cheat
    ],

    cheat
  ])

(* All non-ExecInst steps do not modify stacks. *)
val non_ExecInst_preserves_stack_delta = store_thm(
  "non_ExecInst_preserves_stack_delta",
  ``∀step skΔ st1 st2.
      st2 = state_upd (idΔ with stacks := skΔ) st1
    ∧ step ≠ ExecInst
    ⇒ take_step step st2 =
      liftM (liftM (state_upd (idΔ with stacks := skΔ))) (take_step step st1)``,
  Cases >> rw[
    state_upd_def, id_state_delta_def, new_address_space_def, merge_empty,
    o_DEF, address_space_union_def, lift_left_def, lift_option_def,
    OPTION_BIND_def, take_step_def, sched_state_component_equality,
    address_space_component_equality, get_thread_def, sched_addr_def,
    buffer_dequeue_def
  ] >| [
    rw[next_thread_def],
    rw[next_addr_def, FIND_def] >> multi_case_tac >>
      rfs[INDEX_FIND_def, NULL_EQ, get_thread_def] >> fs[] >> rw[sched_addr_def],
    Cases_on_match `a` `a = _` >> rw[] >> fs[] >> multi_case_tac >>
      `st2.memory = st1.memory` by prove_tac[address_space_component_equality] >>
      rw[bind_left_case] >> multi_case_tac >>
      fs[address_space_union_def, merge_empty,
         sched_state_component_equality, address_space_component_equality],
    Cases_on_match `a` `a = _` >> rw[] >> fs[] >> multi_case_tac >>
      fs[address_space_union_def, merge_empty,
         sched_state_component_equality, address_space_component_equality]
  ])

(* ExecInst does not modify any stack other than the stack bound to the
   scheduler's current thread.
*)
val ExecInst_preserves_stack_delta = store_thm(
  "ExecInst_preserves_stack_delta",
  ``∀skΔ st1 st2.
      wf st1.stacks
    ∧ st2 = state_upd (idΔ with stacks := skΔ) st1
    ∧ (∀th. get_thread st1.sched_tid st1 = SOME th
          ⇒ stack_id_dest th.sid ∉ domain skΔ)
    ⇒ take_step ExecInst st2 =
      liftM (liftM (state_upd (idΔ with stacks := skΔ))) (take_step ExecInst st1)``,
  rw[state_upd_def, id_state_delta_def, o_DEF, address_space_union_empty,
     lift_left_def, lift_option_def, OPTION_BIND_def, take_step_def,
     get_thread_def, get_stack_def, sched_addr_def,
     converse lookup_NONE_domain] >>
  Cases_on_match `a` `a = _` >> rw[] >> fs[OPTION_BIND_def, lookup_spt_upd] >>
  pop_assum mp_tac >> ntac 2 CASE_TAC >> ntac 2 (rfs[] >> rw[]) >>
  multi_case_tac >>
  fs[exec_non_memory_action_def, exec_load_def, exec_store_def,
     exec_commit_def, exec_reconcile_def, exec_command_def] >>
  pop_assum (assume_tac o converse) >> rw[] >>
  rw[bind_left_case] >> multi_case_tac >>
  rw[sched_state_component_equality] >> ntac 2 (rw[Once insert_insert]) >>
  fs[lookup_NONE_domain, spt_upd_insert_commute] >>
  fs[bind_left_case] >> multi_case_tac)

val advances_pc_of_def = Define`
  (advances_pc_of (sid : stack_id) ExecInst (st : sched_state) ⇔
     ∃th. get_thread st.sched_tid st = SOME th ∧ th.sid = sid) ∧
  (advances_pc_of _ _ _ ⇔ F)
`

val not_advances_pc_of_preserves_stack_delta = store_thm(
  "not_advances_pc_of_preserves_stack_delta",
  ``∀step sid Δ st1 st2.
      wf st1.stacks
    ∧ ¬has_terminated st1
    ∧ ¬has_terminated st2
    ∧ ¬advances_pc_of (StackID sid) step st1
    ∧ st2 = state_upd (idΔ with stacks := insert sid Δ LN) st1
    ⇒ try_step step st2 =
      state_upd (idΔ with stacks := insert sid Δ LN) <$> try_step step st1``,
  ntac 5 strip_tac >> Cases_on `step = ExecInst` >> DISCH_TAC >>
  rfs[advances_pc_of_def] >| [
    `∀th. get_thread st1.sched_tid st1 = SOME th
        ⇒ stack_id_dest th.sid ∉ domain (insert sid Δ LN)`
        by (rw[] >> fs[] >> Cases_on `th.sid` >> fs[stack_id_dest_def]) >>
      imp_res_tac ExecInst_preserves_stack_delta >>
      fs[try_step_def, lift_left_def, lift_option_def, OPTION_BIND_def, o_DEF] >>
      multi_case_tac,

    imp_res_tac non_ExecInst_preserves_stack_delta >>
      fs[try_step_def, lift_left_def, lift_option_def, OPTION_BIND_def, o_DEF] >>
      multi_case_tac
  ])

(*
val active_frame_delta_def = Define`
  active_frame_delta (sid : stack_id)
                     (opns : opn list)
                     (regs_add : mem_region)
                     (regs_del : one spt)
                     : state_delta =
    let f opt =
      let sk = THE opt; fr = HD sk.frames in
      sk with frames := (
        (fr with <|
          state := case fr.state of
                   | ACTIVE _ => ACTIVE opns
                   | _ => fr.state;
          regs updated_by λr. union regs_add (mk_wf (difference r regs_del))
        |>) :: TL sk.frames)
    in idΔ with stacks := insert (stack_id_dest sid) f LN
`
*)

val equiv_frames_def = Define`
  equiv_frames (opns1 : opn list)
               (opns2 : opn list)
               (same_regs : num set)
               (f1 : frame)
               (f2 : frame) ⇔
  ∃regs1 regs2.
    f1.id = f2.id
  ∧ f1.fn = f2.fn
  ∧ f1.state = ACTIVE opns1 regs1
  ∧ f2.state = ACTIVE opns2 regs2
  ∧ ∀r. r ∈ same_regs ⇒ lookup r regs1 = lookup r regs2
`

val equiv_upto_top_frame_def = Define`
  equiv_upto_top_frame (sid : stack_id)
                       (opns1 : opn list)
                       (opns2 : opn list)
                       (same_regs : num set)
                       (st1 : sched_state)
                       (st2 : sched_state) ⇔
    st1.program = st2.program
  ∧ st1.memory = st2.memory
  ∧ st1.sched_tid = st2.sched_tid
  ∧ (∀n. lookup n st1.sched_addrs = lookup n st2.sched_addrs)
  ∧ (∀tid. get_thread tid st1 = get_thread tid st2)
  ∧ (∀sid'. sid ≠ sid' ⇒ get_stack sid' st1 = get_stack sid' st2)
  ∧ ∃sk1 sk2.
      get_stack sid st1 = SOME sk1
    ∧ get_stack sid st2 = SOME sk2
    ∧ ¬NULL (sk1.frames)
    ∧ ¬NULL (sk2.frames)
    ∧ equiv_frames opns1 opns2 same_regs (HD sk1.frames) (HD sk2.frames)
    ∧ TL sk1.frames = TL sk2.frames
    ∧ sk1.next_frame = sk2.next_frame
`

val enter_block_equiv_frames = store_thm("enter_block_equiv_frames",
  ``∀f f1 f2 b1 b2 args exc.
      b1.regs = b2.regs
    ∧ b1.exc_param = b2.exc_param
    ∧ enter_block b1 args exc f = OK f1
    ∧ enter_block b2 args exc f = OK f2
    ⇒ equiv_frames b1.opns b2.opns univ(:num) f1 f2``,
  rw[enter_block_def, equiv_frames_def] >> rfs[])

val enter_block_equiv_upto_top_frame = store_thm(
  "enter_block_equiv_upto_top_frame",
  ``∀sid st sk b1 b2 args exc st1 st2 sk1 sk2.
      get_stack sid st = SOME sk
    ∧ b1.regs = b2.regs
    ∧ b1.exc_param = b2.exc_param
    ∧ OK sk1 = upd_top_frame (enter_block b1 args exc) sk
    ∧ OK sk2 = upd_top_frame (enter_block b2 args exc) sk
    ∧ st1 = (st with stacks updated_by insert (stack_id_dest sid) sk1)
    ∧ st2 = (st with stacks updated_by insert (stack_id_dest sid) sk2)
    ⇒ equiv_upto_top_frame sid b1.opns b2.opns univ(:num) st1 st2``,
  rw[equiv_upto_top_frame_def, upd_top_frame_def, get_thread_def, get_stack_def] >>
  fs[lookup_insert] >> rw[] >| [
    Cases_on `sid` >> Cases_on `sid'` >> fs[stack_id_dest_def],
    prove_tac[enter_block_equiv_frames]
  ])

val equiv_states_def = Define`
  equiv_states (st1 : sched_state) (st2 : sched_state) ⇔
    st1.program = st2.program
  ∧ st1.memory = st2.memory
  ∧ st1.sched_tid = st2.sched_tid
  ∧ (∀n. lookup n st1.sched_addrs = lookup n st2.sched_addrs)
  ∧ (∀tid. get_thread tid st1 = get_thread tid st2)
  ∧ (∀sid. get_stack sid st1 = get_stack sid st2)
`

val equiv_states_refl = store_thm("equiv_states_refl",
  ``∀st. equiv_states st st``,
  rw[equiv_states_def])

(* This takes a LONG time to compute, and it doesn't print anything while it's
   working, so it looks like it's frozen... but it does compile, just give it
   time!
*)
(*
val advances_pc_of_same_opn_equiv_upto_top_frame = store_thm(
  "advances_pc_of_same_opn_equiv_upto_top_frame",
  ``∀step op ops1 ops2 sid st1 st2.
      advances_pc_of sid step st1
    ∧ equiv_upto_top_frame sid (op::ops1) (op::ops2) univ(:num) st1 st2
    ∧ IS_SOME (take_step step st1)
    ∧ IS_SOME (take_step step st2)
    ⇒ merge_left (K (try_step step st1 = try_step step st2))
        (equiv_states <$> try_step step st1 <*> try_step step st2)
    ∨ equiv_upto_top_frame sid ops1 ops2 univ(:num)
        (OUTL (try_step step st1)) (OUTL (try_step step st2))``,
  Cases >> rw[advances_pc_of_def, equiv_upto_top_frame_def, equiv_frames_def] >>
  `¬has_terminated st1 ∧ ¬has_terminated st2` by (
    rw[has_terminated_def, thread_current_frame_state_def, EXISTS_MEM] >>
    qexists_tac `(thread_id_dest st1.sched_tid, th)` >> rw[] >>
    multi_case_tac >> fs[MEM_toAList, get_thread_def, thread_id_dest_def] >>
    rw[] >> fs[] >> rw[] >> fs[NULL_DEF]) >>
  rw[merge_left_def, try_step_def, lift_left_def, ap_left_def] >>
  multi_case_tac >> rfs[take_step_def, get_thread_def, get_stack_def] >| [
    (* OK *)
    Cases_on `op` >> fs[] >| [
      (* Nm *)
      DISJ2_TAC >> fs[exec_non_memory_action_def, run_reg_reader_stack_def] >>
      rfs[run_reg_reader_def] >> rw[RIGHT_EXISTS_AND_THM] >>
      assume_tac set_registers_preserves >>
      fs[preserves_def, propagates_def, FORALL_AND_THM] >> res_tac >> fs[] >>
      rw[lookup_insert, insert_insert] >| [
        rw[lookup_insert] >> map_every Cases_on [`th.sid`, `sid'`] >>
        fs[stack_id_dest_def],

        rw[converse RIGHT_EXISTS_AND_THM] >> match_mp_tac set_registers_lookup >>
        map_every qexists_tac [`rs`,
          `sk1 with frames updated_by (λf. (HD f with state := (HD sk'.frames).state)::TL f)`,
          `sk2 with frames updated_by (λf. (HD f with state := (HD sk''.frames).state)::TL f)`,
          `regs1`, `regs2`] >> fs[]
      ],

      (* Ld *)
      DISJ2_TAC >> fs[exec_load_def, run_reg_reader_stack_def, set_registers_def] >>
      rfs[run_reg_reader_def] >>
      rw[RIGHT_EXISTS_AND_THM, lookup_insert, Once insert_insert] >| [
        rw[lookup_insert] >> map_every Cases_on [`th.sid`, `sid'`] >>
        fs[stack_id_dest_def],

        fs[mem_region_set_value_def, UNCURRY] >> rw[lookup_insert] >> rfs[]
      ],

      (* St *)
      DISJ2_TAC >> fs[exec_store_def, run_reg_reader_stack_def, UNCURRY] >>
      rfs[run_reg_reader_def] >> rw[lookup_insert, lookup_map] >> fs[] >>
      rw[lookup_insert] >> map_every Cases_on [`th.sid`, `sid'`] >>
      fs[stack_id_dest_def],
    
      (* Com *)
      DISJ2_TAC >> fs[exec_commit_def] >> rw[lookup_insert] >>
      map_every Cases_on [`th.sid`, `sid'`] >> fs[stack_id_dest_def],

      (* Rec *)
      DISJ2_TAC >> fs[exec_reconcile_def] >> rw[lookup_insert] >>
      map_every Cases_on [`th.sid`, `sid'`] >> fs[stack_id_dest_def],

      (* Cmd *)
      fs[exec_command_def, run_reg_reader_stack_def] >> rfs[run_reg_reader_def] >>
      Cases_on `cmd` >> qpat_x_assum `_ = cmd'` (fn x => fs[converse x]) >| (
        let
          val eq_tac =
            DISJ1_TAC >> fs[push_stack_def, pop_stack_def, resume_stack_def,
              jump_def, enter_block_def, UNCURRY, equiv_states_def,
              get_thread_def, get_stack_def] >>
            multi_case_tac >> rfs[] >> rw[] >> fs[] >> rfs[] >>
            rw[lookup_insert, insert_insert, stack_component_equality,
               frame_component_equality] >> FIRST [metis_tac[], rfs[]]
          val upd_tac =
            DISJ2_TAC >> rfs[UNCURRY, set_registers_def] >>
            rw[lookup_insert, Once insert_insert] >| [
              map_every Cases_on [`th.sid`, `sid'`] >> fs[stack_id_dest_def],
              fs[mem_region_set_value_def, UNCURRY] >> rw[] >> rfs[] >>
              rw[lookup_insert]
            ]
        in [eq_tac, eq_tac, eq_tac, upd_tac, upd_tac] end)
    ],

    (* Fail *)
    DISJ1_TAC >> Cases_on `op` >> fs[exec_non_memory_action_def, exec_load_def,
      exec_store_def, exec_commit_def, exec_reconcile_def, exec_command_def,
      run_reg_reader_stack_def, bind_left_case, UNCURRY] >| [

      multi_case_tac >> rfs[run_reg_reader_def] >> rw[] >>
      qpat_x_assum `set_registers _ _ = Fail _` (fn fail_case =>
        qpat_assum `set_registers _ _ = _` (fn ok_case =>
          assume_tac fail_case >>
          `^(lhs (concl ok_case)) = ^(rhs (concl fail_case))`
            suffices_by simp[] >>
          match_mp_tac set_registers_fail >> 
          qexists_tac `^(#2 (dest_comb (lhs (concl fail_case))))` >>
          rw[] >> fs[])),
      
      BasicProvers.every_case_tac >>
      rfs[run_reg_reader_def, set_registers_def, mem_region_set_value_def,
          bind_left_case, UNCURRY] >>
      ntac 3 (rw[] >> fs[]) >> rfs[UNCURRY] >> multi_case_tac >> fs[],

      BasicProvers.every_case_tac >>
      rfs[run_reg_reader_def, set_registers_def, mem_region_set_value_def,
          bind_left_case, UNCURRY] >>
      ntac 3 (rw[] >> fs[]) >> rfs[UNCURRY] >> multi_case_tac >> fs[],

      multi_case_tac,

      multi_case_tac,

      Cases_on `run_reg_reader f regs1` >> rfs[run_reg_reader_def] >>
      ntac 3 (rw[] >> fs[]) >> Cases_on `x''` >> fs[] >| [
        fs[push_stack_def, resume_stack_def, jump_def, enter_block_def,
           bind_left_case] >> multi_case_tac,

        fs[pop_stack_def, bind_left_case] >> multi_case_tac >>
        fs[resume_stack_def, jump_def, enter_block_def, bind_left_case, UNCURRY] >>
        BasicProvers.every_case_tac >> ntac 3 (rw[] >> fs[] >> rfs[]),

        fs[jump_def, enter_block_def, bind_left_case] >> multi_case_tac >>
        ntac 3 (rw[] >> fs[] >> rfs[]),

        multi_case_tac >>
        fs[set_registers_def, mem_region_set_value_def, bind_left_case, UNCURRY] >>
        multi_case_tac >> rfs[] >> fs[] >> rw[],

        multi_case_tac >>
        fs[set_registers_def, mem_region_set_value_def, bind_left_case, UNCURRY] >>
        multi_case_tac >> rfs[] >> fs[] >> rw[]
      ]
    ]
  ])
*)

val spt_submap_def = Define`
  spt_submap sup sub ⇔ ∀n x. lookup n sub = SOME x ⇒ lookup n sup = SOME x
`

(*
val reads_reg_union = store_thm("reads_reg_union",
  ``∀r1 r2 f. wf r1 ∧ wf r2 ∧ (∀n. n ∈ domain r1 ⇒ ¬reads_reg n f)
            ⇒ run_reg_reader f (union r1 r2) = run_reg_reader f r2``,
  rw[] >>
  `∃al. al = toAList r1` by simp[] >>
  `r1 = fromAList al` by simp[fromAList_toAList] >>
  rw[] >> pop_assum (K all_tac) >> Induct_on `al` >| [
    rw[fromAList_def],

    Cases >> fs[fromAList_def, reads_reg_def] >> rw[] >>
      `union (insert q r (fromAList al)) r2 =
       insert q r (union (fromAList al) r2)`
        suffices_by (fs[DISJ_IMP_THM, FORALL_AND_THM, wf_fromAList] >> prove_tac[]) >>
      rw[spt_eq_thm, wf_fromAList, wf_insert, wf_union, lookup_union,
         lookup_insert] >> multi_case_tac
  ])

val reads_reg_difference = store_thm("reads_reg_differece",
  ``∀r1 r2 f. wf r1 ∧ wf r2 ∧ (∀n. n ∈ domain r1 ⇒ ¬reads_reg n f)
            ⇒ run_reg_reader f (mk_wf (difference r2 r1)) = run_reg_reader f r2``,
  rw[] >>
  `r2 = union (inter r2 r1) (mk_wf (difference r2 r1))` suffices_by
    metis_tac[reads_reg_union, domain_inter, IN_INTER, wf_union, wf_inter,
              wf_mk_wf] >>
  rw[lookup_union, lookup_inter, lookup_difference, lookup_mk_wf,
     wf_union, wf_inter, wf_mk_wf, spt_eq_thm] >> multi_case_tac)
*)

val foldl_OK = store_thm("foldl_ok",
  ``∀ys x x' f. FOLDL (λs r. s >>= f r) x ys = OK x'
              ⇒ ∃xv. x = OK xv ∧ FOLDL (λs r. OUTL (f r s)) xv ys = x'``,
  Induct >> rw[] >> Cases_on `x` >> fs[foldl_fail] >> res_tac >> rw[])

(*
val advances_pc_of_same_opn_preserves_upcoming_opns = store_thm(
  "advances_pc_of_same_opn_preserves_upcoming_opns",
  ``∀step sid Δ st1 st2 sk1 f1 fs1 opn opns1 opns2.
      wf st1.stacks
    ∧ advances_pc_of sid step st1
    ∧ st2 = state_upd (active_frame_delta sid (opn::opns2) LN LN) st1
    ∧ get_stack sid st1 = SOME sk1
    ∧ sk1.frames = f1::fs1
    ∧ f1.state = ACTIVE (opn::opns1)
    ⇒ (try_step step st2 = state_upd (active_frame_delta sid opns2 LN LN)
                           <$> try_step step st1
    ∨ (try_step step st1 = OK st1 ∧ try_step step st2 = OK st2))``,
  Cases >> ntac 10 strip_tac >>
  simp[advances_pc_of_def, active_frame_delta_def, lift_left_def,
       state_upd_def, id_state_delta_def, o_DEF, address_space_union_empty] >>
  DISCH_TAC >> fs[] >>
  `¬has_terminated st1 ∧ ¬has_terminated st2` by (
    rw[has_terminated_def, EVERY_MEM, thread_current_frame_state_def,
       pairTheory.UNCURRY] >>
    qexists_tac `(thread_id_dest st1.sched_tid, th)` >>
    fs[get_thread_def, get_stack_def, MEM_toAList, thread_id_dest_def,
       lookup_spt_upd, lookup_insert]) >>
  rw[try_step_def] >> multi_case_tac >>
  fs[take_step_def, get_thread_def, get_stack_def,
     sched_state_component_equality, lookup_spt_upd, lookup_insert] >>
  rfs[] >> rw[] >> rfs[] >> Cases_on `opn` >>
  fs[exec_non_memory_action_def, exec_load_def, exec_store_def,
     exec_commit_def, exec_reconcile_def, exec_command_def] >>
  ntac 2 (pop_assum (mp_tac o converse)) >> rw[] >> DISJ1_TAC >>
*)
(*
val advances_pc_of_same_opn_preserves_frame_delta = store_thm(
  "advances_pc_of_same_opn_preserves_frame_delta",
  ``∀step sid Δ st1 st2 sk1 f1 fs1 opn opns1 opns2 regs_add regs_del.
      wf st1.stacks
    ∧ wf f1.regs
    ∧ wf regs_add
    ∧ wf regs_del
    ∧ advances_pc_of sid step st1
    ∧ st2 = state_upd (active_frame_delta sid (opn::opns2) regs_add regs_del) st1
    ∧ get_stack sid st1 = SOME sk1
    ∧ sk1.frames = f1::fs1
    ∧ f1.state = ACTIVE (opn::opns1)
    ∧ (∀r. opn_reads_reg r opn ⇒ r ∉ (domain regs_add ∪ domain regs_del))
    ⇒ ∃regs_add' regs_del'.
        spt_submap regs_add regs_add'
      ∧ spt_submap regs_del regs_del'
      ∧ (try_step step st2 =
           state_upd (active_frame_delta sid opns2 regs_add' regs_del')
           <$> try_step step st1
       ∨ (try_step step st1 = OK st1 ∧ try_step step st2 = OK st2))``,
  Cases >> ntac 12 strip_tac >>
  simp[advances_pc_of_def, active_frame_delta_def, lift_left_def,
       state_upd_def, id_state_delta_def, o_DEF, address_space_union_empty] >>
  DISCH_TAC >> fs[] >>
  `¬has_terminated st1 ∧ ¬has_terminated st2` by (
    rw[has_terminated_def, EVERY_MEM, thread_current_frame_state_def,
       pairTheory.UNCURRY] >>
    qexists_tac `(thread_id_dest st1.sched_tid, th)` >>
    fs[get_thread_def, get_stack_def, MEM_toAList, thread_id_dest_def,
       lookup_spt_upd, lookup_insert]) >>
  rw[try_step_def, take_step_def] >> REPEAT CASE_TAC >> rw[] >>
  rfs[sched_state_component_equality, get_thread_def, get_stack_def,
      lookup_spt_upd, lookup_insert] >> fs[] >> rfs[] >>
  fs[exec_non_memory_action_def, exec_load_def, exec_store_def,
     exec_commit_def, exec_reconcile_def, exec_command_def] >>
  TRY (qpat_assum `_ = x` (fn a => rw[converse a])) >>
  qpat_x_assum `¬has_terminated _` (K all_tac) >>
  fs[sched_state_component_equality] >| [
    (* Com *)
    map_every qexists_tac [`regs_add`, `regs_del`] >> simp[spt_submap_def],

    (* Nm *)
    fs[opn_reads_reg_def] >> rw_assums_once[MONO_NOT_EQ] >>
    fs[DE_MORGAN_THM, DISJ_IMP_THM, FORALL_AND_THM] >>
    simp[reads_reg_union, reads_reg_difference, wf_mk_wf] >>
    Cases_on `run_reg_reader f f1.regs` >> rw[] >| [
      map_every qexists_tac [
        `mk_wf (difference regs_add (fromAList x))`,
        `mk_wf (difference regs_del (fromAList x))`] >> rw[spt_submap_def]
      >- fs[lookup_mk_wf, lookup_difference]
      >- fs[lookup_mk_wf, lookup_difference] >>
      DISJ1_TAC >> rw[bind_left_case] >> multi_case_tac >| [
        rw[sched_state_component_equality] >> ntac 2 (rw[Once insert_insert]) >>
        rw[spt_eq_thm, wf_insert, wf_spt_upd, lookup_spt_upd, lookup_insert] >>
        CASE_TAC >> rw[stack_component_equality] >| [
          Cases_on `NULL x'.frames` >> Cases_on `x'.frames` >> 
          FIRST [fs[NULL_DEF] >> NO_TAC, rw[]] >| [
            rw_assums[converse bind_left_case] >>
            `∀r. propagates (λsk. ¬NULL sk.frames) (set_register r)` suffices_by
              metis_tac[NULL_DEF, foldl_propagates, propagates_def]
          ]
        ]
        frame_component_equality] >| [
          rw[spt_eq_thm, wf_union, wf_mk_wf, lookup_union, lookup_mk_wf,
             lookup_difference] >> multi_case_tac >> fs[fromAList_def, lookup_def],
 
          Cases_on_match `a` `a = _` >> rw[] >| [
            qpat_x_assum `run_reg_reader _ _ = _` (K all_tac) >>
            qpat_x_assum `run_reg_reader _ _ = _ ⇒ _` (K all_tac) >>
            Induct_on `x` >> rw[] >> Cases_on `h` >> rw[set_register_def]
          ]
        ]
        >> Cases_on `h` >> rw[set_register_def]

      ]
      
      
      Induct_on `x` >> rw[] >| [
        rw[sched_state_component_equality] >> ntac 4 (rw[Once insert_insert]) >>
        rw[spt_eq_thm, wf_insert, wf_spt_upd, lookup_spt_upd, lookup_insert] >>
        CASE_TAC >> rw[stack_component_equality, frame_component_equality,
          spt_eq_thm, wf_union, wf_mk_wf, lookup_union, lookup_mk_wf,
          lookup_difference, fromAList_def] >> multi_case_tac >> fs[lookup_def],
        
        rw[bind_left_case] >> multi_case_tac >| [
          ntac 2 (pop_assum (K all_tac)) >> rw[]
          rw[sched_state_component_equality] >> ntac 2 (rw[Once insert_insert]) >>
          rw[spt_eq_thm, wf_insert, wf_spt_upd, lookup_spt_upd, lookup_insert] >>
          CASE_TAC >> Cases_on `h` >> rw[set_register_def]
          
          rw[stack_component_equality] >| [
            Cases_on `x'.frames` >> rw[] >| [
              Cases_on `h` >> Induct_on `x` >> rw[set_register_def] >> fs[] >>
              Cases_on `mem_region_set_value q r (union regs_add
                (mk_wf (difference f1.regs regs_del)))` >> fs[FOLDL]

              
              fs[FOLDL] >>
              multi_case_tac
            ]

      ]
    ]
  ])
*)

(*
val ExecInst_same_inst_preserves_stack_delta = store_thm(
  "ExecInst_same_inst_preserves_stack_delta",
  ``∀skΔ st1 st2 th f1 f2.
      wf st1.stacks
    ∧ st2 = state_upd (idΔ with stacks := skΔ) st1
    ∧ th = 
    ⇒ take_step ExecInst st2 =
      liftM (liftM (state_upd (idΔ with stacks := skΔ))) (take_step ExecInst st1)``,
*)


(*
val advances_pc_by_0_stack_delta = store_thm("advances_pc_by_0_stack_delta",
  ``∀sc st tid sid th f.
      advances_pc_by 0 sc st tid
    ∧ get_thread tid st = SOME th
    ∧ th.sid = StackID sid
    ⇒ run_finite_schedule sc (state_upd (idΔ with stacks := insert sid f LN) st) =
      state_upd (idΔ with stacks := insert sid f LN)
      <$> run_finite_schedule sc st``,
  Induct >> rw[state_upd_def, lift_left_def, o_DEF, run_finite_schedule_def] >>
  fs[run_finite_schedule_def] >> res_tac >>
    
  fs[Once advances_pc_by_cases] >| [
    rw[run_finite_schedule_def],
    multi_case_tac
  ]
  *)

(* If two states differ only in the active frame's registers, and the next
   operation in the active frame does not reference any of the changed
   registers, then the two states remain different in only those registers.
*)
(*
val advance_pc_ignores_unused_regs = store_thm("advance_pc_ignores_unused_regs",
  ``∀st st' tid sid th sk regs next_op sc.
      get_thread tid st = SOME th
    ∧ get_stack sid st = SOME sk
    ∧ th.sid = sid
    ∧ advances_pc_by 1 sc st tid
    ∧ (∃rest. (HD sk.frames).state = ACTIVE (next_op::rest))
    ∧ (∀r. r ∈ domain regs ⇒ ¬opn_reads_reg r next_op)
    ∧ st' = state_upd
        (idΔ with stacks updated_by insert (stack_id_dest sid) (λsk.
           THE sk with frames updated_by (λf.
             (HD f with regs updated_by union regs)::TL f))) st
    ⇒ ∃regs'. domain regs' ⊆ domain regs
      ∧ run_finite_schedule sc st' = state_upd
          (idΔ with stacks updated_by insert (stack_id_dest sid) (λsk.
             THE sk with frames updated_by (λf.
               (HD f with regs updated_by union regs')::TL f)))
          <$> run_finite_schedule sc st``,
  rw[state_upd_def, lift_left_def, o_DEF]
  *)

(*
val compile_block_same_regs = store_thm("compile_block_same_regs",
  ``∀b body' lbl.
      (compile_block lbl b).regs =
      (compile_block lbl (b with body := body')).regs
    ∧ (compile_block lbl b).exc_param =
      (compile_block lbl (b with body := body')).exc_param
*)

(*
val remove_inst_block_types = store_thm("remove_inst_block_types",
  ``∀b n lbl.
      n < (LENGTH b.body)
    ∧ EVERY (λs. ∃m. m < n ∧ MEM s (MAP FST (inst_types (EL m b.body))))
            (inst_all_vars (EL n b.body))
    ⇒ nub (block_types b) =
      nub (block_types (b with body updated_by λis. TAKE n is ++ DROP (SUC n) is))``
  rw[inst_all_vars_def, EVERY_MEM, UNCURRY] >>
  qmatch_assum_rename_tac `n < LENGTH is` >> POP_ASSUM_LIST (map_every mp_tac) >>
  qid_spec_tac `is` >> listLib.SNOC_INDUCT_TAC >- rw[] >> REPEAT strip_tac >>
  fs[UNCURRY] >> Cases_on `n < LENGTH is` >| [
    fs[block_types_def, LIST_BIND_APPEND, TAKE_APPEND1, DROP_APPEND1, EL_SNOC] >> rw[] >>
  ]
  >>

  rw[block_types_def, inst_all_vars_def, UNCURRY, EVERY_MEM, nub_append,
     FILTER_APPEND_DISTRIB] >>
  qmatch_assum_rename_tac `n < LENGTH is` >> POP_ASSUM_LIST (map_every mp_tac) >>
  qid_spec_tac `is` >> listLib.SNOC_INDUCT_TAC >- rw[] >> REPEAT strip_tac >>
  Cases_on `n < LENGTH is` >| [
    fs[LIST_BIND_APPEND, TAKE_APPEND1, DROP_APPEND1, EL_SNOC] >> rw[] >>
    metis_tac[EL_SNOC, LESS_TRANS],

    fs[converse LESS_EQ_IFF_LESS_SUC, LESS_OR_EQ] >> rw[] >>
    fs[LIST_BIND_APPEND, TAKE_APPEND2, DROP_APPEND2, EL_LENGTH_SNOC]
  ]
  Induct_on `is` >> rw[]

val remove_inst_same_regs = store_thm("remove_inst_same_regs",
  ``∀b n lbl.
      n < (LENGTH b.body)
    ∧ EVERY (λs. ∃m. m < n ∧ MEM s (MAP FST (inst_types (EL m b.body))))
            (inst_all_vars (EL n b.body))
    ⇒ (compile_block lbl b).regs =
      (compile_block lbl (b with body updated_by λis. TAKE n is ++ DROP (SUC n) is)).regs
    ∧ (compile_block lbl b).exc_param =
      (compile_block lbl (b with body updated_by λis. TAKE n is ++ DROP (SUC n) is)).exc_param``,
  rw[compile_block_def, block_types_def, UNCURRY]
*)

(* Simplest possible optimization: remove an ID that writes an SSA variable to
   itself (a no-op).

   The SSA variable must occur in some other instruction before the no-op in the
   block; this ensures that the order of register numbers is not changed.
*)
val opt_remove_noop = store_thm("opt_remove_noop",
  ``∀n ssa. equivalent_code (K (K T))
      (λb. ∃ty m. m < n
                ∧ ALOOKUP (inst_types (EL m b.body)) ssa = SOME ty
                ∧ EL n b.body = Assign [ssa] (Id ty (Var ssa)))
      (λb. b with body updated_by λis. TAKE n is ++ DROP (SUC n) is)``,
  rw[equivalent_code_def, state_subset_def, IN_APP, final_states_def] >>
  qmatch_assum_abbrev_tac `OK sk = upd_top_frame (_ b1 _ _) _` >>
  qmatch_assum_abbrev_tac `OK sk' = upd_top_frame (_ b2 _ _) _` (*>>
  `b1.regs = b2.regs ∧ b1.exc_param = b2.exc_param`
    by fs[Abbr `b1`, Abbr `b2`, compile_block_def, UNCURRY] >>
  qmatch_abbrev_tac `
    ∃s2'. (∃sc. finite_schedule_terminates st1 sc
              ∧ run_finite_schedule sc st1 = s2')
        ∧ isomorphic_states (run_finite_schedule sc st2) s2'` >>
  Cases_on `∃sc1 sc2 mid_st.
      sc = sc1 ++ sc2
    ∧ advances_pc_by n sc1 st1 orig_st.sched_tid
    ∧ run_finite_schedule sc1 st2 = OK mid_st
    ∧ mid_st.sched_tid = orig_st.sched_tid
  ` >> rw[] (*>| [
    `run_finite_schedule (sc1 ++ [ExecInst]) st1 = OK mid_st` by cheat >>
    qexists_tac `run_finite_schedule (sc1 ++ [ExecInst] ++ sc2) st1` >> rw[] >| [
      qexists_tac `sc1 ++ [ExecInst] ++ sc2` >>
      fs[converse run_finite_schedule_append, finite_schedule_terminates_def,
         merge_left_def] >> multi_case_tac,

      fs[converse run_finite_schedule_append] >> rw[isomorphic_states_def]
    ]

  (* Proof process:

     - All steps until the removed ID is reached are identical
     - Once the ID is reached, all schedules with an extra ExecInst for the
       code with the ID are identical to schedules without the ExecInst for the
       code without the ID
  *)
  ]*)*) >> cheat
)

val _ = export_theory()

