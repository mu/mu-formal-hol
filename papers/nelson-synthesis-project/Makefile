thy_dir = ../..
thys = sumMonad uvmTypes uvmErrors uvmValues uvmMemory uvmIR uvmThreadsStacks uvmInstructionSemantics uvmScheduler
thy_names = $(patsubst %,$(thy_dir)/%Theory,$(thys))
thy_srcs = $(patsubst %,$(thy_dir)/%Script.sml,$(thys))
thy_files = $(patsubst %Theory,%Theory.sml,$(thy_names))

holdirfile = .holdir

pandoc_opts = +RTS -V0 -RTS

all: synthesis.pdf

$(holdirfile):
	if test -z "$$holdir"; then \
	  clear; \
	  echo "holdir is not defined. Where is HOL4 installed? [Type path, press ENTER]"; \
	  read holdir; \
	fi; \
	echo "" | $$holdir/bin/hol && \
	echo "$$holdir" > $(holdirfile)

$(thy_files): $(holdirfile) $(thy_srcs)
	cd $(thy_dir); Holmake

munge.exe: $(holdirfile) $(thy_files)
	$(shell cat $(holdirfile))/bin/mkmunge.exe $(thy_names)

synthesis.htex: synthesis.md
	pandoc $(pandoc_opts) --toc --to=latex -o synthesis.htex synthesis.md

# There's some regex voodoo below; it does two things:
# - The first `sed` replaces `%HOLIMPORT` in the header with an import.
# - The second and third `sed`s escape some underscores that HOL's munger doesn't.
synthesis.tex: munge.exe synthesis.htex header.tex
	sed s:%HOLIMPORT:\\\\usepackage{$(shell cat $(holdirfile))/src/TeX/holtexbasic}: \
	  < header.tex > synthesis.tex
	./munge.exe overrides.txt -w86 < synthesis.htex \
	  | sed 's/{\([a-z_]\+\)_/{\1\\_/g' \
	  | sed 's/{\([a-z_]\+\)_/{\1\\_/g' >> synthesis.tex
	echo '\end{document}' >> synthesis.tex

synthesis.pdf: synthesis.tex
	pdflatex synthesis.tex

clean:
	rm -f synthesis.htex synthesis.tex synthesis.aux synthesis.pdf munge.exe
	cd $(thy_dir); Holmake clean

