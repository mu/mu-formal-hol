open HolKernel Parse boolLib bossLib

open uvmSchedulerTheory
open BundleParser
open EvalUtils

val _ = new_theory "basicMemoryOpsTest"
val _ = add_extra_eval_thms()

val global_cell_test = load_uir_file "uir/global-cell-store-load.uir"
  |> to_env |> start_at "main" `[IntV 8 4]`
  |> run_schedule `naive_schedule`
  |> expect_result `OK (NOR [IntV 8 4])`

val heap_alloc_test = load_uir_file "uir/heap-store-load.uir"
  |> to_env |> start_at "main" `[IntV 8 4]`
  |> run_schedule `naive_schedule`
  |> expect_result `OK (NOR [IntV 8 4])`

val stack_alloc_test = load_uir_file "uir/stack-store-load.uir"
  |> to_env |> start_at "main" `[IntV 8 4]`
  |> run_schedule `naive_schedule`
  |> expect_result `OK (NOR [IntV 8 4])`

val throw_exc_test = load_uir_file "uir/throw.uir"
  |> to_env |> start_at "catch" `[]`
  |> run_schedule `naive_schedule`
  |> expect_result `OK (NOR [IntV 8 2])`

val _ = export_theory()

