open HolKernel Parse boolLib bossLib

open uvmSchedulerTheory
open BundleParser
open EvalUtils

val _ = new_theory "basicOpsTest"
val _ = add_extra_eval_thms()

val bundle1 = load_uir_file "uir/add-two.uir"

val state1 = bundle1 |> to_env |> start_at "add_two" `[IntV 8 3]`

val _ = state1 |> run_finite_schedule `[ExecInst; ExecInst]`
               |> expect_result `OK (NOR [IntV 8 5])`

val _ = state1 |> run_schedule `cycle [ExecInst]`
               |> expect_result `OK (NOR [IntV 8 5])`

(* Test with undefined input *)
val state_undef = bundle1 |> to_env |> start_at "add_two" `[UndefV (Int 8)]`

val _ = state_undef |> run_finite_schedule `[ExecInst; ExecInst]`
                    |> expect_result `OK (NOR [UndefV (Int 8)])`

val _ = export_theory()

