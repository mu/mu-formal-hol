open HolKernel Parse boolLib bossLib

open uvmSchedulerTheory
open BundleParser
open EvalUtils

val _ = new_theory "factorialTest"
val _ = add_extra_eval_thms()

val bundle = load_uir_file "uir/factorial.uir" |> to_env

val fact2_rec_test =
  bundle |> start_at "fact_rec" `[IntV 16 2]`
         |> run_schedule `cycle [ExecInst]`
         |> expect_result `OK (NOR [IntV 16 2])`

val fact5_rec_test =
  bundle |> start_at "fact_rec" `[IntV 16 5]`
         |> run_schedule `cycle [ExecInst]`
         |> expect_result `OK (NOR [IntV 16 120])`

val fact2_iter_test =
  bundle |> start_at "fact_iter" `[IntV 16 2]`
         |> run_schedule `cycle [ExecInst]`
         |> expect_result `OK (NOR [IntV 16 2])`

val fact5_iter_test =
  bundle |> start_at "fact_iter" `[IntV 16 5]`
         |> run_schedule `cycle [ExecInst]`
         |> expect_result `OK (NOR [IntV 16 120])`

val _ = export_theory()

