open HolKernel Parse boolLib bossLib

open ParserHelpers
open TypeParser
open ValueParser
open wordsTheory
open monadsTheory
open uvmTypesTheory
open uvmValuesTheory
open uvmMemoryTheory

val _ = new_theory "valuesTest"

fun assert_parse(s, t, vq) =
  let val v = typedTerm vq (ty_list ty_val)
      val v' = uir_value [QUOTE s] handle HOL_ERR e => raise ParseError(
        "failed to parse value " ^ s ^ ": " ^ #message e)
  in
    prove(mk_eq(mk_comb(v', t), mk_return v),
      simp[lift_left_def, ap_left_def, mapM_left_def, sequence_left_def])
    handle HOL_ERR e => raise ParseError(
      "value " ^ s ^ " did not parse as " ^ term_to_string v ^ ": " ^ #message e)
  end

val _ =
  assert_parse("1", ``Int 8``, `[IntV 8 1]`);
  assert_parse("91", ``Int 32``, `[IntV 32 91]`);
  assert_parse("NULL", ``Ref (Type Void)``, `[RefV Void NONE]`);
  assert_parse("NULL", ``Ref (Type (Int 8))``, `[RefV (Int 8) NONE]`);
(* (* FIXME: Composite constants don't typecheck *)
  assert_parse("{5 6}", ``Array (Int 8) 2``, `[IntV 8 5; IntV 8 6]`);
*)
(*
  (* FIXME: This doesn't parse yet; something's wrong with take_one_value *)
  assert_parse("{{1 2} {3 4}}", ``Array (Vector (Int 16) 2) 2``,
    `ArrayV (Vector (Int 16) 2) [
      SOME (VectorV (Int 16) [IntV 16 1w; IntV 16 2w]);
      SOME (VectorV (Int 16) [IntV 16 3w; IntV 16 4w])
    ]`);
*)

val _ = export_theory()

