open HolKernel Parse boolLib bossLib

open uvmSchedulerTheory
open BundleParser
open EvalUtils

val _ = new_theory "basicThreadOpsTest"
val _ = add_extra_eval_thms()

(* TODO: Uncomment these when NEWTHREAD and SWAPSTACK are reimplemented *)

(*
val spawn_thread_test = load_uir_file "uir/spawn-thread.uir"
  |> to_env |> start_at "spawn" `[]`
  |> run_schedule `seq_cst_schedule 2`
  |> expect_global_value "out" `OK (IntV 8 2w)`

val swapstack_test = load_uir_file "uir/swapstack.uir"
  |> to_env |> start_at "sender" `[]`
  |> run_schedule `single_thread_seq_cst_schedule`
  |> expect_global_value "out" `OK (IntV 8 2w)`
*)

val _ = export_theory()

