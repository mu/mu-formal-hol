open HolKernel Parse boolLib bossLib

open ParserHelpers
open TypeParser
open monadsTheory
open uvmTypesTheory

val _ = new_theory "typesTest"

fun ty q = uir_type mock_context q
fun assert_parse(s, t) =
  let val t' = ty [QUOTE s] in
    prove(mk_eq(t', mk_return t), simp[lift_left_def, ap_left_def])
    handle HOL_ERR e => raise ParseError(
      "type " ^ s ^ " did not parse as " ^ term_to_string t ^ ": " ^ #message e)
  end

(* Test the type parser on all kinds of valid inputs *)

val _ =
  assert_parse("void", ``Void``);
  assert_parse("float", ``Float``);
  assert_parse("double", ``Double``);
  assert_parse("stackref", ``StackRef``);
  assert_parse("threadref", ``ThreadRef``);
  assert_parse("tagref64", ``TagRef64``);
  assert_parse("int<1>", ``Int 1``);
  assert_parse("int<32>", ``Int 32``);
  assert_parse("ref<void>", ``Ref (Type Void)``);
  assert_parse("ref<int<32>>", ``Ref (Type (Int 32))``);
  assert_parse("iref<void>", ``IRef REF (Type Void)``);
  assert_parse("uptr<void>", ``IRef PTR (Type Void)``);
  assert_parse("ref<iref<int<32>>>", ``Ref (Type (IRef REF (Type (Int 32))))``);
  assert_parse("weakref<int<32>>", ``WeakRef (Type (Int 32))``);
  assert_parse("array<float 4>", ``Array Float 4``);
  assert_parse("array<int<8> 2>", ``Array (Int 8) 2``);
  assert_parse("vector<uptr<float> 3>", ``Vector (IRef PTR (Type Float)) 3``);
  (* TODO: struct, hybrid, funcref, ufuncptr *)

val _ = export_theory()

