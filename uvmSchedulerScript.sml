open HolKernel Parse boolLib bossLib

open uvmMemoryTheory
open uvmThreadsStacksTheory
open uvmInstructionSemanticsTheory

open arithmeticTheory
open combinTheory
open numpairTheory
open pred_setTheory
open listTheory
open llistTheory
open sortingTheory
open sptreeTheory
open sumTheory

open syntaxSugarLib
open syntaxSugarTheory
open BasicProvers
open extraTactics

val _ = new_theory "uvmScheduler"
val _ = monadsyntax.add_monadsyntax()
val _ = reveal "C"

val _ = Datatype`
  sched_step =
  | ExecInst
  | NextThread
  | NextAddr
  | DequeueStore
  | DequeueInvalidation
`

val _ = Datatype`
  sched_state = <|
    program: program;
    threads: thread spt;
    stacks: stack spt;
    memory: memory;
    sched_tid: thread_id;
    sched_addrs: addr spt;
  |>
`

val state_ok_def = Define`
  state_ok (s : sched_state) : bool ⇔
    let thread_ok' th = (
        thread_ok th
      ∧ stack_id_dest th.sid ∈ domain s.stacks
      ∧ ∀n th'. lookup n s.threads = SOME th' ∧ th ≠ th'
              ⇒ th.sid ≠ th'.sid)
    in wf s.threads
     ∧ wf s.stacks
     ∧ wf s.sched_addrs
     ∧ s.threads ≠ LN
     ∧ s.stacks ≠ LN
     ∧ EVERY (PROD_ALL (domain s.sched_addrs) thread_ok') (toAList s.threads)
     ∧ EVERY (stack_ok o SND) (toAList s.stacks)
     ∧ memory_ok s.memory
`

val get_thread_def = Define`
  get_thread tid state : thread option =
    lookup (thread_id_dest tid) state.threads
`

val get_stack_def = Define`
  get_stack sid state : stack option =
    lookup (stack_id_dest sid) state.stacks
`

val sched_addr_def = Define`
  sched_addr (st : sched_state) : addr =
    option_CASE (lookup (thread_id_dest st.sched_tid) st.sched_addrs)
                (Global, 0) I
`

val next_thread_def = Define`
  next_thread state : thread_id =
    let tids = QSORT $< (MAP FST (toAList state.threads))
    in ThreadID (
         option_CASE (FIND ($> (thread_id_dest state.sched_tid)) tids)
                     (HD tids) I)
`

val next_addr_def = Define`
  next_addr st : addr =
    let addrs =
      option_CASE (get_thread st.sched_tid st)
                  [] (λth. nub (QSORT addr_lt (addrs_of th.sb ++ addrs_of th.ib)))
    in option_CASE (FIND (C addr_lt (sched_addr st)) addrs)
                   (if NULL addrs then (Global, 0) else HD addrs) I
`

val new_sched_state_def = Define`
  new_sched_state (env : environment) (main : func_name)
                  : sched_state or_error = do
    mem <- new_memory env.globals;
    vopt <- ALOOKUP env.func_versions main
      or_fail Impossible ("no function named " ++ (case main of Func s => s));
    version <- vopt or_fail UndefBehavior (NoSuchFunction main);
    func <- ALOOKUP env.functions version
      or_fail Impossible "no such main function version";
    return <|
      program := compile_program env;
      threads := insert 0 (new_thread (StackID 0)) LN;
      stacks := insert 0 <|
          frames := [<|
              id := FrameID 0;
              fn := main;
              state := READY func.signature.arg_types <|
                  nor_jump := (0, GENLIST INL (LENGTH func.signature.arg_types));
                  exc_jump := NONE
                |>
            |>];
          next_frame := FrameID 1
        |> LN;
      memory := mem;
      sched_tid := ThreadID 0;
      sched_addrs := insert 0 (Global, 0) LN
    |>
  od
`

(*
val new_sched_state_ok = store_thm("new_sched_state_ok",
  ``∀env main s. new_sched_state env main = OK s ⇒ state_ok s``,

  rw[new_sched_state_def, state_ok_def] >>
  fs[wf_insert, wf_def, new_memory_def, new_address_space_def] >>
  ONCE_REWRITE_TAC[insert_def] >>
  simp[toList_def, toListA_def, toAList_def, foldi_def, new_thread_ok,
       thread_id_dest_def, stack_id_dest_def, frame_id_lt_def,
       stack_ok_def, memory_ok_def, mem_region_ok_def, frame_ok_def, wf_def] >>
  multi_case_tac >> simp[wf_def, toListA_def] >| [
    ntac 2 (pop_assum (K all_tac)) >> pop_assum mp_tac >>
    qmatch_rename_tac `FOLDL _ (OK (ll, _)) (ZIP (_, MAP SND globals)) = _ ⇒ _` >>
    map_every qid_spec_tac [`q`, `r`, `globals`] >>
    listLib.SNOC_INDUCT_TAC >> fs[wf_def] >> rw[] >>
    fs[MAP_SNOC, GENLIST, rich_listTheory.ZIP_SNOC, FOLDL_SNOC, region_alloc_def] >>
    multi_case_tac >> fs[] >> POP_ASSUM_LIST (K all_tac) >>
    Induct_on `q''`
    Induct_on `globals` >> 
    Induct_on `env.globals` >> rw[] >| [
      `env.globals = []` by simp[] >> fs[] >> prove_tac[wf_def],
      `env.globals = h::v`
    ]
  ]
  Induct_on `env.globals` >> rw[]

  fs[MEM_EL, EL_GENLIST, optionTheory.IS_SOME_DEF, PEEK_ADD] >| [

    `stack_ok <|frames := []; next_frame := Frame 0|>`
      suffices_by metis_tac[push_frame_ok, propagates_def] >>
    simp[stack_ok_def],

    match_mp_tac EVERY_LEAF_ADD >> simp[EVERY_LEAF_def, new_thread_ok]
  ])
*)

val thread_current_frame_state_def = Define`
  thread_current_frame_state tid state = do
    th <- get_thread tid state;
    sk <- get_stack th.sid state;
    return (case sk.frames of
            | fr::_ => fr.state
            | _ => DEAD NONE)
  od
`

val start_with_args_def = Define`
  start_with_args (args : value list) (st : sched_state)
                  : sched_state or_error = do
    sk <- get_stack (StackID 0) st or_fail Impossible "no stack 0";
    sk' <- resume_stack st.program (NOR args) sk;
    return (st with stacks updated_by insert 0 sk')
  od
`

val run_reg_reader_stack_def = Define`
  run_reg_reader_stack (r : α or_error reg_reader) (sk : stack) : α or_error =
    (assert (¬NULL sk.frames) or_fail Impossible "stack underflow") *>
    case (HD sk.frames).state of
    | ACTIVE _ regs => join (run_reg_reader r regs)
    | _ => Fail (Impossible "exec on inactive stack")
`

val exec_non_memory_action_def = Define`
  exec_non_memory_action (rdr : (num, value) alist or_error reg_reader)
                         (th : thread)
                         (sk : stack)
                         (st : sched_state)
                         : sched_state or_error option = SOME do
    rs <- run_reg_reader_stack rdr sk;
    sk' <- set_registers rs sk;
    return (st with stacks updated_by insert (stack_id_dest th.sid) sk')
  od
`

val exec_load_def = Define`
  exec_load (rdr : iaddr or_error reg_reader)
            (dst : num)
            (th : thread)
            (sk : stack)
            (st : sched_state)
            : sched_state or_error option = SOME do
    ia <- run_reg_reader_stack rdr sk;
    ad <- HD <$> lift_mem_error (get_iaddr_cell_addrs st.memory ia);
    ld <- lift_mem_error (memory_load st.memory th.sb th.ib ad);
    sk' <- set_registers [(dst, ld)] sk;
    return (st with stacks updated_by insert (stack_id_dest th.sid) sk')
  od
`

val exec_store_def = Define`
  exec_store (rdr : (iaddr # value) or_error reg_reader)
             (th : thread)
             (sk : stack)
             (st : sched_state)
             : sched_state or_error option = SOME do
    (ia, v) <- run_reg_reader_stack rdr sk;
    ad <- HD <$> lift_mem_error (get_iaddr_cell_addrs st.memory ia);
    return (st with threads updated_by
      insert (thread_id_dest st.sched_tid) (enqueue_store (ad, v) th) o
      map (enqueue_invalidation (ad, v)))
  od
`

val exec_commit_def = Define`
  exec_commit (th : thread) (sk : stack) (st : sched_state)
              : sched_state or_error option = do
    assert (NULL (addrs_of th.sb));
    return (return st)
  od
`

val exec_reconcile_def = Define`
  exec_reconcile (th : thread) (sk : stack) (st : sched_state)
                 : sched_state or_error option =
    SOME (OK (st with threads updated_by insert (thread_id_dest st.sched_tid)
      (th with ib := new_address_space)))
`

val exec_command_def = Define`
  exec_command (rdr : command or_error reg_reader)
               (th : thread)
               (sk : stack)
               (st : sched_state)
               : sched_state or_error option = SOME do
    cmd <- run_reg_reader_stack rdr sk;
    case cmd of
    | DoPushStack fn sig args next => do
        sk' <- push_stack st.program fn sig args next sk;
        return (st with stacks updated_by insert (stack_id_dest th.sid) sk')
      od
    | DoPopStack rvs => do
        (sk', fr) <- pop_stack st.program rvs sk;
        return (st with <|
          stacks updated_by insert (stack_id_dest th.sid) sk';
          memory updated_by free_stack th.sid fr.id
        |>)
      od
    | DoJump label args => do
        sk' <- jump st.program label args NONE sk;
        return (st with stacks updated_by insert (stack_id_dest th.sid) sk')
      od
    | DoHeapAlloc ty sz dst => do
        (mem, ad) <- memory_heap_alloc st.memory sz ty;
        sk' <- set_registers [(dst, RefV ty (SOME ad))] sk;
        return (st with <|
          stacks updated_by insert (stack_id_dest th.sid) sk';
          memory := mem
        |>)
      od
    | DoStackAlloc ty sz dst => do
        (mem, ad) <- memory_stack_alloc st.memory th.sid (HD sk.frames).id sz ty;
        sk' <- set_registers [(dst, RefV ty (SOME ad))] sk;
        return (st with <|
          stacks updated_by insert (stack_id_dest th.sid) sk';
          memory := mem
        |>)
      od
    | DoGetStack dst => Fail (NotImplemented "DoGetStack")
    | DoNewThread sid rvs dst => Fail (NotImplemented "DoNewThread")
    | DoThreadExit => Fail (NotImplemented "DoThreadExit")
  od
`

val take_step_def = Define`
    take_step ExecInst st : sched_state or_error option = do
      th <- get_thread st.sched_tid st;
      case do
        sk <- get_stack th.sid st or_fail Impossible "missing stack";
        case (HD sk.frames).state of
        | ACTIVE (opn::rest) regs =>
            let
              sk = sk with frames updated_by λf.
                     (HD f with state := ACTIVE rest regs)::TL f;
              st = st with stacks updated_by insert (stack_id_dest th.sid) sk;
              step = case opn of
                     | Nm rdr => exec_non_memory_action rdr
                     | Ld rdr dst => exec_load rdr dst
                     | St rdr => exec_store rdr
                     | Com => exec_commit
                     | Rec => exec_reconcile
                     | Cmd rdr => exec_command rdr
            in return (step th sk st)
        | _ => Fail (Impossible "live thread's top stack frame is not ACTIVE")
      od of
      | OK x => x
      | Fail x => SOME (Fail x)
    od
  ∧ take_step NextThread st =
      SOME (OK (st with sched_tid := next_thread st))
  ∧ take_step NextAddr st =
      SOME (OK (st with sched_addrs updated_by
                  insert (thread_id_dest st.sched_tid) (next_addr st)))
  ∧ take_step DequeueStore st = do
      th <- get_thread st.sched_tid st;
      (sb, v) <- buffer_dequeue th.sb (sched_addr st);
      return do
        mem <- lift_mem_error (memory_put st.memory (sched_addr st) v);
        return (st with <|
          threads updated_by
            insert (thread_id_dest st.sched_tid) (th with sb := sb);
          memory := mem
        |>)
      od
    od
  ∧ take_step DequeueInvalidation st = do
      th <- get_thread st.sched_tid st;
      (ib, _) <- buffer_dequeue th.ib (sched_addr st);
      return (OK (st with threads updated_by
        insert (thread_id_dest st.sched_tid) (th with ib := ib)))
    od
`

val has_terminated_def = Define`
  has_terminated (state : sched_state) : bool ⇔
    EVERY (λ(tid, _).
      case thread_current_frame_state (ThreadID tid) state of
      | SOME (DEAD _) => T
      | _ => F
    ) (toAList state.threads)
`

val try_step_def = Define`
  try_step (step : sched_step) (state : sched_state) : sched_state or_error =
    if has_terminated state
    then return state
    else case take_step step state of
         | NONE => return state
         | SOME next => next
`

(*
val try_step_ok = store_thm("try_step_ok",
  ``∀step. propagates state_ok (try_step step)``,
  assume_tac take_step_ok >> assume_tac resolve_all_ok >>
  fs[propagates_def] >> rw[try_step_def] >> every_case_tac >> fs[] >>
  metis_tac[])
  *)

(* Expand eta reductions ("pointfree" style) into lambdas *)
val pointful = prove(``∀P f. P (λx. f x) ⇒ P f``, metis_tac[])

val take_steps_def = Define`
  take_steps (steps : sched_step list) (state : sched_state) : sched_state or_error =
    FOLDL (λs step. s >>= try_step step) (OK state) steps
`

(*
val take_steps_ok = store_thm("take_steps_ok",
  ``∀steps. propagates state_ok (take_steps steps)``,
  strip_tac >> match_mp_tac pointful >> rw[take_steps_def] >>
  match_mp_tac foldl_propagates >> simp[try_step_ok])
*)

val get_return_values_def = Define`
  get_return_values state =
    case thread_current_frame_state (ThreadID 0) state of
    | SOME (DEAD (SOME rv)) => return rv
    | SOME (DEAD NONE) => Fail (Impossible "no return value")
    | _ => Fail (Impossible "schedule exhausted")
`

val get_global_value_def = Define`
  get_global_value env name state = do
    n <- FST <$> INDEX_FIND 0 ($= name o FST) env.globals;
    FST <$> get_addr state.memory (Global, n)
  od or_fail Impossible (name ++ " is undefined")
`

val run_finite_schedule_def = Define`
  run_finite_schedule (schedule : sched_step list)
                      (state : sched_state)
                      : sched_state or_error =
    FOLDL (λs step. s >>= try_step step) (OK state) schedule
`

(*
val run_finite_schedule_ok = store_thm("run_finite_schedule_ok",
  ``∀sc. propagates state_ok (run_finite_schedule sc)``,
  strip_tac >> match_mp_tac pointful >> rw[run_finite_schedule_def] >>
  ho_match_mp_tac foldl_resolve_all_lemma >> rw[propagates_def] >>
  FULL_CASE_TAC >> rw[] >> assume_tac try_step_ok >> fs[propagates_def] >>
  metis_tac[])
*)

val run_finite_schedule_last_step = store_thm("run_finite_schedule_last_step",
  ``∀step steps s s'. run_finite_schedule (SNOC step steps) s = OK s'
      ⇒ ∃last. run_finite_schedule steps s = OK last
             ∧ try_step step last = OK s'``,
  rw[run_finite_schedule_def, FOLDL_SNOC] >> qexists_tac `s''` >>
  BasicProvers.FULL_CASE_TAC >> rw[])

val run_finite_schedule_snoc = store_thm("run_finite_schedule_snoc",
  ``∀s step steps. s >>= run_finite_schedule (SNOC step steps)
                 = s >>= run_finite_schedule steps >>= try_step step``,
  Cases >> rw[FUN_EQ_THM] >>
  Cases_on `run_finite_schedule (SNOC step steps) x` >>
  fs[run_finite_schedule_def, FOLDL_SNOC])

val run_schedule_def = Define`
  run_schedule (schedule : sched_step llist)
               (state : sched_state)
               : sched_state or_error =
    SND (WHILE
      (λ(steps, state).
        IS_SOME (LHD steps) ∧
        (case state of OK state => ¬has_terminated state | Fail _ => F))
      (λ(steps, state). THE (LTL steps), state >>= try_step (THE (LHD steps)))
      (schedule, OK state))
`

val run_schedule_terminated = store_thm("run_schedule_terminated",
  ``∀st. has_terminated st ⇒ ∀sc. run_schedule sc st = OK st``,
  rw[run_schedule_def, Once whileTheory.WHILE])

(*
val run_finite_schedule_eq_run_schedule = store_thm("run_finite_schedule_eq_run_schedule",
  ``∀sc st. run_finite_schedule sc st = run_schedule (fromList sc) st``,
  Induct >> rw[run_finite_schedule_def, run_schedule_def]
  >- simp[Once whileTheory.WHILE]
  >- (`run_schedule (fromList sc) st = run_schedule (h:::fromList sc) st`
        suffices_by fs[run_schedule_def, run_finite_schedule_def] >>
      rw[run_schedule_def] >> Cases_on `sc` >>
      simp[Once whileTheory.WHILE] >> simp[Once whileTheory.WHILE])
  >- (simp[Once whileTheory.WHILE] >>
      Cases_on `∃st'. try_step h st = OK st'`
      >- (fs[run_schedule_def, run_finite_schedule_def] >> metis_tac[])
      >- (`∃x. try_step h st = Fail x` suffices_by (
            rw[Once whileTheory.WHILE] >> Cases_on `try_step h st` >>
            fs[uvmThreadsStacksTheory.foldl_fail]) >>
          fs[try_step_def] >> Cases_on `take_step h st` >> fs[] >>
          Cases_on `x` >> fs[])))
*)

val run_finite_schedule_append = store_thm("run_finite_schedule_append",
  ``∀sc1 sc2 st. run_finite_schedule sc1 st >>= run_finite_schedule sc2
               = run_finite_schedule (sc1 ++ sc2) st``,
  strip_tac >> listLib.SNOC_INDUCT_TAC >> strip_tac >>
  fs[run_finite_schedule_def] >| [
    `run_finite_schedule [] = OK` suffices_by simp[] >>
    simp[FUN_EQ_THM, run_finite_schedule_def],

    `sc1 ++ SNOC x sc2 = SNOC x (sc1 ++ sc2)` by simp[] >>
    simp[run_finite_schedule_snoc, FOLDL_SNOC]
  ])

(*
val run_schedule_finite_ok = store_thm("run_schedule_finite_ok",
  ``∀sc. LFINITE sc ⇒ propagates state_ok (run_schedule sc)``,
  rw[] >> match_mp_tac pointful >>
  `propagates state_ok (λx. run_finite_schedule (THE (toList sc)) x)`
    suffices_by rw[run_finite_schedule_eq_run_schedule, to_fromList] >>
  metis_tac[run_finite_schedule_ok])
*)

val finite_schedule_terminates_def = Define`
  finite_schedule_terminates (st : sched_state) (schedule : sched_step list) ⇔
    merge_left (K T) (has_terminated <$> run_finite_schedule schedule st)
`

val schedule_terminates_def = Define`
  schedule_terminates (st : sched_state) (schedule : sched_step llist) ⇔
    ∃n fs. LTAKE n schedule = SOME fs ∧ finite_schedule_terminates st fs
`

val finite_schedule_valid_def = Define`
  finite_schedule_valid (validator : sched_state -> bool)
                        (st : sched_state)
                        (schedule : sched_step list) ⇔
    ∀len. len ≤ LENGTH schedule
        ⇒ merge_left (K T)
            (validator <$> run_finite_schedule (TAKE len schedule) st)
`

val schedule_valid_def = Define`
  schedule_valid (validator : sched_state -> bool)
                 (st : sched_state)
                 (schedule : sched_step llist) ⇔
    ∀len fs. LTAKE len schedule = SOME fs
        ⇒ merge_left (K T) (validator <$> run_finite_schedule fs st)
`

val finite_schedule_terminates_snoc = store_thm("finite_schedule_terminates_snoc",
  ``∀p s. finite_schedule_terminates p s ⇒ ∀x. finite_schedule_terminates p (SNOC x s)``,
  rw[finite_schedule_terminates_def, monadsTheory.merge_left_def] >>
  multi_case_tac >> fs[run_finite_schedule_def, FOLDL_SNOC, try_step_def] >> rw[])

val finite_schedule_terminates_snoc_eq = store_thm("finite_schedule_terminates_snoc_eq",
  ``∀st sc. finite_schedule_terminates st sc
        ⇒ ∀x. run_finite_schedule sc st
            = run_finite_schedule (SNOC x sc) st``,
  rw[finite_schedule_terminates_def, monadsTheory.merge_left_def] >>
  multi_case_tac >> fs[run_finite_schedule_def, FOLDL_SNOC, try_step_def])

val finite_schedule_valid_snoc = store_thm("finite_schedule_valid_snoc",
  ``∀st sc. finite_schedule_terminates st sc
    ⇒ ∀v. finite_schedule_valid v st sc
    ⇒ ∀x. finite_schedule_valid v st (SNOC x sc)``,
  rw[finite_schedule_valid_def] >> Cases_on `len = SUC (LENGTH sc)` >>
  fs[] >> rw[]
  >- (`TAKE (SUC (LENGTH sc)) (SNOC x sc) = SNOC x sc` by (
        `LENGTH (SNOC x sc) = SUC (LENGTH sc)` by simp[] >>
        metis_tac[TAKE_LENGTH_ID]) >>
      imp_res_tac finite_schedule_terminates_snoc_eq >>
      rw[] >> `LENGTH sc ≤ LENGTH sc` by simp[] >>
      metis_tac[TAKE_LENGTH_ID])
  >- (`len ≤ LENGTH sc` by simp[] >>
      `TAKE len (SNOC x sc) = TAKE len sc` suffices_by metis_tac[] >>
      fs[TAKE_APPEND1]))

val finite_schedule_valid_monotonic = store_thm("finite_schedule_valid_monotonic",
  ``∀v v' st sc. (∀x. v x ⇒ v' x)
               ∧ finite_schedule_valid v st sc
               ⇒ finite_schedule_valid v' st sc``,
  rw[finite_schedule_valid_def, monadsTheory.merge_left_def] >> RES_TAC >>
  multi_case_tac >> first_x_assum match_mp_tac >> rfs[])

val schedule_valid_monotonic = store_thm("schedule_valid_monotonic",
  ``∀v v' st sc. (∀x. v x ⇒ v' x) ∧ schedule_valid v st sc
               ⇒ schedule_valid v' st sc``,
  rw[schedule_valid_def, monadsTheory.merge_left_def] >> RES_TAC >>
  multi_case_tac >> first_x_assum match_mp_tac >> rfs[])

val schedule_valid_inter = store_thm("schedule_valid_inter",
  ``∀v v' st sc. schedule_valid v st sc ∧ schedule_valid v' st sc
               ⇒ schedule_valid (v ∩ v') st sc``,
  rw[schedule_valid_def, monadsTheory.merge_left_def] >> RES_TAC >>
  multi_case_tac >> rw[IN_APP] >> rfs[] >> rw[])

val has_thread_with_frame_def = Define`
  has_thread_with_frame tid P st ⇔
    ∃th sk. get_thread tid st = SOME th
          ∧ get_stack th.sid st = SOME sk
          ∧ ¬NULL sk.frames
          ∧ P (HD sk.frames)
`

val (advances_pc_by_rules, advances_pc_by_ind, advances_pc_by_cases) = Hol_reln`
    (∀st tid. advances_pc_by 0 [] st tid)
  ∧ (∀st st' n x xs tid.
      try_step x st = OK st'
    ∧ advances_pc_by n xs st' tid
    ⇒ advances_pc_by (if (x = ExecInst ∧ st.sched_tid = tid ∧ st ≠ st')
                      then SUC n else n) (x::xs) st tid)
`

val advances_pc_by_0_tl = store_thm("advances_pc_by_0_tl",
  ``∀sc0 sc st st' tid.
      advances_pc_by 0 (sc0::sc) st tid
    ∧ try_step sc0 st = OK st'
    ⇒ advances_pc_by 0 sc st' tid``,
  rw[] >> rw_assums_once[advances_pc_by_cases] >> fs[] >> CASE_TAC >> fs[])

val advances_pc_by_le = store_thm("advances_pc_by_le",
  ``∀n sc0 sc st st' tid.
      advances_pc_by n (sc0::sc) st tid
    ∧ try_step sc0 st = OK st'
    ⇒ ∃n'. n' ≤ n ∧ advances_pc_by n' sc st' tid``,
  rw[] >> rw_assums_once[advances_pc_by_cases] >> fs[] >> CASE_TAC >> fs[]
  >- (qexists_tac `n'` >> simp[]) >> prove_tac[LESS_OR_EQ])

(*
val advances_pc_by_le_append = store_thm("advances_pc_by_le_append",
  `` ``
  )

val advances_pc_by_ACTIVE = store_thm("advances_pc_by_ACTIVE",
  ``∀sc st st' tid o0 os.
      opns_ok (o0::os)
    ∧ advances_pc_by 1 sc st tid
    ∧ has_thread_with_frame tid (λf. f.state = ACTIVE (o0::os)) st
    ∧ run_finite_schedule sc st = OK st'
    ⇒ has_thread_with_frame tid (λf. f.state = ACTIVE os) st'``
    )

val has_thread_with_frame_advances_pc_by_0 = store_thm(
  "has_thread_with_frame_advances_pc_by_0",
  ``∀sc st st' tid P.
      advances_pc_by 0 sc st tid
    ∧ has_thread_with_frame tid P st
    ∧ run_finite_schedule sc st = OK st'
    ⇒ has_thread_with_frame tid P st'``,
  Induct >> rw[] >> fs[run_finite_schedule_def, has_thread_with_frame_def] >>
  imp_res_tac advances_pc_by_0_tl >> first_x_assum match_mp_tac >>
  Cases_on `try_step h st` >> rw[] >> fs[foldl_fail] >> qexists_tac `x` >>
  rw[] >> fs[try_step_def] >> multi_case_tac >> Cases_on `h` >| [
    (* ExecInst *)
    POP_ASSUM_LIST (map_every assume_tac) (* reverse argument list *) >>
    pop_assum (assume_tac o ONCE_REWRITE_RULE [advances_pc_by_cases]) >>
    rfs[COND_RAND, try_step_def] >> rw[] >> multi_case_tac >>
    fs[take_step_def] >> multi_case_tac >>
    rfs[exec_non_memory_action_def, exec_load_def, exec_store_def,
        exec_commit_def, exec_reconcile_def, exec_command_def] >>
    TRY (map_every qexists_tac [`th`, `sk`] >> fs[get_thread_def, get_stack_def] >>
         multi_case_tac >> rw[lookup_insert] >> NO_TAC)
        
        
        >> FIRST [

      map_every (fn q => qpat_assum q mp_tac)
                [`FOLDL _ _ rs = _`, `¬NULL sk.frames`] >>
      POP_ASSUM_LIST (K ALL_TAC) >> map_every qid_spec_tac [`sk'`, `rs`] >>
      listLib.SNOC_INDUCT_TAC >> rw[FOLDL_SNOC] >> fs[] >> Cases_on `x` >>
      fs[set_register_def] >> multi_case_tac >> NO_TAC,

      map_every (fn q => qpat_assum q mp_tac)
                [`FOLDL _ _ rs = _`, `P (HD sk.frames)`] >>
      POP_ASSUM_LIST (K ALL_TAC) >> map_every qid_spec_tac [`sk'`, `rs`] >>
      listLib.SNOC_INDUCT_TAC >> rw[FOLDL_SNOC]
      

    ]
    BasicProvers.every_case_t
    
    >| [
      (* st.sched_tid = tid *)
      fs[has_thread_with_frame_def] >> rfs[] >> rw[] >>
      spose_not_then (K all_tac) >> pop_assum mp_tac >> simp[] >>
      map_every qexists_tac [`th`, `sk`] >> rw[] >>
      rfs[take_step_def] >> multi_case_tac >>
      rfs[exec_non_memory_action_def, exec_load_def, exec_store_def,
          exec_commit_def, exec_reconcile_def, exec_command_def,
          get_thread_def] >> rw[] >| [
        (* Nm *)
        rfs[exec_non_memory_action_def] >>
        map_every qexists_tac [`th`, `sk'`] >>
        fs[get_thread_def, get_stack_def] >> rw[] >> fs[lookup_insert] >| [

          map_every (fn q => qpat_assum q mp_tac)
                    [`FOLDL _ _ rs = _`, `¬NULL sk.frames`] >>
          POP_ASSUM_LIST (K ALL_TAC) >> map_every qid_spec_tac [`sk'`, `rs`] >>
          listLib.SNOC_INDUCT_TAC >> rw[FOLDL_SNOC] >> fs[] >> Cases_on `x` >>
          fs[set_register_def] >> multi_case_tac,

          map_every (fn q => qpat_assum q mp_tac)
                    [`FOLDL _ _ rs = _`, `P (HD sk.frames)`] >>
        ]
        Induct_on `rs`
      ]

      rfs[]
    ]

  rw_assums_once[advances_pc_by_cases]
  ]

  TRY (res_tac >> NO_TAC) >> rw[] >> fs[has_thread_with_frame_def] >>
  pop_assum mp_tac >> rw_assums_once[advances_pc_by_cases] >>
  fs[] >> multi_case_tac >> Cases_on `h`
  
  Cases_on `x` >| [
    `has_thread_with_frame tid P x' ∧
     advances_pc_by 0 sc x' tid` suffices_by prove_tac[] >> rw[] >| [
      pop_assum mp_tac >> rw_assums_once[advances_pc_by_cases] >>
      fs[COND_RAND, try_step_def] >> rw[]
      
      Cases_on `h` >> fs[has_thread_with_frame_def, take_step_def] >| [
        Cases_on `st''.sched_tid = tid` >| [
          
        ]
        fs[exec_non_memory_action_def, exec_load_def, exec_store_def,
           exec_commit_def, exec_reconcile_def, exec_command_def,
           get_thread_def]
      ]
      multi_case_tac >>

    (* Case 1: Step fails, returns NONE *)
    imp_reprove_tac[],
    
    (* Case 2: State has already terminated *)
    imp_res_tac advances_pc_by_0_tl >> prove_tac[],

    (* Case 3: Normal execution, state has not terminated *)

  ]
    ntac 2 (pop_assum mp_tac) >> >> rw[] >>
    multi_case_tac >> Cases_on `h = ExecInst` >>
    Cases_on `st''.sched_tid = tid` >> fs[try_step_def, run_finite_schedule_def] >>
    multi_case_tac
    
  ]

val advances_pc_by_0_append = store_thm("advances_pc_by_0_append",
  ``∀sc1 sc2 st st' tid.
      advances_pc_by 0 (sc1 ++ sc2) st tid
    ∧ run_finite_schedule sc1 st = OK st'
    ⇒ advances_pc_by 0 sc1 st tid
    ∧ advances_pc_by 0 sc2 st' tid``,
  rw[run_finite_schedule_def] >| [
    pop_assum mp_tac >> qid_spec_tac `st'` >> Induct_on `sc1` >> rw[] >>
    fs[Once advances_pc_by_cases] >> multi_case_tac >>
    map_every qexists_tac [`DequeueStore`, `st''`, `0`] >> multi_case_tac >>
    Cases_on `sc1` >> fs[advances_pc_by_rules] >> first_x_assum match_mp_tac
  ]
*)
(*
val finite_schedule_valid_state_ok = store_thm("finite_schedule_valid_state_ok",
  ``∀st sc. finite_schedule_valid state_ok st sc``,
  rw[finite_schedule_valid_def, monadsTheory.merge_left_def] >>
  multi_case_tac >>
  prove_tac[new_sched_state_ok, run_finite_schedule_ok, propagates_def])

val schedule_valid_state_ok = store_thm("schedule_valid_state_ok",
  ``∀p s. schedule_valid state_ok p s``,
  rw[schedule_valid_def, sumMonadTheory.merge_left_def] >>
  multi_case_tac >>
  prove_tac[init_sched_state_ok, run_finite_schedule_ok, propagates_def])
*)
(*
val generate_valid_schedule_def = Define`
  generate_valid_schedule (valid : sched_state -> bool)
                          (prg : program)
                          (steps : sched_step llist)
                          : sched_step list option =
    let init = new_sched_state prg in
    if ¬IMAGE OK valid init then NONE else
    FST (WHILE
      (λ(sched, state).
        IS_SOME sched ∧
        (case state of Fail _ => F | OK s => ¬has_terminated s))
      (λ(sched, state).
        case LHD (WHILE (λsteps.
          merge_left ($= InvalidStep) (do
            step <- expect (LHD steps) (InvalidState ARB);
            state <- state;
            next <- take_step step state;
            return (valid next)
          od)) (THE o LTL) steps) of
        | SOME next =>
            (SOME (SNOC next (THE sched)), state >>= try_step next)
        | NONE => (NONE, state))
      (SOME [], init))
`

val generate_valid_schedule_terminates = store_thm("generate_valid_schedule_terminates",
  ``∀v vs p s. generate_valid_schedule v p s = SOME vs
             ⇒ finite_schedule_terminates p vs``,
  strip_tac >> `∀x. x ∈ v ⇒ v x` by metis_tac[pred_setTheory.IN_APP] >> Induct >>
  rw[generate_valid_schedule_def, finite_schedule_terminates_def,
     sumMonadTheory.merge_left_def] >> CASE_TAC >> fs[run_finite_schedule_def] >>
  rw[] >> fs[sumMonadTheory.lift_left_def] >> rw[]
  >- (fs[Once whileTheory.WHILE] >> SPOSE_NOT_THEN MP_TAC >> strip_tac >> fs[] >>
      POP_ASSUM (K ALL_TAC) >> POP_ASSUM (K ALL_TAC) >> POP_ASSUM MP_TAC >>
      CASE_TAC >> fs[Once whileTheory.WHILE]
      Q.MATCH_ABBREV_TAC `
        FST (WHILE wcond wbody
          case winit of
          | NONE => (NONE, OK (resolve_all y0)) 
          | SOME next => (SOME [next], try_step next (resolve_all y0))
        ) = SOME [] ⇒ F` >>
      Cases_on 
     [Once whileTheory.WHILE]
  listLib.SNOC_INDUCT_TAC

val generate_valid_schedule_valid = store_thm("generate_valid_schedule_valid",
  ``∀v vs p s. generate_valid_schedule v p s = SOME vs
             ⇒ finite_schedule_valid v p vs``,
  strip_tac >> `∀x. x ∈ v ⇒ v x` by metis_tac[pred_setTheory.IN_APP] >>
  rw[sumMonadTheory.merge_left_def, finite_schedule_valid_def,
     generate_valid_schedule_def] >>
  CASE_TAC >> fs[] >> rw[] >> fs[] >> rw[] >>
  REPEAT (POP_ASSUM MP_TAC) >>
  SPEC_TAC (``y0': sched_state``, ``y0': sched_state``) >>
  Induct_on `len`
  >- (rw[run_finite_schedule_def] >> fs[])
  >- (rw[] >> fs[])
  >> rw[] >> fs[] >>
  `TAKE (SUC len) vs = `
  
  SPEC_TAC (``len``, ``len``)
  
  listLib.SNOC_INDUCT_TAC
  >- (rw[sumMonadTheory.merge_left_def, finite_schedule_valid_def,
         generate_valid_schedule_def] >>
      CASE_TAC >> fs[run_finite_schedule_def] >> rw[] >> fs[])
  >- (rw[sumMonadTheory.merge_left_def, finite_schedule_valid_def,
         generate_valid_schedule_def] >>
      CASE_TAC >> fs[run_finite_schedule_def])
)
*)

(* A schedule is fair iff, at any point in the schedule, all possible steps
   occur at some future point in the schedule.
*)
val fair_def = Define`
  fair (s : sched_step llist) : bool ⇔
    ∀step n. ∃j. LNTH (n + j) s = SOME step
`

(* A schedule is fair within the bounding set `m` iff, for some length `l`, at
   any point in the schedule, every step in `m` is at most `l` steps away.
*)
val fair_bounded_def = Define`
  fair_bounded (m : sched_step set) (l : num) (s : sched_step llist) : bool ⇔
    ∀step. step ∈ m ⇒ ∀n. ∃j. j ≤ l ∧ (LNTH (n + j) s = SOME step) 
`

(* A finite schedule is fair within the bounding set `m` iff, for some length
   `l`, at any point in the schedule, if there are at least `l` steps remaining,
   every step in `m` is at most `l` steps away.

   A finite schedule cannot be fair for an infinite set `m`, or for a length `l`
   that is less than the cardinality of `m`, as no possible schedule could
   satisfy these requirements.
*)
val fair_finite_def = Define`
  fair_finite (m : sched_step set) (l : num) (s : sched_step list) : bool ⇔
    FINITE m ∧ CARD m ≤ SUC l ∧
    ∀step. step ∈ m ⇒ ∀n. (n ≤ LENGTH s - l) ⇒ ∃j. j ≤ l ∧ (EL (n + j) s = step) 
`

local
  open optionTheory
  open pred_setTheory

  val fair_bounded_lemma = Q.prove(
    `fair_bounded m l s ⇒ m ⊆ IMAGE (λn. THE (LNTH n s)) (count (SUC l))`,
    rw[fair_bounded_def, SUBSET_DEF] >>
    metis_tac[THE_DEF, LESS_EQ_IMP_LESS_SUC, ADD_0])
in
  val fair_weak_corollary = Q.store_thm("fair_weak_corollary",
    `∀s. fair s ⇒ ∀step. exists ($= step) s`,
    rw[fair_def, exists_LNTH] >> metis_tac[])

  val fair_infinite = Q.store_thm("fair_infinite",
    `∀s. fair s ⇒ ¬LFINITE s`,

    SPOSE_NOT_THEN STRIP_ASSUME_TAC >> fs[fair_def, LFINITE_LNTH_NONE] >>
    `∀j. n ≤ j + n` by simp[] >> metis_tac[LNTH_NONE_MONO, NOT_SOME_NONE])

  val fair_lcons = Q.store_thm("fair_lcons",
    `∀s. fair s ⇔ ∀s'. fair (s':::s)`,

    strip_tac >>

    `(∀s'. fair (s':::s)) ⇒ fair s` by (
      rw[fair_def] >> Induct_on `n` >> rw[] >>
      metis_tac[LNTH, LTL_LCONS, OPTION_JOIN_DEF, OPTION_MAP_DEF, ADD_SUC]) >>

    `fair s ⇒ ∀s'. fair (s':::s)` by (
      rw[fair_def] >> `∀step. ∃j. LNTH j s = SOME step` by metis_tac[] >>
      Induct_on `n` >> rw[] >>
      metis_tac[LNTH, LTL_LCONS, OPTION_JOIN_DEF, OPTION_MAP_DEF, ADD_SUC]) >>

    metis_tac[])

  val fair_bounded_weak_corollary = Q.store_thm("fair_bounded_weak_corollary",
    `∀m s. (∃l. fair_bounded m l s) ⇒ ∀step. step ∈ m ⇒ exists ($= step) s`,
    rw[fair_bounded_def, exists_LNTH] >> metis_tac[])

  val fair_bounded_infinite = Q.store_thm("fair_bounded_infinite",
    `∀m l s. fair_bounded m l s ∧ m ≠ {} ⇒ ¬LFINITE s`,

    SPOSE_NOT_THEN STRIP_ASSUME_TAC >> fs[fair_bounded_def, LFINITE_LNTH_NONE] >>
    `∀j. n ≤ j + n` by simp[] >>
    metis_tac[LNTH_NONE_MONO, NOT_SOME_NONE, MEMBER_NOT_EMPTY])

  val fair_bounded_finite_set = Q.store_thm("fair_bounded_finite_set",
    `∀m l s. fair_bounded m l s ⇒ FINITE m`,
    REPEAT strip_tac >>
    metis_tac[fair_bounded_lemma, SUBSET_FINITE, FINITE_COUNT, IMAGE_FINITE])

  val fair_bounded_card = Q.store_thm("fair_bounded_card",
    `∀m l s. fair_bounded m l s ⇒ CARD m ≤ SUC l`,
    REPEAT strip_tac >>
    metis_tac[fair_bounded_lemma, CARD_COUNT, CARD_IMAGE, CARD_SUBSET,
      SUBSET_FINITE, FINITE_COUNT, IMAGE_FINITE, LESS_EQ_TRANS])

  val fair_finite_weak_corollary = Q.store_thm("fair_finite_weak_corollary",
    `∀m l s. fair_finite m l s ∧ l < LENGTH s ⇒ m ⊆ set s`,
    rw[fair_finite_def] >>
    `∀step. step ∈ m ⇒ ∃j. j ≤ l ∧ EL j s = step` by
      (fs[SUB_LEFT_LESS_EQ] >> metis_tac[ADD_0, LESS_0_CASES]) >>
    metis_tac[MEM, MEM_EL, LIST_TO_SET, LESS_EQ_LESS_TRANS, INSERT_DEF, SUBSET_DEF])

  (*
  val fair_bounded_ltake_fair_finite = Q.store_thm("fair_bounded_ltake_fair_finite",
    `∀m l s n. fair_bounded m l s ∧ l < n ⇒ fair_finite m l (THE (LTAKE n s))`,
    rw[fair_finite_def]
    >- metis_tac[fair_bounded_finite_set]
    >- metis_tac[fair_bounded_card]
    >- (
    fs[fair_bounded_def] >>
    metis_tac[LTAKE_LNTH_EL, THE_DEF, LESS_EQ_IMP_LESS_SUC, LTAKE_LENGTH]
    )
  ) *)

end

val cycle_def = Define`
  cycle (xs : α list) : α llist =
    LUNFOLD (λn. SOME (SUC n MOD LENGTH xs, EL n xs)) 0
`

val cycle_lnth =
  let
    val lemma = Q.prove(
      `∀m n f g. LNTH n (LUNFOLD (λn. SOME (f n, g n)) m)
               = LNTH 0 (LUNFOLD (λn. SOME (f n, g n)) (FUNPOW f n m))`,
      Induct_on `n` >>
      fs[FUNPOW_SUC, LNTH_LUNFOLD] >> metis_tac[FUNPOW_SUC, FUNPOW])
    val lemma' = SIMP_RULE (srw_ss()) [LNTH_LUNFOLD] lemma
  in
    Q.store_thm("cycle_nth",
      `∀n xs. xs ≠ [] ⇒ (LNTH n (cycle xs) = SOME (EL (n MOD (LENGTH xs)) xs))`,

      rw[cycle_def, lemma'] >>
      `FUNPOW (λn. SUC n MOD LENGTH xs) n 0 = n MOD LENGTH xs` suffices_by simp[] >>
      Induct_on `n` >>
      `0 ≠ LENGTH xs` by metis_tac[LENGTH_NIL] >>
      simp[FUNPOW_SUC, MOD_MOD] >>
      asm_simp_tac(srw_ss() ++ numSimps.MOD_ss ++ numSimps.ARITH_ss)[ADD1])
  end

val cycle_lnth_mem = store_thm("cycle_lnth_mem",
  ``∀n xs x. xs ≠ [] ∧ LNTH n (cycle xs) = SOME x ⇒ MEM x xs``,
  rw[MEM_EL] >> imp_res_tac cycle_lnth >> fs[] >>
  qexists_tac `n MOD LENGTH xs` >> rw[] >>
  match_mp_tac MOD_LESS >> prove_tac[LENGTH_NIL, LESS_0_CASES])

(*
val cycle_ltake_mem = store_thm("cycle_ltake_mem",
  ``∀n x xs xs'. xs ≠ [] ∧ LTAKE n (cycle xs) = SOME xs' ⇒ MEM x xs' ⇒ MEM x xs``,
  Induct >> rw[] >> fs[] >>
  Cases_on `LTAKE n (cycle xs)` >> fs[LTAKE_SNOC_LNTH] >>
  `LNTH n (cycle xs) = SOME (EL (n MOD (LENGTH xs)) xs)`
    by metis_tac[cycle_lnth] >>
  fs[] >> rw[] >> fs[] >> simp[MEM_EL] >>
  qexists_tac `n MOD LENGTH xs` >>
  `0 ≠ LENGTH xs` by metis_tac[LENGTH_NIL] >> fs[])
*)

val naive_schedule_def = Define`
  naive_schedule : sched_step llist =
    cycle [ExecInst; DequeueStore; NextAddr; NextThread; DequeueInvalidation]
`

val _ = export_theory()

