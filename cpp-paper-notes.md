## Specifying a Micro Virtual Machine above Weak Memory

- Specification of realistic virtual machine IR down to weak memory
- Execution of concrete IR programs on spec
- Verification of simple optimizations
- Enrichment of WMM
    - AtomicRMW
    - Connection to higher-level concepts (SeqCst, C11 memory model)
- Why?
    - Goal is to produce completely verified stack (from OS to software)
    - Verification of spec, so that applications can be verified against spec

