open HolKernel Parse boolLib bossLib;

open uvmTypesTheory
open uvmValuesTheory
open uvmErrorsTheory
open uvmMemoryTheory
open monadsTheory
open extraTactics

open alistTheory
open finite_mapTheory
open listTheory
open monadsyntax

val _ = new_theory "uvmIR"

val _ = Datatype`
  block_label = BlockLabel string ;
  const_name  = ConstName string ;
  type_name   = TypeName string ;
  sig_name    = SigName string ;
  trap_data   = TrapData num
`

(* Memory order (see the spec)
   https://github.com/microvm/microvm-spec/blob/master/memory-model.rest#memory-operations
*)
val _ = Datatype`
  memory_order =
  | NOT_ATOMIC
  | RELAXED
  | CONSUME
  | ACQUIRE
  | RELEASE
  | ACQ_REL
  | SEQ_CST
`

(* AtomicRMW operators (see the spec)
   https://github.com/microvm/microvm-spec/blob/master/instruction-set.rest#atomicrmw-instruction
*)
val _ = Datatype`
  atomicrmw_op =
  | RMW_XCHG (* <any> - exchange *)
  | RMW_ADD  (* <int> - add *)
  | RMW_SUB  (* <int> - subtract *)
  | RMW_AND  (* <int> - bitwise and *)
  | RMW_NAND (* <int> - bitwise nand *)
  | RMW_OR   (* <int> - bitwise or *)
  | RMW_XOR  (* <int> - bitwise xor *)
  | RMW_MAX  (* <int> - signed max *)
  | RMW_MIN  (* <int> - signed min *)
  | RMW_UMAX (* <int> - unsigned max *)
  | RMW_UMIN (* <int> - unsigned min *)
`

(* Binary operations (see the spec)
   https://github.com/microvm/microvm-spec/blob/master/instruction-set.rest#binary-operations
*)
val _ = Datatype`
  bin_op =
  | ADD  (* <int> - add *)
  | SUB  (* <int> - subtract *)
  | MUL  (* <int> - multiply *)
  | SDIV (* <int> - signed divide *)
  | SREM (* <int> - signed remainder *)
  | UDIV (* <int> - unsigned divide *)
  | UREM (* <int> - unsigned remainder *)
  | SHL  (* <int> - left shift *)
  | LSHR (* <int> - logical right shift *)
  | ASHR (* <int> - arithmetic right shift *)
  | AND  (* <int> - bitwise and *)
  | OR   (* <int> - bitwise or *)
  | XOR  (* <int> - bitwise exclusive or *)
  | FADD (* <float|double> - FP add *)
  | FSUB (* <float|double> - FP subtract *)
  | FMUL (* <float|double> - FP multiply *)
  | FDIV (* <float|double> - FP divide *)
  | FREM (* <float|double> - FP remainder *)
`

val exec_bin_op_def = Define`
    exec_bin_op ADD : value -> value -> value + type_error = int_binop $+
  ∧ exec_bin_op SUB = int_binop $-
  ∧ exec_bin_op MUL = int_binop $*
  ∧ exec_bin_op SDIV = K (K (Fail (NotImplemented "SDIV")))
  ∧ exec_bin_op SREM = K (K (Fail (NotImplemented "SREM")))
  ∧ exec_bin_op UDIV = K (K (Fail (NotImplemented "UDIV")))
  ∧ exec_bin_op UREM = K (K (Fail (NotImplemented "UREM")))
  ∧ exec_bin_op _ = K (K (Fail (NotImplemented "bitwise and fp binops")))
`

(* Binary operation types: Relation between a binary operation * and a triple of
   types (α, β, ρ) such that (a: α) * (b: β) = (r: ρ).
*)
val (bin_op_type_rules, bin_op_type_ind, bin_op_type_cases) = Hol_reln`
  (∀n opn. opn ∈ {ADD; SUB; MUL; SDIV; SREM; UDIV; UREM; AND; OR; XOR} ⇒
           bin_op_type opn (Int n) (Int n) (Int n)) ∧

  (∀n m opn. opn ∈ {SHL; LSHR; ASHR} ⇒
             bin_op_type opn (Int n) (Int m) (Int n)) ∧

  (∀ty opn. fp_type ty ∧ opn ∈ {FADD; FSUB; FMUL; FDIV; FREM} ⇒
            bin_op_type opn ty ty ty)
`

(* Comparison operations (see the spec)
   https://github.com/microvm/microvm-spec/blob/master/instruction-set.rest#comparison
*)
val _ = Datatype`
  cmp_op =
  | EQ     (* <any EQ-comparable> - equal *)
  | NE     (* <any EQ-comparable> - not equal *)
  | SGE    (* <int> - signed greater than or equal *)
  | SGT    (* <int> - signed greater than *)
  | SLE    (* <int> - signed less than or equal *)
  | SLT    (* <int> - signed less than *)
  | UGE    (* <any ULT-comparable> - unsigned greater than or equal *)
  | UGT    (* <any ULT-comparable> - unsigned greater than *)
  | ULE    (* <any ULT-comparable> - unsigned less than or equal *)
  | ULT    (* <any ULT-comparable> - unsigned less than *)
  | FFALSE (* <float|double> - always false *)
  | FTRUE  (* <float|double> - always true *)
  | FOEQ   (* <float|double> - ordered equal *)
  | FOGT   (* <float|double> - ordered greater than *)
  | FOGE   (* <float|double> - ordered greater than or equal *)
  | FOLT   (* <float|double> - ordered less than *)
  | FOLE   (* <float|double> - ordered less than or equal *)
  | FONE   (* <float|double> - ordered not equal *)
  | FORD   (* <float|double> - ordered *)
  | FUEQ   (* <float|double> - unordered equal *)
  | FUGT   (* <float|double> - unordered greater than *)
  | FUGE   (* <float|double> - unordered greater than or equal *)
  | FULT   (* <float|double> - unordered less than *)
  | FULE   (* <float|double> - unordered less than or equal *)
  | FUNE   (* <float|double> - unordered not equal *)
  | FUNO   (* <float|double> - unordered *)
`

(* The result type of a comparison operation on a given type *)
val cmp_result_def = Define`
  cmp_result (Vector _ sz) = Vector (Int 1) sz ∧
  cmp_result _ = Int 1
`

(* Comparison operation types: Relation between a comparison operation ≤ and a
   triple of types (α, β, ρ) such that (a: α) ≤ (b: β) = (r: ρ).
*)
val (cmp_op_type_rules, cmp_op_type_ind, cmp_op_type_cases) = Hol_reln`
  (∀iop ity.
      maybeVector eq_comparable_type ity ∧ iop ∈ {EQ ; NE}
    ⇒
      cmp_op_type iop ity ity (cmp_result ity)) ∧

  (∀iop ity.
      maybeVector ({Int n | T} ∪ {IRef REF t | T} ∪ pointer_type) ity ∧
      iop ∈ { UGE ; UGT ; ULE ; ULT}
    ⇒
      cmp_op_type iop ity ity (cmp_result ity)) ∧

  (∀iop ity.
       maybeVector {Int n | T} ity ∧
       iop ∈ {SGE ; SGT ; SLE ; SLT}
    ⇒
       cmp_op_type iop ity ity (cmp_result ity)) ∧

  (∀fop fty.
      (floating_point_type fty ∨
      (∃sz fty0. floating_point_type fty0 ∧ fty = Vector fty0 sz)) ∧
      fop ∈ { FFALSE ; FTRUE ; FOEQ ; FOGT ; FOGE ; FOLT ; FOLE ; FONE ;
              FORD ; FUEQ ; FUGT ; FUGE ; FULT ; FULE ; FUNE ; FUNO }
    ⇒
      cmp_op_type fop fty fty (cmp_result fty))
`

(* Either a variable or a constant *)
val _ = Datatype`
  ssa_or_const =
  | Var ssavar
  | Const uvm_type value (value list)
`

(* A block label with arguments *)
val _ = type_abbrev("destination", ``:block_label # ssa_or_const list``)

val _ = Datatype`
  resumption_data = <|
    normal_dest : destination ;
    exceptional_dest : destination option
  |>
`

val _ = Datatype`
  callconvention =
  | Mu
  | Foreign ffitype ;

  ffitype = FFIType string
`

val _ = Datatype`
  calldata = <|
    name : ssavar + func_name ;  (* allowing for indirect calls *)
    args : ssa_or_const list ;
    signature : funcsig ;
    convention : callconvention
  |>
`

val get_callee_def = Define`
  get_callee (callee : ssa_or_const) : (ssavar + func_name) + type_error =
    case callee of
    | Var v => return (INL v)
    | Const _ c _ => INR o SND o THE <$> get_funcref_data c
`

(* Type for expressions, instructions which do not contain a reference to their
   output variable(s).

   Note that the σ type of an expression should allow for literal constants;
   that is, the expression type contained in a `σ instruction` is a
   `σ or_const expression`.
*)
val _ = Datatype`
  expression =

  (* Basic Operations *)

  | Id (* no-op identity expr, yields its argument *)
      uvm_type  (* type of argument *)
      ssa_or_const (* argument *)
  | BinOp (* performs arithmetic, yields a value *)
      bin_op (* operation to perform *)
      uvm_type  (* type of both operands *)
      ssa_or_const (* op1 *)
      ssa_or_const (* op2 *)
  | CmpOp (* performs a comparison, yields an int<1> (or a vector) *)
      cmp_op (* operation to perform *)
      uvm_type  (* type of both operands *)
      ssa_or_const (* op1 *)
      ssa_or_const (* op2 *)
  | ConvOp (* converts between two types *)
      convtype (* operation to perform *)
      uvm_type  (* source type *)
      uvm_type  (* destination type *)
      ssa_or_const (* operand *)
  | Select (* Given a boolean (int<1>), select one of two values *)
      uvm_type  (* cond type *)
      uvm_type  (* ifTrue, ifFalse type *)
      ssa_or_const (* cond *)
      ssa_or_const (* ifTrue *)
      ssa_or_const (* ifFalse *)

  (* Aggregate Type Operations *)

  | ExtractValue (* get field from struct in SSA var *)
      uvm_type  (* type of opnd, should be struct *)
      num  (* field index *)
      ssa_or_const (* opnd - struct *)
  | InsertValue (* set field in struct in SSA var *)
      uvm_type  (* type of opnd, should be struct *)
      num  (* field index *)
      ssa_or_const (* opnd - struct to insert into *)
      ssa_or_const (* value to insert into struct *)
  | ExtractElement (* get element from array/vector in SSA var *)
      uvm_type  (* type of opnd, should be array/vector *)
      uvm_type  (* type of index, should be int *)
      ssa_or_const (* opnd - array/vector *)
      ssa_or_const (* index *)
  | InsertElement (* set element in array/vector in SSA var *)
      uvm_type  (* type of opnd, should be array/vector *)
      uvm_type  (* type of index, should be int *)
      ssa_or_const (* opnd - array/vector *)
      ssa_or_const (* index *)
      ssa_or_const (* value to insert *)
  | ShuffleVector (* mix two vectors according to a mask *)
      uvm_type  (* type of opnd, should be vector *)
      uvm_type  (* type of mask, should be vector of any int type *)
      ssa_or_const (* vec1 *)
      ssa_or_const (* vec2 *)
      ssa_or_const (* mask *)

  (* Memory Addressing *)

  | GetIRef (* convert a ref into an iref *)
      uvm_type  (* referent type of opnd *)
      ssa_or_const (* opnd, should be a ref *)
  | GetFieldIRef (* move iref to struct/hybrid field *)
      ref_type (* REF/PTR *)
      uvm_type  (* referent type of opnd - should be struct or hybrid *)
      num  (* field index *)
      ssa_or_const (* opnd - iref/ptr *)
  | GetElementIRef (* move iref to array element *)
      ref_type (* REF/PTR *)
      uvm_type  (* referent type of opnd - should be array *)
      uvm_type  (* index type - should be int *)
      ssa_or_const (* opnd - iref/ptr *)
      ssa_or_const (* index *)
  | ShiftIRef (* move iref by specific offset, yields same iref type *)
      ref_type (* REF/PTR *)
      uvm_type  (* referent type of opnd *)
      uvm_type  (* offset type - should be int *)
      ssa_or_const (* opnd - iref/ptr *)
      ssa_or_const (* offset *)
  | GetVarPartIRef (* yields iref/ptr to first element of var-part of hybrid, if it exists *)
      ref_type (* REF/PTR *)
      uvm_type  (* referent type of opnd - should be hybrid *)
      ssa_or_const (* opnd - iref/ptr *)
`

(* Types for instructions, both normal and terminal. Use `Assign` to create a
   `σ instruction` from a `σ or_const expression`.
*)
val _ = Datatype`
  instruction =
  | Assign
      (ssavar list) (* output variable(s) *)
      expression
  | Load
      ssavar (* destination variable  *)
      ref_type (* REF/PTR *)
      uvm_type  (* referent type of source *)
      ssavar (* source - iref/uptr *)
      memory_order
  | Store
      ssa_or_const (* value to be written *)
      ref_type (* REF/PTR *)
      uvm_type  (* referent type of destination *)
      ssavar (* destination - iref/uptr *)
      memory_order
  | CmpXchg
      ssavar (* output1 : oldvalue *)
      ssavar (* output2 : boolean for whether compare succeeded *)
      ref_type (* REF/PTR *)
      strength (* STRONG/WEAK *)
      memory_order (* success order *)
      memory_order (* failure order *)
      uvm_type  (* referent type of location *)
      ssavar (* memory location - iref/uptr *)
      ssa_or_const (* expected value *)
      ssa_or_const (* desired value *)
  | AtomicRMW
      ssavar (* output: old memory value *)
      ref_type (* REF/PTR *)
      memory_order
      atomicrmw_op
      uvm_type  (* referent type of location*)
      ssavar (* memory location - iref/uptr *)
      ssa_or_const (* operand for op *)
  | Fence memory_order
  | New
      ssavar (* output : heap-allocated ref *)
      uvm_type (* type - must not be hybrid *)
  | Alloca
      ssavar (* output : stack-allocated iref *)
      uvm_type
  | NewHybrid
      ssavar (* output : heap-allocated ref *)
      uvm_type (* hybrid type *)
      uvm_type (* length type *)
      ssa_or_const (* length of varying part (can be zero);
                      will cause u.b., or raise exn if
                      GetVarPartIRef call is made on return value *)
  | AllocaHybrid
      ssavar (* output : stack-allocated iref *)
      uvm_type
      uvm_type
      ssa_or_const
  | NewThread
      ssavar (* output: threadref *)
      ssa_or_const (* stack *)
      ssa_or_const (* threadlocal *)
      (ssa_or_const new_stack_clause) ;

  terminst =
  | Ret (ssa_or_const list)
  | Throw ssa_or_const
  | Call (ssavar list) calldata resumption_data
  | TailCall calldata
  | Branch1 destination
  | Branch2 ssa_or_const destination destination
  | Switch
      uvm_type
      ssa_or_const
      destination
      ((value # destination) list)
  | Watchpoint
      ((wpid # destination) option)
         (* NONE = unconditional trap *)
         (* SOME(wpid, dest) = conditional on wpid, trap if set;
                               if not, branch to dest *)
      (uvm_type list) (* return types *)
      resumption_data
  | WPBranch wpid destination destination
  | SwapStack
      (ssavar list)
      ssa_or_const (* swappee - stackref *)
      cur_stack_clause
      (ssa_or_const new_stack_clause)
      resumption_data
  | CommInst
      (ssavar list)
      comminst
      resumption_data
  | ExcClause
      instruction
      resumption_data ;

  (* Wrapping expressions with ExcClause forces the implementation to
     gracefully handle what would have otherwise been undefined
     behaviour. For example, an unwrapped division lets demons fly out
     of your nose if the second argument is 0. On the other hand, if the
     client wraps a division with resumption_data, the implementation
     must check for the zero argument and/or set up the appropriate
     signal handling so that the exceptional branch can get called when
     the second argument is indeed 0.
  *)

  wpid = WPID num ;

  strength = STRONG | WEAK ;

  new_stack_clause =
  | PassValues ((σ # uvm_type) list)
  | ThrowExc σ ;

  cur_stack_clause =
  | RetWith (uvm_type list)
  | KillOld ;

  comminst =
  | NewStack funcsig ssa_or_const
  | CurrentStack
  | ThreadExit
`

val var_list_def = Define `var_list (Var s) = [s] ∧ var_list _ = []`

val new_stack_clause_vars_def = Define`
    new_stack_clause_vars (PassValues vs) = vs >>= var_list o FST
  ∧ new_stack_clause_vars (ThrowExc e) = var_list e
`

(* Given an expression, returns a list of all variables read by the expression
   (its _input variables_).
*)
val expr_vars_def = Define`
    expr_vars (Id _ v) = [v]
  ∧ expr_vars (BinOp _ _ a b) = nub [a; b]
  ∧ expr_vars (CmpOp _ _ a b) = nub [a; b]
  ∧ expr_vars (ConvOp _ _ _ v) = [v]
  ∧ expr_vars (Select _ _ c t f) = nub [c; t; f]

  ∧ expr_vars (ExtractValue _ _ s) = nub [s]
  ∧ expr_vars (InsertValue _ _ s v) = nub [s; v]
  ∧ expr_vars (ExtractElement _ _ a i) = nub [a; i]
  ∧ expr_vars (InsertElement _ _ a i v) = nub [a; i; v]
  ∧ expr_vars (ShuffleVector _ _ v1 v2 m) = nub [v1; v2; m]

  ∧ expr_vars (GetIRef _ ref) = [ref]
  ∧ expr_vars (GetFieldIRef _ _ _ ref) = [ref]
  ∧ expr_vars (GetElementIRef _ _ _ ref index) = nub [ref; index]
  ∧ expr_vars (ShiftIRef _ _ _ ref offset) = nub [ref; offset]
  ∧ expr_vars (GetVarPartIRef _ _ ref) = nub [ref]
`

(* Given an expression, returns a list the types of its output values. *)
val expr_types_def = Define`
    expr_types (Id ty _) = [ty]
  ∧ expr_types (BinOp _ ty _ _) = [ty]
  ∧ expr_types (CmpOp _ _ _ _) = [Int 1]
  ∧ expr_types (ConvOp _ _ ty _) = [ty]
  ∧ expr_types (Select _ ty _ _ _) = [ty]
  ∧ expr_types (ExtractValue ty n _) = [EL n (member_types (SOME 0) ty)]
  ∧ expr_types (InsertValue ty _ _ _) = [ty]
  ∧ expr_types (ExtractElement ty _ _ _) = [HD (member_types (SOME 0) ty)]
  ∧ expr_types (InsertElement ty _ _ _ _) = [ty]
  ∧ expr_types (ShuffleVector ty _ _ _ _) = [ty]
  ∧ expr_types (GetIRef ty _) = [IRef REF (Type ty)]
  ∧ expr_types (GetFieldIRef rt ty n _) =
      [IRef rt (Type (EL n (member_types (SOME 0) ty)))]
  ∧ expr_types (GetElementIRef rt ty _ _ _) =
      [IRef rt (Type (HD (member_types (SOME 0) ty)))]
  ∧ expr_types (ShiftIRef rt ty _ _ _) = [IRef rt (Type ty)]
  ∧ expr_types (GetVarPartIRef rt ty _) =
      let var_part = LENGTH (member_types (SOME 0) ty) in
      [IRef rt (Type (EL var_part (member_types (SOME 1) ty)))]
`

(* Given an instruction, returns a pair of lists (input variables, output
   variables). The union of these lists is the list of all variables referenced
   by the instruction.
*)
val inst_vars_def = Define`
    inst_vars (Assign vs e) = (expr_vars e >>= var_list, nub vs)
  ∧ inst_vars (Load v _ _ src _) = ([src], [v])
  ∧ inst_vars (Store src _ _ dst _) = (var_list src ++ [dst], [])
  ∧ inst_vars (CmpXchg v1 v2 _ _ _ _ _ loc exp des) =
      (nub (loc::var_list exp ++ var_list des), nub [v1; v2])
  ∧ inst_vars (AtomicRMW v _ _ _ _ loc opnd) = (nub (loc::var_list opnd), [v])
  ∧ inst_vars (Fence _) = ([], [])
  ∧ inst_vars (New v _) = ([], [v])
  ∧ inst_vars (Alloca v _) = ([], [v])
  ∧ inst_vars (NewHybrid v _ _ len) = (var_list len, [v])
  ∧ inst_vars (AllocaHybrid v _ _ len) = (var_list len, [v])
  ∧ inst_vars (NewThread v s tl nsc) =
      (nub (var_list s ++ var_list tl ++ new_stack_clause_vars nsc), [v])
`

val inst_input_vars_def = Define`inst_input_vars i = FST (inst_vars i)`

val inst_output_vars_def = Define`inst_output_vars i = SND (inst_vars i)`

val inst_all_vars_def = Define`
  inst_all_vars i = let (a, b) = inst_vars i in nub (a ++ b)
`

(* Given an instruction, returns an association list from SSA variables to
   types. The list only contains new SSA variables defined by the instruction.
*)
val inst_types_def = Define`
    inst_types (Assign vs e) = ZIP (vs, expr_types e)
  ∧ inst_types (Load dst _ ty _ _) = [(dst, ty)]
  ∧ inst_types (Store _ _ _ _ _) = []
  ∧ inst_types (CmpXchg v1 v2 _ _ _ _ ty _ _ _) = [(v1, ty); (v2, Int 1)]
  ∧ inst_types (AtomicRMW v _ _ _ ty _ _) = [(v, ty)]
  ∧ inst_types (Fence _) = []
  ∧ inst_types (New v ty) = [(v, Ref (Type ty))]
  ∧ inst_types (Alloca v ty) = [(v, Ref (Type ty))]
  ∧ inst_types (NewHybrid v ty _ _) = [(v, Ref (Type ty))]
  ∧ inst_types (AllocaHybrid v ty _ _) = [(v, Ref (Type ty))]
  ∧ inst_types (NewThread v _ _ _) = [(v, ThreadRef)]
`

val comminst_vars_def = Define`
    comminst_vars (NewStack _ fref) = var_list fref
  ∧ comminst_vars CurrentStack = []
  ∧ comminst_vars ThreadExit = []
`

val comminst_types_def = Define`
    comminst_types (NewStack _ _) = [StackRef]
  ∧ comminst_types CurrentStack = [StackRef]
  ∧ comminst_types ThreadExit = []
`

(* Given a terminating instruction, returns a pair of lists (input variables,
   output variables). The union of these lists is the list of all variables
   referenced by the instruction.
*)
val terminst_vars_def = Define`
  terminst_vars (inst : terminst) : ssavar list # ssavar list =
    let dest_vars dest = SND dest >>= var_list in
    let rd_vars rd =
      dest_vars rd.normal_dest ++
      option_CASE rd.exceptional_dest [] dest_vars in
    case inst of
    | Ret vals => nub (vals >>= var_list), []
    | Throw v => nub (var_list v), []
    | Call vs cd rd =>
        nub (left_list cd.name ++ cd.args >>= var_list ++ rd_vars rd),
        nub vs
    | TailCall cd => nub (left_list cd.name ++ cd.args >>= var_list), []
    | Branch1 dst => nub (dest_vars dst), []
    | Branch2 cond dst1 dst2 =>
        nub (var_list cond ++ dest_vars dst1 ++ dest_vars dst2),
        []
    | Switch _ cond def_dst branches =>
        nub (var_list cond ++ dest_vars def_dst ++ branches >>= dest_vars o SND),
        []
    | Watchpoint NONE _ rd => nub (rd_vars rd), []
    | Watchpoint (SOME (id, dst)) _ rd => nub (dest_vars dst ++ rd_vars rd), []
    | WPBranch id dst1 dst2 => nub (dest_vars dst1 ++ dest_vars dst2), []
    | SwapStack vs id _ nsc rd =>
        nub (var_list id ++ new_stack_clause_vars nsc ++ rd_vars rd),
        nub vs
    | CommInst vs ci rd => nub (comminst_vars ci ++ rd_vars rd), nub vs
    | ExcClause inst rd =>
        nub (inst_input_vars inst ++ rd_vars rd),
        inst_output_vars inst
`

val terminst_input_vars_def = Define`terminst_input_vars i = FST (terminst_vars i)`

val terminst_output_vars_def = Define`terminst_output_vars i = SND (terminst_vars i)`

val terminst_all_vars_def = Define`
  terminst_all_vars i = let (a, b) = terminst_vars i in nub (a ++ b)
`

(* Given a terminating instruction, returns an association list from SSA
   variables to types. The list only contains new SSA variables defined by the
   instruction.
*)
val terminst_types_def = Define`
    terminst_types (Call vs cd _) = ZIP (vs, cd.signature.return_types)
  ∧ terminst_types (SwapStack vs _ (RetWith tys) _ _) = ZIP (vs, tys)
  ∧ terminst_types (CommInst vs ci _) = ZIP (vs, comminst_types ci)
  ∧ terminst_types (ExcClause i _) = inst_types i
  ∧ terminst_types _ = []
`

(* An instruction is well-formed (OK) if
   - All output variables are distinct
   - For Assign instructions, the number of output variables matches the number
     of return values
   - For NewHybrid/AllocaHybrid, the types are valid
*)
val inst_ok_def = Define`
    inst_ok (Assign vs e) =
      (LENGTH vs = LENGTH (expr_types e) ∧ ALL_DISTINCT vs)
  ∧ inst_ok (CmpXchg v1 v2 _ _ _ _ _ _ _ _) = (v1 ≠ v2)
  ∧ inst_ok (New _ (Hybrid _ _)) = F
  ∧ inst_ok (New _ _) = T
  ∧ inst_ok (Alloca _ (Hybrid _ _)) = F
  ∧ inst_ok (Alloca _ _) = T
  ∧ inst_ok (NewHybrid _ (Hybrid _ _) (Int _) _) = T
  ∧ inst_ok (NewHybrid _ _ _ _) = F
  ∧ inst_ok (AllocaHybrid _ (Hybrid _ _) (Int _) _) = T
  ∧ inst_ok (AllocaHybrid _ _ _ _) = F
  ∧ inst_ok _ = T
`

val terminst_ok_def = Define`
    terminst_ok (Call vs cd _) = (
      LENGTH cd.args = LENGTH cd.signature.arg_types
    ∧ LENGTH vs = LENGTH cd.signature.return_types
    ∧ ALL_DISTINCT vs)
  ∧ terminst_ok (TailCall cd) = (
      LENGTH cd.args = LENGTH cd.signature.arg_types)
  ∧ terminst_ok (SwapStack vs _ KillOld _ _) = ALL_DISTINCT vs
  ∧ terminst_ok (SwapStack vs _ (RetWith tys) _ _) = (
      ALL_DISTINCT vs
    ∧ LENGTH vs = LENGTH tys)
  ∧ terminst_ok (CommInst vs ci _) = (
      ALL_DISTINCT vs
    ∧ LENGTH vs = LENGTH (comminst_types ci))
  ∧ terminst_ok (ExcClause i _) = inst_ok i
  ∧ terminst_ok _ = T
`

(* A basic block (see the spec)
   https://github.com/microvm/microvm-spec/blob/master/uvm-ir.rest#function-body
*)
val _ = Datatype`
  bblock = <|
    args: (ssavar # uvm_type) list;
    exc: ssavar option;
    body: instruction list;
    exit: terminst;
    keepalives: ssavar list
  |>
`

val block_arg_types_def = Define`
  block_arg_types (block : bblock) : (ssavar # uvm_type) list =
    block.args ++
    option_CASE block.exc [] (λe. [(e, Ref (Type Void))])
`

(* A basic block is well-formed (OK) if
   - All instructions are OK
   - Every SSA variable is assigned only once
   - Every SSA variable is assigned before it is read
*)
val block_ok_def = Define`
  block_ok (block : bblock) ⇔
    let args = MAP FST (block_arg_types block) in
    EVERY inst_ok block.body
  ∧ terminst_ok block.exit
  ∧ ALL_DISTINCT (args ++ block.body >>= inst_output_vars ++
                  terminst_output_vars block.exit)
  ∧ (∀i. i ≤ LENGTH block.body
       ⇒ EVERY (λv. MEM v (args ++ TAKE i block.body >>= inst_output_vars))
               (inst_input_vars (EL i block.body)))
  ∧ EVERY (λv. MEM v (args ++ block.body >>= inst_output_vars))
          (terminst_input_vars block.exit)
`

val block_vars_def = Define`
  block_vars (block : bblock) : ssavar list =
    nub (MAP FST (block_arg_types block) ++
         block.body >>= inst_output_vars ++
         terminst_all_vars block.exit ++
         block.keepalives)
`

val block_types_def = Define`
  block_types (blk : bblock) : (ssavar # uvm_type) list =
    block_arg_types blk ++ blk.body >>= inst_types ++ terminst_types blk.exit
`

val inst_types_output_vars = store_thm("inst_types_output_vars",
  ``∀i x. inst_ok i
        ⇒ (MEM x (MAP FST (inst_types i)) ⇔ MEM x (inst_output_vars i))``,
  Cases >>
  rw[inst_ok_def, inst_types_def, inst_output_vars_def, inst_vars_def, MAP_ZIP])

val inst_types_output_vars_bind = store_thm("inst_types_output_vars_bind",
  ``∀is x. EVERY inst_ok is
         ⇒ (MEM x (MAP FST (is >>= inst_types)) ⇔ 
            MEM x (is >>= inst_output_vars))``,
  Induct >> rw[] >> Cases_on `MEM x (is >>= inst_output_vars)` >>
  simp[inst_types_output_vars])

val inst_ok_inst_types_distinct = store_thm("inst_ok_inst_types_distinct",
  ``∀i. inst_ok i ⇒ ALL_DISTINCT (MAP FST (inst_types i))``,
  Cases >> rw[inst_ok_def, inst_types_def, MAP_ZIP])

val block_ok_block_types_distinct = store_thm("block_ok_block_types_distinct",
  ``∀b. block_ok b ⇒ ALL_DISTINCT (MAP FST (block_types b))``,
  rw[block_ok_def, block_arg_types_def, block_types_def] >>
  rw[ALL_DISTINCT_APPEND] >> BasicProvers.every_case_tac >>
  fs[ALL_DISTINCT_APPEND, inst_types_output_vars_bind]
  >- prove_tac[] >>
  TRY (qmatch_assum_rename_tac `ALL_DISTINCT (body >>= inst_output_vars)` >>
       qpat_x_assum `ALL_DISTINCT (_ _ inst_output_vars)` mp_tac >>
       qpat_x_assum `EVERY inst_ok _` mp_tac >> POP_ASSUM_LIST (K ALL_TAC) >>
       Induct_on `body` >- rw[] >> fs[ALL_DISTINCT_APPEND] >> Cases_on `h` >>
       fs[inst_ok_def, inst_types_def, inst_output_vars_def, inst_vars_def] >>
       rw[] >> fs[] >> rfs[MAP_ZIP, inst_types_output_vars_bind]) >>
  Cases_on `b.exit` >>
  fs[terminst_types_def, terminst_output_vars_def, terminst_vars_def,
     terminst_ok_def, MAP_ZIP, inst_ok_inst_types_distinct,
     inst_types_output_vars] >>
  TRY (Cases_on `c` >> fs[terminst_types_def, terminst_ok_def, MAP_ZIP]) >>
  prove_tac[inst_types_output_vars_bind])

val _ = Datatype`
  function = <|
    signature: funcsig;
    blocks: (block_label, bblock) alist
  |>
`

val _ = Datatype`
  environment = <|
    globals: (string, uvm_type) alist;
    constants: const_name |-> ssa_or_const;
    types: type_name |-> uvm_type;
    funcsigs: sig_name |-> funcsig;
    func_versions: (func_name, func_version option) alist;
    functions: (func_version, function) alist
  |>
`

val empty_env_def = Define`
  empty_env : environment = <|
    globals := [];
    constants := FEMPTY;
    types := FEMPTY;
    funcsigs := FEMPTY;
    func_versions := [];
    functions := []
  |>
`

(* A bundle is a function that extends an environment. It may return an error,
   for example, if the bundle refers to a name that is not defined in the input
   environment, or if the input environment causes a type mismatch.
*)
val _ = type_abbrev("bundle", ``:environment -> environment + type_error``)

(* Utility function for BundleParser *)
val generate_function_def = Define`
  generate_function (env : environment)
                    (name : string)
                    (func : function)
                    : environment =
    let fnv = FuncVersion (Func name) 0 in env with <|
      func_versions updated_by CONS (Func name, SOME fnv);
      functions updated_by CONS (fnv, func)
    |>
`

val _ = export_theory()

