open HolKernel Parse boolLib bossLib

open uvmMemoryTheory
open uvmIRTheory
open uvmValuesTheory
open uvmErrorsTheory
open uvmInstructionSemanticsTheory
open monadsTheory
open monadsLib

open arithmeticTheory
open ASCIInumbersTheory
open combinTheory
open finite_mapTheory
open patriciaTheory
open listTheory
open indexedListsTheory
open optionTheory
open pairTheory
open pred_setTheory
open relationTheory
open sortingTheory
open sptreeTheory
open lcsymtacs
open extraTactics
open syntaxSugarTheory

val _ = new_theory "uvmThreadsStacks"
val _ = monadsyntax.add_monadsyntax()

val _ = Datatype`
  thread = <|
    sid: stack_id;
    sb: buffer;
    ib: buffer
  |>
`

val _ = Datatype`
  frame_state =
    (* READY = waiting to be resumed
       Arguments: parameter types, normal/exceptional jumps *)
  | READY (uvm_type list) jumps

    (* ACTIVE = currently running
       Arguments: upcoming opns, registers *)
  | ACTIVE (opn list) mem_region

    (* DEAD = top-level function returned, or uvm.thread_exit
       Argument: return values of top-level function, if any *)
  | DEAD (value resume_values option) 
`

val _ = Datatype`
  frame = <|
    id: frame_id;
    fn: func_name;
    state: frame_state
  |>
`

val _ = Datatype`
  stack = <|
    frames: frame list;
    next_frame: frame_id
  |>
`

val frame_ok_def = Define`
  frame_ok (f : frame) ⇔
    ∃opns regs. f.state = ACTIVE opns regs
    ⇒ opns_ok opns ∧ mem_region_ok regs
`

val stack_ok_def = Define`
  stack_ok (s : stack) ⇔
    ¬NULL s.frames ∧
    EVERY (λf. frame_ok f ∧ (f.id ≺ s.next_frame)) s.frames ∧
    EVERY (λf. ¬∃ops regs. f.state = ACTIVE ops regs) (TL s.frames)
`

val thread_ok_def = Define`
  thread_ok (th : thread) ⇔ address_space_ok th.sb ∧ address_space_ok th.ib
`

(* A frame f₁ follows a previous frame f₀ iff f₁ is a valid future
   state of f₀. There are only two conditions on this: the frame's ID/function
   remains the same, and a dead frame cannot be modified or become alive again.

   This relation is a partial ordering on frames.
*)
val frame_follows_def = Define`
  frame_follows (f0 : frame) (f1 : frame) ⇔
    frame_ok f0
  ⇒ frame_ok f1
  ∧ (f0.id = f1.id)
  ∧ (f0.fn = f1.fn)
  ∧ (∀vs. f0.state = DEAD vs ⇒ f0 = f1)
`

val frame_follows_refl = store_thm("frame_follows_refl",
  ``∀s. frame_follows s s``,
  rw[frame_follows_def])

val frame_follows_trans = store_thm("frame_follows_trans",
  ``∀a b c. frame_follows a b ∧ frame_follows b c ⇒ frame_follows a c``,
  metis_tac[frame_follows_def])

(* A stack s₁ follows a previous stack s₀ iff s₁ is a valid future
   state of s₀.

   This relation is a partial ordering on stacks.
*)
val stack_follows_def = Define`
  stack_follows (s0 : stack) (s1 : stack) ⇔
    stack_ok s0
  ⇒ stack_ok s1
  ∧ s0.next_frame ≼ s1.next_frame
  ∧ EVERY (λn. n < LENGTH s0.frames
             ∧ frame_follows (EL n (REVERSE s0.frames)) (EL n (REVERSE s1.frames))
             ∨ s0.next_frame ≼ (EL n (REVERSE s1.frames)).id)
          (GENLIST I (LENGTH s1.frames))
`

val stack_follows_refl = store_thm("stack_follows_refl",
  ``∀s. stack_follows s s``,
  rw[stack_follows_def, EVERY_MEM, MEM_EL] >>
  rw[EL_GENLIST, frame_follows_refl])

val stack_follows_trans = store_thm("stack_follows_trans",
  ``∀a b c. stack_follows a b ∧ stack_follows b c ⇒ stack_follows a c``,
  rw[stack_follows_def, EVERY_EL] >| [
    prove_tac[frame_id_lt_trans],

    res_tac >> res_tac >>
    fs[EL_REVERSE] >> rfs[stack_ok_def, EVERY_EL, EL_REVERSE] >> 
    `∀m n. m < n ⇒ PRE (n - m) < n` by simp[] >>
    metis_tac[frame_follows_def, frame_id_lt_trans, EL_REVERSE]
  ])

val propagates_def = Define`
  propagates P f ⇔ ∀a b. P a ⇒ f a = OK b ⇒ P b
`

val frame_follows_ok = store_thm("frame_follows_ok",
  ``∀f. (∀t0 t1. f t0 = OK t1 ⇒ frame_follows t0 t1)
      ⇒ propagates frame_ok f``,
  rw[frame_follows_def, propagates_def] >> prove_tac[])

val imp_frame_follows_ok = store_thm("imp_frame_follows_ok",
  ``∀f t0 t1.  frame_ok t0 ∧ frame_follows t0 t1 ⇒ frame_ok t1``,
  rw[frame_follows_def])

val stack_follows_ok = store_thm("stack_follows_ok",
  ``∀f. (∀t0 t1. f t0 = OK t1 ⇒ stack_follows t0 t1)
      ⇒ propagates stack_ok f``,
  rw[stack_follows_def, propagates_def] >> prove_tac[])

val imp_stack_follows_ok = store_thm("imp_stack_follows_ok",
  ``∀f t0 t1.  stack_ok t0 ∧ stack_follows t0 t1 ⇒ stack_ok t1``,
  rw[stack_follows_def])

val imp_propagates = store_thm("imp_propagates",
  ``∀P f a b. propagates P f ∧ P a ⇒ f a = OK b ⇒ P b``,
  metis_tac[propagates_def])

val imp_propagates_fst = store_thm("imp_propagates_fst",
  ``∀P f a b b'. propagates P (liftM FST o f) ∧ P a ⇒ f a = OK (b, b') ⇒ P b``,
  rw[propagates_def] >> metis_tac[FST])

val imp_propagates_snd = store_thm("imp_propagates_snd",
  ``∀P f a b b'. propagates P (liftM SND o f) ∧ P a ⇒ f a = OK (b', b) ⇒ P b``,
  rw[propagates_def] >> metis_tac[SND])

val propagates_liftM = store_thm("propagates_liftM",
  ``∀P f g. propagates P (liftM g o f) ⇔ ∀a b. P a ∧ f a = OK b ⇒ P (g b)``,
  rw[propagates_def] >> metis_tac[])

val preserves_def = Define`
  preserves a f ⇔ ∀v. propagates ($= v o a) f
`

val imp_preserves = store_thm("imp_preserves",
  ``∀a f x y. preserves a f ⇒ f x = OK y ⇒ a x = a y``,
  rw[preserves_def, propagates_def])

val imp_preserves_snd = store_thm("imp_preserves_snd",
  ``∀a f x y y'. preserves a (liftM SND o f) ⇒ f x = OK (y', y) ⇒ a x = a y``,
  rw[propagates_def, preserves_def] >> metis_tac[SND])

val foldl_fail = store_thm("foldl_fail",
  ``∀f e. FOLDL (λs r. s >>= f r) (Fail e) rs = Fail e``,
  Induct_on `rs` >> fs[] >> Cases_on `h` >> fs[])

val foldr_fail = store_thm("foldr_fail",
  ``∀rs f e. FOLDR (λr s. s >>= f r) (Fail e) rs = Fail e``,
  Induct >> fs[] >> Cases_on `h` >> fs[])

val foldl_follows = store_thm("foldl_follows",
  ``∀f t0 t1.
      (∀r t0 t1. (f r t0 = OK t1) ⇒ stack_follows t0 t1)
    ∧ (∃rs. FOLDL (λs r. s >>= f r) (OK t0) rs = OK t1)
    ⇒ stack_follows t0 t1``,
  rw[] >> pop_assum mp_tac >> map_every qid_spec_tac [`t0`, `t1`] >>
  Induct_on `rs` >> fs[FOLDL, stack_follows_refl] >> rw[] >>
  Cases_on `f h t0` >>
  metis_tac[stack_follows_trans, foldl_fail])

val foldl_propagates = store_thm("foldl_propagates",
  ``(∀r. propagates P (f r))
    ⇒ propagates P (λa. FOLDL (λs r. s >>= f r) (OK a) rs)``,
  strip_tac >> Induct_on `rs` >> rw[]
  >- (rw[propagates_def] >> simp[FOLDL])
  >- (rw[propagates_def] >> Cases_on `f h a` >> fs[propagates_def] >>
      metis_tac[foldl_fail]))

val foldr_propagates = store_thm("foldr_propagates",
  ``∀rs f P.
      (∀r. propagates P (f r))
    ⇒ propagates P (λa. FOLDR (λr s. s >>= f r) (OK a) rs)``,
  Induct >> rw[propagates_def] >> fs[propagates_def] >> ntac 2 res_tac)

val imp_foldl_propagates = store_thm("imp_foldl_propagates",
  ``∀P. (∀r. propagates P (f r)) ∧ P a
      ∧ FOLDL (λs r. s >>= f r) (OK a) rs = OK b ⇒ P b``,
  rw[] >> pop_assum (mp_tac o CONV_RULE (UNBETA_CONV ``a`` |> LHS_CONV)) >>
  match_mp_tac imp_propagates >> simp[foldl_propagates])

val foldl_preserves = store_thm("foldl_preserves",
  ``∀f a rs.
      (∀r. preserves a (f r))
    ⇒ preserves a (λx. FOLDL (λs r. s >>= f r) (OK x) rs)``,
  rw[preserves_def, foldl_propagates])

val foldr_preserves = store_thm("foldr_preserves",
  ``∀f a rs.
      (∀r. preserves a (f r))
    ⇒ preserves a (λx. FOLDR (λr s. s >>= f r) (OK x) rs)``,
  rw[preserves_def, foldr_propagates])

val imp_foldl_preserves = store_thm("imp_foldl_preserves",
  ``∀a. (∀r. preserves a (f r)) ∧ FOLDL (λs r. s >>= f r) (OK x) rs = OK y
      ⇒ a x = a y``,
  rw[] >> pop_assum (mp_tac o CONV_RULE (UNBETA_CONV ``x`` |> LHS_CONV)) >>
  match_mp_tac imp_preserves >> simp[foldl_preserves])

val set_registers_def = Define`
  set_registers (rs : (num # value) list) (s : stack) : stack or_error =
  let f = HD s.frames;
      set (r, v) rs = mem_region_set_value r v rs;
      no_undef e = case e of
                   | NotImplemented m => NotImplemented m
                   | Impossible m => Impossible m
                   | UndefBehavior _ => Impossible "error writing register"
  in (assert (¬NULL s.frames) or_fail Impossible "stack underflow")
  *> case f.state of
     | ACTIVE opns regs =>
        (λr. s with frames := (f with state := ACTIVE opns r)::TL s.frames)
        <$> lift_right no_undef (FOLDR (λr s. s >>= set r) (OK regs) rs)
     | _ => Fail (Impossible "set_registers on inactive frame")
`

val set_registers_fail = store_thm("set_registers_fail",
  ``∀rs sk1 sk2 opns1 opns2 regs1 regs2 e.
      NULL sk1.frames = NULL sk2.frames
    ∧ (HD sk1.frames).state = ACTIVE opns1 regs1
    ∧ (HD sk2.frames).state = ACTIVE opns2 regs2
    ∧ (∀r. lookup r regs1 = lookup r regs2)
    ∧ set_registers rs sk1 = Fail e
    ⇒ set_registers rs sk2 = Fail e``,
  rw[set_registers_def, lift_left_def, lift_right_def, o_DEF, bind_right_def] >>
  Cases_on `NULL sk1.frames` >> fs[] >> rfs[] >>
  Induct_on `rs` >> rw[] >>
  map_every Cases_on
    (let val f = ``λ(r,v) rs. mem_region_set_value r v rs`` in [
      `FOLDR (λr s. s >>= ^f r) (OK regs1) rs >>= ^f h`,
      `FOLDR (λr s. s >>= ^f r) (OK regs2) rs >>= ^f h`,
      `FOLDR (λr s. s >>= ^f r) (OK regs1) rs`,
      `FOLDR (λr s. s >>= ^f r) (OK regs2) rs`
    ] end) >> rfs[UNCURRY] >> fs[] >>
  imp_res_tac mem_region_set_value_foldr >> fs[UNCURRY] >> res_tac >>
  REPEAT (qpat_x_assum `mem_region_set_value _ _ _ = _`
    (assume_tac o ONCE_REWRITE_RULE[mem_region_set_value_def])) >>
  fs[UNCURRY] >> rw[] >> rfs[UNCURRY] >| [
    `y = y'` suffices_by simp[] >>
    REPEAT (qpat_x_assum `_ >>= _ = Fail _`
      (assume_tac o REWRITE_RULE[bind_left_case])) >> multi_case_tac,

    `∀r. lookup r regs2 = lookup r regs1` by simp[] >>
    imp_res_tac mem_region_set_value_foldr_fail >> fs[UNCURRY] >> res_tac >>
    fs[]
  ])

val set_registers_lookup = store_thm("set_registers_lookup",
  ``∀rs sk1 sk2 sk1' sk2' regs1 regs2 opns1 opns2.
      (HD sk1.frames).state = ACTIVE opns1 regs1
    ∧ (HD sk2.frames).state = ACTIVE opns2 regs2
    ∧ (∀r. lookup r regs1 = lookup r regs2)
    ∧ set_registers rs sk1 = OK sk1'
    ∧ set_registers rs sk2 = OK sk2'
    ⇒ ∃regs1' regs2'.
        (HD sk1'.frames).state = ACTIVE opns1 regs1'
      ∧ (HD sk2'.frames).state = ACTIVE opns2 regs2'
      ∧ ∀r. lookup r regs1' = lookup r regs2'``,
  rw[set_registers_def, lift_left_def, lift_right_def, o_DEF, bind_right_def] >>
  rfs[] >> multi_case_tac >> metis_tac[mem_region_set_value_foldr])

(*
val set_register_follows = store_thm("set_register_follows",
  ``∀r s0 s1. set_register r s0 = OK s1 ⇒ stack_follows s0 s1``,
  Cases >> rw[set_register_def, stack_follows_def, stack_ok_def] >>
  fs[EVERY_EL] >> rw[] >> multi_case_tac >>
  Cases_on `s0.frames` >> fs[LENGTH_CONS] >>
  Cases_on `n` >> res_tac >> fs[] >> FIRST [
    `frame_ok (EL 0 (h::t))` by simp[] >> fs[frame_ok_def] >>
      prove_tac[mem_region_set_value_ok],
    Cases_on `REVERSE t` >> fs[frame_follows_def, frame_ok_def] >>
      multi_case_tac >> prove_tac[mem_region_set_value_ok],
    ONCE_REWRITE_TAC[prove(``REVERSE t ++ [h] = REVERSE (h::t)``, simp[])] >>
      simp[EL_REVERSE] >> rw[] >> Cases_on `PRE (LENGTH t - n')` >>
      fs[frame_follows_def] >| [
        `frame_ok (EL 0 (h::t))` by simp[] >> fs[frame_ok_def] >>
          multi_case_tac >> prove_tac[mem_region_set_value_ok],
        `SUC n < SUC (LENGTH t)` by simp[] >> res_tac >> fs[]
      ]
  ])
*)

val set_registers_preserves = store_thm("set_registers_preserves",
  ``∀rs. preserves stack_next_frame (set_registers rs)
       ∧ preserves (TL o stack_frames) (set_registers rs)
       ∧ preserves (NULL o stack_frames) (set_registers rs)
       ∧ preserves (frame_id o HD o stack_frames) (set_registers rs)
       ∧ preserves (frame_fn o HD o stack_frames) (set_registers rs)
       ∧ preserves (frame_state o HD o stack_frames) (set_registers rs)``,
  Cases >> rw[preserves_def, propagates_def, set_registers_def] >> simp[])

val enqueue_store_def = Define`
  enqueue_store ((a, v) : addr # value) (th : thread) : thread =
    th with sb updated_by buffer_enqueue a v
`

val enqueue_invalidation_def = Define`
  enqueue_invalidation ((a, v) : addr # value) (th : thread) : thread =
    th with ib updated_by buffer_enqueue a v
`

val enter_block_def = Define`
  enter_block (block : opn_block)
              (args  : value list)
              (exc   : value option)
              (fr    : frame)
              : frame or_error = do
    assert (case fr.state of DEAD _ => F | _ => T)
      or_fail Impossible "jump on dead frame";
    assert (LENGTH args ≤ LENGTH block.regs)
      or_fail Impossible "block arity mismatch";
    regs <- sptree$fromList <$> sequence (GENLIST (λn.
        let (tys, offs) = EL n block.regs in do
          v <- if n < LENGTH args
               then return (EL n args)
               else if SOME n = block.exc_param
               then return (option_CASE exc (RefV Void NONE) I)
               else zero_value (HD tys);
          return (v, tys, offs)
        od) (LENGTH block.regs));
    return (fr with <| state := ACTIVE block.opns regs |>)
  od
`

val in_block_def = Define`
  in_block (block : opn_block) (fr : frame) ⇔
    ∃opns regs. fr.state = ACTIVE opns regs
              ∧ IS_SUFFIX block.opns opns
              ∧ MAP SND (toList regs) = block.regs
`

val at_start_of_block_def = Define`
  at_start_of_block (block : opn_block) (fr : frame) ⇔
    ∃regs. fr.state = ACTIVE block.opns regs
         ∧ MAP SND (toList regs) = block.regs
`

val stack_in_block_def = Define`
  stack_in_block (block : opn_block) (sk : stack) ⇔
    (¬NULL sk.frames ∧ in_block block (HD sk.frames))
`

val jump_def = Define`
  jump (prg  : program)
       (lbl  : num)
       (args : value list)
       (exc  : value option)
       (sk   : stack)
       : stack or_error =
    let fr = HD sk.frames in do
      assert (¬NULL sk.frames) or_fail Impossible "stack underflow";
      blocks <- FLOOKUP prg fr.fn or_fail UndefBehavior (NoSuchFunction fr.fn);
      block <- lookup lbl blocks
        or_fail Impossible ("undefined label: " ++ num_to_dec_string lbl);
      fr' <- enter_block block args exc fr;
      return (sk with frames := fr'::TL sk.frames)
    od
`

val resume_stack_def = Define`
  resume_stack (prg : program) (rvs : value resume_values) (sk : stack)
               : stack or_error =
    (assert (¬NULL sk.frames) or_fail Impossible "stack underflow") *>
    let f = HD sk.frames in
    case f.state of
    | READY tys next => do
        (res_args, exc, label, params) <-
          case rvs of
          | NOR vs => return (vs, NONE, next.nor_jump)
          | EXC vs e => do
              j <- next.exc_jump or_fail UndefBehavior (UncaughtException (OK e));
              return (vs, SOME e, j)
            od;
        args <- mapM (λparam.
            case param of
            | INR c => return c
            | INL n => do
                assert (n < LENGTH res_args)
                  or_fail Impossible "missing resume argument";
                return (EL n res_args)
              od)
          params;
        jump prg label args exc sk
      od
    | _ => Fail (UndefBehavior (FrameNotReady f.id))
`

val push_stack_def = Define`
  push_stack (prg : program)
             (fn : func_name)
             (sig : funcsig)
             (args : value list)
             (next : jumps option)
             (sk : stack)
             : stack or_error =
    let new_frame = <|
      id := sk.next_frame; fn := fn;
      state := READY sig.arg_types <|
        nor_jump := (0, GENLIST INL (LENGTH args));
        exc_jump := NONE
      |>
    |> in
    resume_stack prg (NOR args) (
      case next of
      (* SOME = normal call *)
      | SOME js => sk with <|
          frames updated_by (λf.
            new_frame::(HD f with state := READY sig.return_types js)::TL f);
          next_frame updated_by suc
        |>
      (* NONE = tail call *)
      | NONE => sk with <|
          frames updated_by (λf. new_frame::TL f);
          next_frame updated_by suc
        |>)
`

val pop_stack_def = Define`
  pop_stack (prg : program) (rvs : value resume_values) (sk : stack)
            : (stack # frame) or_error = do
    assert (¬NULL sk.frames) or_fail Impossible "stack underflow";
    assert (case (HD sk.frames).state of DEAD _ => F | _ => T)
      or_fail Impossible "pop_stack on dead frame";
    if NULL (TL sk.frames)
    then (let fr = HD sk.frames with state := DEAD (SOME rvs)
          in return (sk with frames := [fr], fr))
    else do
      sk' <- resume_stack prg rvs (sk with frames updated_by TL);
      return (sk', HD sk.frames)
    od
  od
`

(* Creates a new thread with the given stack. *)
val new_thread_def = Define`
  new_thread (sid : stack_id) : thread =
    <| sid := sid; sb := new_address_space; ib := new_address_space |>
`

(******************************************************************************)

val converse = ONCE_REWRITE_RULE [EQ_SYM_EQ]

val toList_fromList = store_thm("toList_fromList",
  ``∀xs. toList (fromList xs) = xs``,
  cheat (* TODO: Solve this for real. *)
(*  
  rw[toList_def] >> Induct_on `fromList xs` >> rw[] >>
  pop_assum (assume_tac o converse) >| [
    simp[toListA_def] >> Induct_on `xs` >> fs[fromList_def, Once insert_def]
    metis_tac[CONS, FOLDL, fromList_def, UNCURRY, FST, SND, PAIR]
    SPOSE_NOT_THEN assume_tac >> imp_res_tac CONS >>
    pop_assum (assume_tac o converse) >> rw[]
    metis_tac[fromList_def]
    `xs = HD xs::TL xs` by metis_tac[CONS]
    simp[isEmpty_toList]
  ]

  listLib.SNOC_INDUCT_TAC >| [
    simp[fromList_def, toList_def, toListA_def],

    map_every (fn x => once_rewrite_tac [x])
      [fromList_def, toList_def, toListA_def, FOLDL_SNOC] >>
    rw[pairTheory.UNCURRY, converse fromList_def] >> fs[toList_def] >>
    Cases_on `fromList xs` >| [
      `xs = []` by prove_tac[toListA_def] >>
      rw[Once insert_def, Once toListA_def],

      `xs = [a]` by prove_tac[toListA_def] >>
      ntac 3 (rw[Once insert_def, Once toListA_def]),

      rw[Once insert_def]
    ]
      
      
      fs[toList_def, toListA_def, Once insert_def] >>
      pop_assum mp_tac >> pop_assum (assume_tac o converse) >>
      rw[toListA_def, Once insert_def]
      
      fs[toListA_def]
      qabbrev_tac `xs_tree = fromList xs` >>
      pop_assum mp_tac >> pop_assum mp_tac >>
      fs[fromList_def, toList_def]
    ]
    strip_tac >> pop_assum mp_tac >>
    rw[]
  ]
*))

val enter_block_in_block = store_thm("enter_block_in_block",
  ``∀b a e f f'. enter_block b a e f = OK f' ⇒ in_block b f'``,
  rw[enter_block_def, in_block_def] >> fs[toList_fromList] >>
  match_mp_tac LIST_EQ >> conj_asm1_tac >> rw[] >| [
    prove_tac[sequence_left_length, LENGTH_GENLIST],

    imp_res_tac el_sequence_left >> fs[EL_MAP, EL_GENLIST, UNCURRY] >>
    res_tac >> multi_case_tac >> prove_tac[SND]
  ])

val new_thread_ok = store_thm("new_thread_ok",
  ``∀sid. thread_ok (new_thread sid)``,
  rw[thread_ok_def, new_thread_def, new_address_space_def,
     address_space_ok_def, wf_def, toList_def, toListA_def])

val _ = export_theory()

