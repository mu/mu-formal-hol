structure syntaxSugarLib :> syntaxSugarLib =
struct
  open HolKernel boolLib bossLib
  open Parse
  open Term

  (* Quickly generate prefix operators *)
  fun prefix_op optr =
    add_rule {term_name   = optr,
              fixity      = Prefix 780,
              pp_elements = [TOK optr],
              paren_style = OnlyIfNecessary,
              block_style = (AroundEachPhrase, (PP.CONSISTENT, 0))}

  (* Superscript "modifier" letters *)
  exception NoSuperscript of string

  fun sup "a" = "ᵃ"
    | sup "b" = "ᵇ"
    | sup "c" = "ᶜ"
    | sup "d" = "ᵈ"
    | sup "e" = "ᵉ"
    | sup "f" = "ᶠ"
    | sup "g" = "ᵍ"
    | sup "i" = "ⁱ"
    | sup "k" = "ᵏ"
    | sup "l" = "ˡ"
    | sup "m" = "ᵐ"
    | sup "n" = "ⁿ"
    | sup "o" = "ᵒ"
    | sup "p" = "ᵖ"
    | sup "s" = "ˢ"
    | sup "t" = "ᵗ"
    | sup "u" = "ᵘ"
    | sup "v" = "ᵛ"
    | sup "x" = "ˣ"
    | sup "z" = "ᶻ"
    | sup letter = raise NoSuperscript letter

  (* Defines a newtype wrapper for a numeric ID, complete with Unicode operators
     for less/less-eq, construction, and destructuring.
  *)

  fun define_num_newtype(type_name : string, ctor_name : string, letter : string) : unit =
    let
      val _ = Datatype[QUOTE type_name, QUOTE " = ", QUOTE ctor_name, QUOTE " num"]
      val ty = mk_type(type_name, [])
      val ctor = mk_const(ctor_name, mk_type("num", []) --> ty)
      
      val suc_name = type_name ^ "_suc"
      val lt_name = type_name ^ "_lt"
      val dest_name = type_name ^ "_dest"
      val suc_def = Define(QUOTE suc_name::` x = case x of ^ctor n => ^ctor (SUC n)`)
      val lt_def = Define(QUOTE lt_name::` x y = case x of ^ctor m => case y of ^ctor n => m < n`)
      val dest_def = Define(QUOTE dest_name::` (^ctor n) = n`)
      val suc = mk_const(suc_name, ty --> ty)
      val lt = mk_const(lt_name, ty --> ty --> mk_type("bool", []))
      val dest = mk_const(dest_name, ty --> mk_type("num", []))
      val _ = Q.store_thm(type_name ^ "_lt_suc[simp]",
        `∀x. ^lt x (^suc x)`,
        Cases >> rw[suc_def, lt_def])
      val _ = Q.store_thm(type_name ^ "_lt_trans",
        `∀x y z. ^lt x y ∧ ^lt y z ⇒ ^lt x z`,
        Cases >> Cases >> Cases >> rw[lt_def])
      val _ = Q.store_thm(type_name ^ "_lt_equiv[simp]",
        `∀x y. ^lt x y ⇒ ∃m n. (x = ^ctor m) ∧ (y = ^ctor n) ∧ m < n`,
        Cases >> Cases >> rw[lt_def])
      val ctor_sym = sup letter ^ "#"
      val _ = TeX_notation{
        hol=ctor_sym,
        TeX=("\\ensuremath{{}^{" ^ letter ^ "}\\#}", 1)
      }
    in
      (* Define "precedes" (U+227A = ≺) and "predeces or equivalent" (U+227C = ≼) operators,
       * which are different Unicode chars from < and ≤. These new symbols are required for
       * overloaded comparisons because ≤ is not defined as an overload. *)
      overload_on("≺", lt);
      overload_on("≼", ``λl r. (^lt l r) ∨ (l = r)``);
      overload_on("suc", suc);
      overload_on("⍽", dest);
      prefix_op(ctor_sym);
      overload_on(ctor_sym, ctor)
    end
end
