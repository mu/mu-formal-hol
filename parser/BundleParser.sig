open HolKernel ParserHelpers

signature BundleParser =
sig
  val ty_env : hol_type

  (* env_context that inserts references to a free variable `env` *)
  val free_env_context : env_context

  (* Output: ``:bundle`` (that is, ``:environment -> environment or_error``) *)
  val uir_bundle : 'a frag list -> term
  val uir_bundle_s : string -> term

  (* Load a bundle from a file *)
  val load_uir_file : string -> term
end

