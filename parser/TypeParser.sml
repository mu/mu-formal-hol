structure TypeParser :> TypeParser =
struct
  open HolKernel Parse Term TypeBase UIRLexer
  open numSyntax
  open sumSyntax
  open uvmTypesTheory
  open ParserHelpers

  fun ty_ap (name : string) (xs : term list) =
    list_mk_lift(mk_thy_const{
      Thy="uvmTypes", Name=name,
      Ty=foldr (op -->) ty_type (map (#1 o dest_sum o type_of) xs)
    }, xs)

  fun parse_type (env : env_context) [SSA (Global, name)] =
        (#get_type env) name
    | parse_type (env : env_context) (Word tn::rest) =
        let
          fun no_params ty =
            case rest of [] => ty
            | _ => raise ParseError ("type " ^ tn ^ " does not take params")
          fun with_params func =
            case rest of [Type ts] => func ts
            | _ => raise ParseError ("expected params for type " ^ tn)
          fun elem t = parse_type env t
          fun size [Int n] = mk_return(lift_num ty_num (Arbint.toNat n))
            | size _ = raise ParseError ("expected " ^ tn ^ " size")
          val t_Type = mk_const("INL", ty_type --> mk_sum(ty_type, ty_num))
        in case tn of
          "void" => no_params (ty_ap "Void" [])
        | "float" => no_params (ty_ap "Float" [])
        | "double" => no_params (ty_ap "Double" [])
        | "threadref" => no_params (ty_ap "ThreadRef" [])
        | "stackref" => no_params (ty_ap "StackRef" [])
        | "framecursorref" =>
            raise ParseError "framecursorref type not yet implemented"
        | "tagref64" => no_params (ty_ap "TagRef64" [])
        | "int" => with_params (fn t => ty_ap "Int" [size t])
        | "ref" => with_params (fn t => ty_ap "Ref" [mk_lift(t_Type, elem t)])
        | "iref" => with_params (fn t => ty_ap "IRef"
            [mk_return(mk_const("REF", ty_reft)), mk_lift(t_Type, elem t)])
        | "uptr" => with_params (fn t => ty_ap "IRef"
            [mk_return(mk_const("PTR", ty_reft)), mk_lift(t_Type, elem t)])
        | "weakref" => with_params (fn t =>
            ty_ap "WeakRef" [mk_lift(t_Type, elem t)])
        | "array" => with_params (fn ts =>
            let val (t1, t2) = take_one_type ts in
              ty_ap "Array" [elem t1, size t2]
            end)
        | "vector" => with_params (fn ts =>
            let val (t1, t2) = take_one_type ts in
              ty_ap "Vector" [elem t1, size t2]
            end)
        | "struct" => raise ParseError "struct type not yet implemented"
        | "hybrid" => raise ParseError "hybrid type not yet implemented"
        | "funcref" => raise ParseError "funcref type not yet implemented"
        | "ufuncptr" => raise ParseError "ufuncptr type not yet implemented"
        | _ => raise ParseError (tn ^ " is not a type constructor")
        end
    | parse_type _ _ = raise ParseError "expected type, got something else"
  and take_one_type (tokens : uir_tokens) : uir_tokens * uir_tokens =
    case tokens of
      SSA (Global, name)::cs => ([SSA (Global, name)], cs)
    | Word w::Type ts::cs => ([Word w, Type ts], cs)
    | Word w::cs => ([Word w], cs)
    | _ => raise ParseError "expected type, got something else"

  fun parse_type_list (env : env_context) [] = []
    | parse_type_list (env : env_context) (tokens : uir_tokens) =
        let val (ty, rest) = take_one_type tokens
        in parse_type env ty :: parse_type_list env rest end

  fun parse_funcsig (env : env_context) [SSA (Global, name)] =
        (#get_funcsig env) name
    | parse_funcsig (env : env_context) [Parens lhs, Arrow, Parens rhs] =
        lift_record(ty_sig, [
          ("arg_types", mk_sequence(parse_type_list env lhs, ty_type)),
          ("return_types", mk_sequence(parse_type_list env rhs, ty_type))
        ])
    | parse_funcsig _ _ = raise ParseError "expected funcsig, got something else"

  fun uir_type env q = parse_type env (tokenize (unquote_all q))

  fun uir_funcsig env q = parse_funcsig env (tokenize (unquote_all q))
end

