structure ValueParser :> ValueParser =
struct
  open HolKernel TypeBase Parse Term UIRLexer 
  open boolLib
  open numSyntax
  open listSyntax
  open stringSyntax
  open sumSyntax
  open uvmValuesTheory
  open uvmMemoryTheory
  open ParserHelpers
  open TypeParser

  fun invalid_ir (m : string) = infer_comb("INR",
    [infer_comb("Impossible", [fromMLstring m], ty_err)],
    or_error (ty_list ty_val))

  fun parse_value [Int n] =
        let val t_size = mk_var("size", ty_num) in mk_pattern_fn[
          (ty_pat "Int" [t_size],
            mk_return(mk_list(
              [infer_comb("IntV", [t_size, mk_num (Arbint.toInt n)], ty_val)],
              ty_val))),
          (mk_var("ty", ty_type),
            invalid_ir "int constant has non-int type")]
        end
    | parse_value [Word "NULL"] =
        let val t_ty = mk_var("ty", ty_type)
            val t_Type = mk_const("INL", ty_type --> mk_sum(ty_type, ty_num))
        in mk_pattern_fn[
          (ty_pat "Ref" [mk_comb(t_Type, t_ty)],
            mk_return(mk_list(
              [infer_comb("RefV",
                [t_ty, mk_const("NONE", mk_type("option", [ty_addr]))],
                ty_val)],
              ty_val))),
          (t_ty,
            invalid_ir "NULL constant has non-ref type")]
        end
    | parse_value (OpenBrace::cs) =
        (* FIXME: Generated code doesn't typecheck *)
        let
          fun next [CloseBrace] = []
            | next cs' =
                let val (hd, rest) = take_one_value cs' in
                  parse_value hd::next rest
                end
          val xs_orig = next cs
          val xs = mk_list(xs_orig, ty_type --> or_error ty_val)
          val t_ty = mk_var("ty", ty_type)
          val t_n = mk_var("n", ty_num)
          val t_f = mk_var("f", ty_type --> or_error ty_val)
          fun seq_case (name : string) : term * term = (
            ty_pat name [t_ty, t_n],
            mk_cond(
              mk_eq(t_n, mk_num (length xs_orig)),
              mk_lift(
                mk_const("FLAT", ty_list (ty_list ty_val) --> ty_list ty_val),
                infer_comb("mapM_left",
                  [mk_abs(t_f, mk_comb(t_f, t_ty)), xs],
                  or_error (ty_list (ty_list ty_val)))),
              invalid_ir (name ^ " constant has wrong length")))
        in mk_pattern_fn[
          seq_case "Array",
          seq_case "Vector",
          (* TODO: struct, hybrid *)
          (t_ty,
            invalid_ir "composite constant has non-composite type")]
        end
    | parse_value _ = raise ParseError "expected constant, got something else"
  and take_one_value (tokens : uir_token list) : uir_token list * uir_token list =
    case tokens of
      Int n::cs => ([Int n], cs)
    | Word "NULL"::cs => ([Word "NULL"], cs)
    | OpenBrace::cs =>
        let fun next (CloseBrace::cs') = ([CloseBrace], cs')
              | next cs' =
                  let
                    val (hd, rest) = take_one_value cs'
                    val (tl, rest') = next rest
                  in (hd @ tl, rest') end
        in next cs end
    | _ => raise ParseError "expected constant, got something else"

  fun uir_value q = parse_value (tokenize (unquote_all q))
end

