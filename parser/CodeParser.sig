open HolKernel UIRLexer ParserHelpers

signature CodeParser =
sig
  val ty_ssa : hol_type
  val ty_blabel : hol_type
  val ty_or_const : hol_type

  val t_Var : term
  val t_Const : term

  val parse_inst : env_context -> uir_token list -> term
  val parse_terminst : env_context -> uir_token list -> term
  val parse_block : env_context -> uir_token list list -> string * term
  val parse_func : env_context -> term -> uir_token list list -> term

  (* Output: ``:ssavar instruction or_error`` *)
  val uir_inst : env_context -> 'a frag list -> term
  val uir_inst_s : env_context -> string -> term

  (* Output: ``:ssavar terminst or_error`` *)
  val uir_terminst : env_context -> 'a frag list -> term
  val uir_terminst_s : env_context -> string -> term

  (* Output: (block name, ``:ssavar bblock or_error``) *)
  val uir_block : env_context -> 'a frag list -> string * term
  val uir_block_s : env_context -> string -> string * term

  (* Second argument is a funcsig. Output: ``:function or_error`` *)
  val uir_func : env_context -> term -> 'a frag list -> term
  val uir_func_s : env_context -> term -> string -> term
end

