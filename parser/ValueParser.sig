open HolKernel UIRLexer

signature ValueParser =
sig
  (* Instead of a ``:value``, this produces a ``:uvm_type -> value or_error``
     (technically, a ``:uvm_type -> (α, β) value_or_error``). The function
     must be applied to a type to complete the parsing process, because the type
     of a value is necessary information (e.g., is `{ 1 2 3 }` an
     `array<int<8> 3>` or a `vector<int<16> 3>`?). The function returns an error
     result at the HOL level if the value does not match the type.
  *)
  val parse_value : uir_tokens -> term

  (* Output: ``:uvm_type -> (α, β) value_or_error`` *)
  val uir_value : 'a frag list -> term
end

