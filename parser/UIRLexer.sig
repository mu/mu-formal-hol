open HolKernel

signature UIRLexer =
sig
  datatype ssa_scope =
      Local
    | Global

  datatype uir_token =
      Word     of string
    | Decl     of string
    | SSA      of (ssa_scope * string)
    | Int      of Arbint.int
    | RetVal   of Arbnum.num
    | Parens   of (uir_token list)
    | Brackets of (uir_token list)
    | Type     of (uir_token list)
    | OpenBrace
    | CloseBrace
    | Eq
    | Colon
    | Arrow

  type uir_tokens = uir_token list

  exception IllegalChar of char
  exception Unclosed of char

  val tokenize : string -> uir_tokens
end

