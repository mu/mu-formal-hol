open HolKernel UIRLexer ParserHelpers

signature TypeParser =
sig
  val parse_type : env_context -> uir_tokens -> term
  val parse_type_list : env_context -> uir_tokens -> term list
  val parse_funcsig : env_context -> uir_tokens -> term
  val take_one_type : uir_tokens -> uir_tokens * uir_tokens

  (* Output: ``:uvm_type or_error`` *)
  val uir_type : env_context -> 'a frag list -> term

  (* Output: ``:funcsig or_error`` *)
  val uir_funcsig : env_context -> 'a frag list -> term
end

