open HolKernel

signature ParserHelpers =
sig
  exception ParseError of string
  type env_context = {
    get_type: string -> term,
    get_funcsig: string -> term,
    get_global: string -> term
  }

  val mock_context : env_context
  
  val ty_num : hol_type
  val ty_str : hol_type
  val ty_type : hol_type
  val ty_reft : hol_type
  val ty_sig : hol_type
  val ty_addr : hol_type
  val ty_iaddr : hol_type
  val ty_funcn : hol_type
  val ty_val : hol_type
  val ty_err : hol_type
  val ty_list : hol_type -> hol_type
  val or_error : hol_type -> hol_type
  
  val infer_comb : string * term list * hol_type -> term
  val mk_some : term -> term
  val mk_return : term -> term
  val mk_bind : term * term -> term
  val mk_sequence : term list * hol_type -> term
  val mk_lift : term * term -> term
  val mk_num : int -> term
  val mk_singleton : term -> term
  val list_mk_lift : term * term list -> term
  val lift_record : hol_type * (string * term) list -> term
  val ty_pat : string -> term list -> term

  val unquote : 'a frag -> string
  val unquote_all : 'a frag list -> string

  exception StartsWithNonHeader
  val split_by_header : ('a -> bool) -> 'a list -> 'a list list
end

