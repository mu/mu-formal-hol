structure UIRLexer :> UIRLexer =
struct
  open HolKernel

  datatype ssa_scope =
      Local
    | Global

  datatype uir_token =
      Word     of string
    | Decl     of string
    | SSA      of (ssa_scope * string)
    | Int      of Arbint.int
    | RetVal   of Arbnum.num
    | Parens   of (uir_token list)
    | Brackets of (uir_token list)
    | Type     of (uir_token list)
    | OpenBrace
    | CloseBrace
    | Eq
    | Colon
    | Arrow

  type uir_tokens = uir_token list

  exception IllegalChar of char
  exception Unclosed of char

  (* True if `c` is a legal Mu IR identifier character *)
  fun is_name_char (c : char) : bool =
    Char.isAlphaNum c orelse c = #"." orelse c = #"_" orelse c = #"-"

  (* Returns the longest sequence of characters at the beginning of `str` that
     satisfy `predicate`. `str` is updated to contain only the remaining
     characters after the extracted word.
  *)
  fun read_word (predicate : char -> bool) (str : string ref) : string =
    let
      val s = !str
      val last =
        case CharVector.findi (fn (_, c) => not (predicate c)) s of
          SOME (i, _) => i
        | NONE => CharVector.length s
    in
      str := String.extract(s, last, NONE);
      String.extract(s, 0, SOME last)
    end

  (* Generates a list of tokens from a UIR source string, stopping when end of
     input is reached or a closing/invalid character is encountered. The ref
     `str` is updated to contain only the remaining, unparsed portion of its
     original contants.
     
     After a successful top-level run of the lexer, `!str` should be "". If it
     is anything else, there was an invalid character in `str`.

     This function calls itself recursively to scan for tokens inside of parens,
     brackets, and angle brackets. A closing paren/bracket/angle bracket will
     cause the lexer to terminate early, allowing it to be used in this way.
  *)
  fun lexer (str : string ref) : uir_tokens =
    let
      fun next_ch() = String.sub(!str, 0) handle Subscript => #"\000"
      fun skip1() = (str := String.extract(!str, 1, NONE); str)
      fun emit (token : uir_token) : uir_token list = token::lexer str
    in
      case next_ch() of
        #"." => emit (Decl (read_word Char.isAlpha (skip1())))
      | #"@" => emit (SSA (Global, read_word is_name_char (skip1())))
      | #"%" => emit (SSA (Local, read_word is_name_char (skip1())))
      | #"(" =>
          let val contents = lexer(skip1()) in
            case next_ch() of
              #")" => (skip1(); emit (Parens contents))
            | #"\000" => raise Unclosed #"("
            | c => raise IllegalChar c
          end
      | #"[" =>
          let val contents = lexer(skip1()) in
            case next_ch() of
              #"]" => (skip1(); emit (Brackets contents))
            | #"\000" => raise Unclosed #"["
            | c => raise IllegalChar c
          end
      | #"<" =>
          let val contents = lexer(skip1()) in
            case next_ch() of
              #">" => (skip1(); emit (Type contents))
            | #"\000" => raise Unclosed #"<"
            | c => raise IllegalChar c
          end
      | #"{" => (skip1(); emit OpenBrace)
      | #"}" => (skip1(); emit CloseBrace)
      | #"=" => (skip1(); emit Eq)
      | #":" => (skip1(); emit Colon)
      | #"-" =>
          if String.isPrefix "->" (!str)
          then (skip1(); skip1(); emit Arrow)
          else
            let val digits = read_word Char.isDigit (skip1()) in
              emit (Int (Arbint.negate (Arbint.fromString digits)))
            end
      | #"+" =>
          let val digits = read_word Char.isDigit (skip1()) in
            emit (Int (Arbint.fromString digits))
          end
      | #"$" =>
          let val digits = read_word Char.isDigit (skip1()) in
            emit (RetVal (Arbnum.fromString digits))
          end
      | #"/" =>
          if String.isPrefix "//" (!str)
          then (str := ""; [])
          else raise IllegalChar #"/"
      | c =>
          if Char.isSpace c then
            lexer (skip1())
          else if Char.isAlpha c then
            emit (Word (read_word is_name_char str))
          else if String.isPrefix "0x" (!str) then
            let val digits = read_word Char.isHexDigit (skip1(); skip1()) in
              emit (Int (Arbint.fromNat (Arbnum.fromHexString digits)))
            end
          else if Char.isDigit c then
            let val digits = read_word Char.isDigit str in
              emit (Int (Arbint.fromString digits))
            end
          else []
    end

  (* Converts a `string` into a `uir_token list`, throwing an exception if the
     input is not valid UIR.
  *)
  fun tokenize (str : string) : uir_tokens =
    let
      val sref = ref str
      val tokens = lexer sref
    in
      case !sref of "" => tokens | s => raise IllegalChar (String.sub(s, 0))
    end

end

