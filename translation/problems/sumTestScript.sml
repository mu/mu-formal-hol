open ml_translatorTheory ml_translatorLib;
open HolKernel bossLib Parse boolLib;
open ml_translatorTheory ml_translatorLib;

val _ = new_theory "sumTest";

val _ = Datatype`
  aType = At
`

val _ = Datatype`
  bType = Bt
`

val _ = Datatype`
  sumType = C (aType + bType)
          | D num
`

val notNum_def = Define`
  notNum (C _) = T ∧
  notNum _     = F
`

(*
val _ = translate notNum_def
*)

val _ = Datatype`
  sumxType = Cx (sumxType + num)
           | Dx num
`

val notNumx_def = Define`
  notNumx (Cx _) = T ∧
  notNumx _      = F
`

val _ = translate notNumx_def

val _ = Datatype`
  sumyType = Cy num (sumyType list)
           | Dy num
`

val notNumy_def = Define`
  (notNumy (Dy _) = T) ∧
  (notNumy _     = F)
`

(*
val _ = translate notNumy_def
*)

val _ = export_theory ();
