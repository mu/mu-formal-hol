(* 
   The script file which will nicely package the evaluation functions
   into a translated term ready to be printed to a file
*)

open HolKernel bossLib Parse boolLib;
open ml_progTheory; (* for the ML_code_def *)
open ml_progLib; (* for the get_thm function *)
open ml_translatorTheory ml_translatorLib; (* Translator *)
open uvmTypesTheory sumMonadTheory;
open combinTheory pred_setTheory;

val _ = new_theory "ml_hol_exprProg";

(* Translatation Step *)

(* sumMonadTheory Translation *)
val _ = translate bind_right_def;

val _ = translate o_DEF; (* combinTheory *)
val _ = translate lift_right_def;

val _ = translate merge_right_def;

val _ = translate merge_left_def;

val INSERT_thm = Q.store_thm (
  "INSERT_thm",
  `x INSERT s = λ y. (x = y) ∨ s(y)`,
  simp [EXTENSION, SPECIFICATION] >> metis_tac []
)

(*
val _ = translate EMPTY_DEF;
val _ = translate IN_DEF;
val _ = translate GSPECIFICATION; (* NOPE *)
val _ = translate INSERT_thm; (* pred_setTheory *)
val _ = translate left_set_def;

val _ = patternMatchesSyntax.ENABLE_PMATCH_CASES();
(* uvmTypesTheory Translation *)
val _ = translate fp_type_def;
*)

(* --- *)


(* Gets prog current state, not sure what it does completely *)
fun get_curr_prog_state () = let
  val k = ref init_state
  val _ = ml_prog_update (fn s => (k := s; s))
  in !k end

fun define_abbrev_conv name tm = let
  val def = define_abbrev true name tm
  in GSYM def |> SPEC_ALL end

val expr_prog_thm =
  get_thm (get_curr_prog_state ())
  |> REWRITE_RULE [ML_code_def]
  |> CONV_RULE ((RATOR_CONV o RATOR_CONV o RAND_CONV)
                (EVAL THENC define_abbrev_conv "expr_code"))
  |> CONV_RULE ((RATOR_CONV o RAND_CONV)
                (EVAL THENC define_abbrev_conv "expr_init_env"))
  |> CONV_RULE ((RAND_CONV)
                (EVAL THENC define_abbrev_conv "expr_init_state"))
  |> curry save_thm "expr_prog_thm"

val _ = export_theory ();
