open HolKernel Parse boolLib bossLib

open datatypeTranslateTheory

val _ = new_theory "datatypeTranslateTestsTheory"

val dict = ref (Binarymap.mkDict KernelSig.name_compare :
            (KernelSig.kernelname, (hol_type * term)) Binarymap.dict)

fun insert_dict dict (kernelname, info_tuple) =
  let
    val tmp = Binarymap.insert (!dict, kernelname, info_tuple)
  in dict := tmp end;

val _ = insert_dict dict ((mk_kn "pred_set" "INSERT"),
          (``:α -> (α -> bool) -> (α -> bool)``,
           ``CONS : α -> α list -> α list``))

val _ = insert_dict dict ((mk_kn "pred_set" "EMPTY"),
          (``:α -> bool``,
           ``[] : α list``))

fun assert_translate_interm dict init_tm assert_tm =
  let
    val fin_tm = translate_interm (! dict) init_tm
  in
    prove (mk_eq (fin_tm, assert_tm), simp[])
  end

val _ = assert_translate_interm dict
          ``{}:'a -> bool`` ``[]:'a list``
val _ = assert_translate_interm dict
          ``{x:num}`` ``[x:num]``
val _ = assert_translate_interm dict
          ``{ODD}`` ``[ODD]:(num -> bool) list``
val _ = assert_translate_interm dict
          ``{{}:'a -> bool}`` ``[[]:'a list]``
val _ = assert_translate_interm dict
          ``{{}; x:num -> bool}`` ``[[]; x:num list]``
val _ = assert_translate_interm dict
          ``{}:'a -> bool=y`` ``[]:'a list=y``
val _ = assert_translate_interm dict
          ``[{3}]`` ``[[3]]``
val _ = assert_translate_interm dict
          ``(x = y) ∧ x ≠ {3}`` ``(x = y) ∧ x ≠ [3]``
val _ = assert_translate_interm dict
          ``x={2}`` ``x=[2]``

val _ = export_theory ()
