open HolKernel Parse boolLib bossLib;

(*
   Translates from sets to lists and fmaps to a tree structure
   This translation is done because the translator is unable to translate the
   previous type; it is also benificial to be able to have efficient datatypes
   in the translation (ie set implementation vs lists).

   Plan: Make it such that there exists a recursive type checker to see if a
         function/constant/variable/abstraction needs to be translated.

         A translator to actually translate these objects

         Hopefully something able to prove some properties which will ensure
         that the translation is correct

           -- Translate the head and determine the expected type of it
           -- Split this up to match the args
              -- If types match don't do anything
              -- Else translate the arg
                 -- If types now match make a rel and translate (can match)
                 -- Else fail
           -- 
*)

val _ = new_theory "datatypeTranslate";

fun mk_kn (thy_str : string) (nam_str : string) : KernelSig.kernelname =
  {Thy=thy_str, Name=nam_str};

fun strip_type_n ty n =
  if (n > 0) then
    case dest_type ty of
        ("fun", [x, xs]) => x :: strip_type_n xs (n-1)
      | _                => [ty]
  else [ty];

fun strip_type ty =
  case dest_type ty of
      ("fun", [x, xs]) => x :: strip_type xs
    | _                => [ty];

fun list_mk_type tyl =
  case tyl of
      [x]     => x
    | x :: xs => mk_type ("fun", [x, list_mk_type xs])
    | _       => failwith "cannot make type"

fun strip_type_meta ty =
  case dest_type ty of
      (meta, [x, xs]) => (meta, x) :: strip_type_meta xs
    | (meta, [x])     => [(meta, x)];

fun list_mk_type tyl =
  case tyl of
      [x]     => x
    | x :: xs => mk_type ("fun", [x, list_mk_type xs])
    | _       => failwith "cannot make type"

fun nonred_insert elm alist =
  let
    val (key, v) = elm
  in
    if (key = v) then alist else elm :: alist
  end;

fun lookup_const dict tm =
    let
      val {Name, Thy, Ty} = dest_thy_const tm
    in
      Binarymap.peek (dict, mk_kn Thy Name)
    end;

fun strip_type_narg ty n =
  if (n > 0) then
    case dest_type ty of
        ("fun", [x, xs]) => x :: strip_type_narg xs (n-1)
      | _                => [ty]
  else [];

datatype hol_type_ext = Single of hol_type
                      | Multiple of hol_type_ext * hol_type_ext list

fun get_single ty =
  case ty of
      Single x => x
    | Multiple (x, _) => get_single x

fun type_comb_resty ty n =
  if (n > 0) then
    case dest_type ty of
        ("fun", [x, xs]) => type_comb_resty xs (n-1)
      | _                => [ty]
  else [ty];

fun type_of_ext tm =
  if is_const tm then
    Single (type_of tm)
  else if is_var tm then
    Single (type_of tm)
  else if is_comb tm then
    let
      val (head, args) = strip_comb tm
      val head_type = type_of_ext head
      val args_type = map type_of_ext args
      val return_ty = hd (type_comb_resty (type_of head) (length args))
    in
      Multiple (Single return_ty, head_type :: args_type)
    end
  else if is_abs tm then
    let
      val (var, body) = dest_abs tm
      val var_type = type_of_ext var
      val body_type = type_of_ext body
    in
      Multiple (Single type_of tm, var_type :: [body_type])
    end
  else failwith "Term type cannot be identified";

fun gen_new_tyvar ty subst_map =
  if is_vartype ty then
    if ty = type_subst subst_map ty then
      let
        val new_ty = gen_tyvar ()
      in
        (new_ty, (match_type ty new_ty) @ subst_map)
      end
    else
      (type_subst subst_map ty, subst_map)
  else
    case dest_type ty of
        (str, [])    => (ty, subst_map)
      | (str, tlist) =>
        let
          fun inner_acc tylist subst acc =
            case tylist of
                [] => (acc, subst)
              | x :: xs =>
                let
                  val (x', subst') = gen_new_tyvar x subst
                in
                  inner_acc xs subst' (x' :: acc)
                end
          val (tlist', subst_map') = inner_acc tlist subst_map []
        in
          (mk_type (str, rev tlist'), subst_map')
        end

fun inst_tyvar ty = #1 (gen_new_tyvar ty [])

fun gen_new_tyvar_ext ty =
  let
    fun gen_new_tyvar_ext_acc ty' subst_map =
      case ty' of
          Single x =>
            let
              val (x', subst_map') = gen_new_tyvar x subst_map
            in
              (Single x', subst_map')
            end
        | Multiple (x, xs) =>
            let
              val (x', subst_map') = gen_new_tyvar_ext_acc x subst_map

              fun inner_acc tyl subst acc =
                case tyl of
                    [] => (acc, subst)
                  | y :: ys =>
                    let
                      val (y', subst') = gen_new_tyvar_ext_acc y subst
                    in
                      inner_acc ys subst' (y' :: acc)
                    end

              val (xs', subst_map'') = inner_acc xs subst_map' []
            in
              (Multiple (x', rev xs'), subst_map'')
            end
  in
    #1 (gen_new_tyvar_ext_acc ty [])
  end;

fun gen_new_ty_ext tm =
  if is_const tm then
    Single (gen_tyvar ())
  else if is_var tm then
    Single (gen_tyvar ())
  else if is_comb tm then
    let
      val (head, args) = strip_comb tm
      val return_ty = hd (type_comb_resty (type_of head) (length args))
    in
      Multiple (Single return_ty,
                (gen_new_ty_ext head) :: (map gen_new_ty_ext args))
    end
  else if is_abs tm then
    let
      val (var, body) = dest_abs tm
      val var_ty = gen_new_ty_ext var
      val body_ty = gen_new_ty_ext body
      val return_ty = (get_single var_ty) --> (get_single body_ty)
    in
      Multiple (Single return_ty, var_ty :: [body_ty])
    end
  else failwith "Unable to identify type of hol term"

fun type_subst_ext subst_map ty =
  case ty of
      Single x => Single (type_subst subst_map x)
    | Multiple (x, xs) =>
      let
        val x' = type_subst_ext subst_map x
        val xs' = map (type_subst_ext subst_map) xs
      in
        Multiple (x', xs')
      end

fun match_type_ext tye1 tye2 =
  let
    fun match_type_acc ty1 ty2 subst =
      case (ty1, ty2) of
          (Single x, Single y) =>
            let
              val subst' = filter (fn a => not (mem a subst)) (match_type x y)
            in
              subst' @ subst
            end
        | (Multiple (x, xs), Multiple (y,ys)) =>
          let
            val subst0 = match_type_acc x y subst
            fun inner_acc tyl1 tyl2 acc =
              case (tyl1, tyl2) of
                  ([], []) => acc
                | (b::bs, c::cs) => inner_acc bs cs (match_type_acc b c acc)
                | (_, _) => failwith "Types do not match"
          in
            inner_acc xs ys subst0
          end
        | (_, _) => failwith "Types do not match"
  in
    match_type_acc tye1 tye2 []
  end

fun find_type dict_out tm_out =
  let
    fun find_type_aux dict tm pred_ty post_ty =
      if is_const tm then
        let
          val {Name, Thy, Ty} = dest_thy_const tm
        in
          case Binarymap.peek (dict, mk_kn Thy Name) of
              NONE =>
              let
                val ty0 = type_of (prim_mk_const {Name=Name, Thy=Thy})
                val ty' = inst_tyvar ty0
              in
                if can (match_type ty') pred_ty andalso
                   can (match_type ty') post_ty then
                  let
                    val inst1 = match_type ty' pred_ty
                    val inst2 = match_type ty' post_ty
                  in
                    (Single (type_subst inst1 ty'),
                     Single (type_subst inst2 ty'))
                  end
                else
                  (Single ty', Single ty')
              end
            | SOME (keyty, tm') =>
              let
                val keyty0 = inst_tyvar keyty
                val inst0 = match_type keyty keyty0
                val ty0 = type_subst inst0 (type_of tm')
              in
                if can (match_type keyty0) pred_ty andalso
                   can (match_type ty0) post_ty then
                  let
                    val inst1 = match_type keyty0 pred_ty
                    val inst2 = match_type ty0 post_ty
                  in
                    (Single (type_subst inst1 keyty0),
                     Single (type_subst inst2 ty0))
                  end
                else
                  (Single keyty0, Single ty0)
              end
        end
      else if is_var tm then
        (Single pred_ty, Single post_ty)
      else if is_comb tm then
        let
          val (head0, args0) = strip_comb tm
          val no_args = length args0
          val (pred_hdtye, post_hdtye) =
                find_type_aux dict head0 pred_ty post_ty
          val pred_hdty0 = get_single pred_hdtye
          val post_hdty0 = get_single post_hdtye
          val pred_res0 = hd (type_comb_resty pred_hdty0 no_args)
          val post_res0 = hd (type_comb_resty post_hdty0 no_args)
          
          val pred_inst0 = if can (match_type pred_res0) pred_ty then
                             match_type pred_res0 pred_ty
                           else []
          val post_inst0 = if can (match_type post_res0) post_ty then
                             match_type post_res0 post_ty
                           else []

          val pred_hdty = type_subst pred_inst0 pred_hdty0
          val post_hdty = type_subst post_inst0 post_hdty0

          val pred_argsty = strip_type_narg pred_hdty no_args
          val post_argsty = strip_type_narg post_hdty no_args

          (*
            val args = args0
            val pred_argsty' = pred_argsty
            val post_argsty' = post_argsty
            val pred_hdty' = pred_hdty
            val post_hdty' = post_hdty
            val pred_acc = []
            val post_acc = []

            val (x::xs) = args
            val (y::ys) = pred_argsty'
            val (z::zs) = post_argsty'
          *)

          fun do_args args pred_argsty' post_argsty' pred_hdty' post_hdty'
                      pred_acc post_acc =
            case (args, pred_argsty', post_argsty') of
                ([], [], []) => (pred_hdty', post_hdty', pred_acc, post_acc)
              | (x::xs, y::ys, z::zs) =>
                let
                  val (pred_arg, post_arg) = find_type_aux dict x y z
                in
                  if pred_arg = post_arg then
                    (*
                      val args = xs
                      val pred_argsty' = ys
                      val post_argsty' = zs
                      val pred_hdty' = pred_hdty'
                      val post_hdty' = post_hdty'
                      val pred_acc = pred_arg :: pred_acc
                      val post_acc = post_arg :: post_acc

                      val (x::xs) = args
                      val (y::ys) = pred_argsty'
                      val (z::zs) = post_argsty'
                    *)
                    do_args xs ys zs pred_hdty' post_hdty'
                      (pred_arg :: pred_acc) (post_arg :: post_acc)

                  else
                    let
                      val pred_aty = get_single pred_arg
                      val post_aty = get_single post_arg
                      val pred_inst = if can (match_type y) pred_aty then
                                        match_type y pred_aty
                                      else []
                      val post_inst = if can (match_type z) post_aty then
                                        match_type z post_aty
                                      else []
                      val ys' = map (type_subst pred_inst) ys
                      val zs' = map (type_subst post_inst) zs
                      val pred_hdty'' = type_subst pred_inst pred_hdty'
                      val post_hdty'' = type_subst post_inst post_hdty'
                      val pred_acc' = map (type_subst_ext pred_inst) pred_acc
                      val post_acc' = map (type_subst_ext post_inst) post_acc
                      (*
                        val args = xs
                        val pred_argsty' = ys'
                        val post_argsty' = zs'
                        val pred_hdty' = pred_hdty''
                        val post_hdty' = post_hdty''

                        val (x::xs) = args
                        val (y::ys) = pred_argsty'
                        val (z::zs) = post_argsty'
                        val pred_acc = pred_arg :: pred_acc'
                        val post_acc = post_arg :: post_acc'
                      *)
                    in
                      do_args xs ys' zs' pred_hdty'' post_hdty''
                        (pred_arg :: pred_acc') (post_arg :: post_acc')
                    end
                end
              | (_, _, _) => failwith "Incorrect argument types"

          val (pred_hdty', post_hdty', pred_args, post_args) =
            do_args args0 pred_argsty post_argsty pred_hdty post_hdty [] []
          val pred_resty = hd (type_comb_resty pred_hdty' no_args)
          val post_resty = hd (type_comb_resty post_hdty' no_args)
        in
          (Multiple (Single pred_resty, (Single pred_hdty') :: rev pred_args),
           Multiple (Single post_resty, (Single post_hdty') :: rev post_args))
        end
      else if is_abs tm then
        failwith "unimplemented"
      else failwith "Unable to identify type of hol term"
  in
    find_type_aux dict_out tm_out (gen_tyvar ()) (gen_tyvar ())
  end

fun translate_conc dict tm ety =
  if is_const tm then
    case ety of
        Single ty =>
          let
            val {Name=name, Thy=thy, Ty=_} = dest_thy_const tm
          in
            case Binarymap.peek (dict, mk_kn thy name) of
                NONE => mk_thy_const {Name=name, Thy=thy, Ty=ty}
              | SOME (_, tm') =>
                let
                  val {Name=name', Thy=thy', Ty=_} = dest_thy_const tm'
                in
                  mk_thy_const {Name=name', Thy=thy', Ty=ty}
                end
          end
      | _ => failwith "Unable to match extended hol type with hol term"
  else if is_var tm then
    case ety of
        Single ty =>
          let
            val (name, _) = dest_var tm
          in
            mk_var (name, ty)
          end
      | _ => failwith "Unable to match extended hol type with hol term"
  else if is_comb tm then
    case ety of
        Multiple (res_ty, head_ety :: args_ety) => 
          let
            val (head, args) = strip_comb tm
            val no_args = length args
            val head_t = translate_conc dict head head_ety
            val args_t = map2 (translate_conc dict) args args_ety
          in
            list_mk_comb (head_t, args_t)
          end
      | _ => failwith "Unable to match extended hol type with hol term"
  else if is_abs tm then
    failwith "Unimplemented"
  else failwith "Unable to identify type of hol term"

fun translate_interm dict tm =
  let
    val ty = type_of_ext tm
    val (key_ty, fin_ty0) = find_type dict tm
    val fin_inst = match_type_ext key_ty ty
    val fin_ty = type_subst_ext fin_inst fin_ty0
  in
    translate_conc dict tm fin_ty
  end

val _ = export_theory ()
