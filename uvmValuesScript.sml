open HolKernel Parse boolLib bossLib

open uvmTypesTheory
open uvmErrorsTheory
open monadsTheory
open syntaxSugarTheory

open ternaryComparisonsTheory
open wordsTheory

val _ = new_theory "uvmValues"
val _ = monadsyntax.add_monadsyntax()
val _ = reveal "C"

(* An iaddr (iref address) is an address with an offset. The offset refers to a
   value nested within the parent value that "iaddr.base" refers to.

   "gen_iaddr", like "gen_value" (next definition), is generic in its address
   type. The alias "iaddr" is defined in uvmMemoryTheory.
*)
val _ = Datatype`
  gen_iaddr = <|
    base: 'addr;
    offset: num;
    base_ty: uvm_type;
    ty: uvm_type
  |>
`

(* The type "value" is defined as:
      
       ('addr, 'func, 'stack, 'thread) gen_value
      
   Its generic parameters are:

   ∙ 'addr: Value address type used by Ref and IRef
   ∙ 'func: Function address type used by FuncRef
   ∙ 'stack: Stack address type used by StackRef
   ∙ 'thread: Thread address type used by ThreadRef

   This type is defined as generic in order to separate the definition of the
   "addr" type, and several other types it depends on, into uvmMemoryTheory
   where it belongs. The "value" alias is also defined in uvmMemoryTheory.
*)
val _ = Datatype`
  gen_value =
  | IntV num num
  | FloatV word32
  | DoubleV word64
  | RefV uvm_type ('addr option)
  | IRefV ref_type ('addr gen_iaddr option)
  | FuncRefV ref_type funcsig ('func option)
  | StackRefV ('stack option)
  | ThreadRefV ('thread option)
  | UndefV uvm_type
`

val type_of_def = Define`
    type_of (IntV n _) = Int n
  ∧ type_of (FloatV _) = Float
  ∧ type_of (DoubleV _) = Double
  ∧ type_of (RefV t _) = Ref (Type t)
  ∧ type_of (IRefV r (SOME a)) = IRef r (Type a.ty)
  ∧ type_of (IRefV r NONE) = IRef r (Type Void) (* FIXME: This might cause typechecking errors *)
  ∧ type_of (FuncRefV r sig _) = FuncRef r sig
  ∧ type_of (StackRefV _) = StackRef
  ∧ type_of (ThreadRefV _) = ThreadRef
  ∧ type_of (UndefV ty) = ty
`

val zero_value_def = Define`
    zero_value Void = Fail (Impossible "cannot allocate void")
  ∧ zero_value (Int sz) = return (IntV sz 0)
  ∧ zero_value Float = return (FloatV 0w)
  ∧ zero_value Double = return (DoubleV 0w)
  ∧ zero_value (Ref _) = return (RefV Void NONE)
  ∧ zero_value (IRef rt _) = return (IRefV rt NONE)
  ∧ zero_value (WeakRef _) = Fail (NotImplemented "weakref")
  ∧ zero_value (FuncRef rt sig) = return (FuncRefV rt sig NONE)
  ∧ zero_value ThreadRef = return (ThreadRefV NONE)
  ∧ zero_value StackRef = return (StackRefV NONE)
  ∧ zero_value TagRef64 = Fail (NotImplemented "tagref64")
  ∧ zero_value FrameCursorRef = Fail (NotImplemented "framecursorref")
  ∧ zero_value IrBuilderRef = Fail (NotImplemented "irbuilderref")
  ∧ zero_value _ = Fail (Impossible "zero_value applies only to scalar types")
`

val _ = Datatype`
  type_mismatch =
  | NotInt uvm_type
  | NotRef uvm_type
  | NotIRef uvm_type
  | NotHybrid uvm_type
  | NotCallable uvm_type
  | NotType uvm_type uvm_type
`

val _ = type_abbrev("type_error", ``:(string # type_mismatch) error ``)

local
  val value = ty_antiq ``:(α, β, σ, τ) gen_value``
  val ub = ``CURRY UndefBehavior: string -> type_mismatch -> type_error``
in

(* Accessor functions

   Destructure μVM values into HOL values, using the error monad to return
   error conditions (such as type mismatches). Some return an option, which will
   be NONE if the return value is undefined but not necessarily an error state.
*)

val get_int1_as_bool_def = Define`
    get_int1_as_bool (IntV 1 n : ^value) : bool option + type_error =
      return (SOME (n ≠ 0))
  ∧ get_int1_as_bool (UndefV (Int 1)) = return NONE
  ∧ get_int1_as_bool v =
      Fail (^ub "get_int1_as_bool" (NotType (Int 1) (type_of v)))
`

val int_type_size_def = Define`
    int_type_size _ (Int n : uvm_type) : num + type_error = return n
  ∧ int_type_size (msg : string) ty = Fail (^ub msg (NotInt ty))
`

val get_int_as_num_def = Define`
    get_int_as_num _ (IntV _ n : ^value) : num option + type_error = OK (SOME n)
  ∧ get_int_as_num _ (UndefV (Int _)) = return NONE
  ∧ get_int_as_num (msg : string) v = Fail (^ub msg (NotInt (type_of v)))
`

val get_iref_addr_def = Define`
    get_iref_addr (IRefV _ a : ^value) : α gen_iaddr option + type_error =
      return a
  ∧ get_iref_addr (UndefV (IRef _ _)) = return NONE
  ∧ get_iref_addr v = Fail (^ub "get_iref_addr" (NotIRef (type_of v)))
`

val get_funcref_data_def = Define`
    get_funcref_data (FuncRefV _ s n : ^value)
                     : (funcsig # β) option + type_error =
      return ($, s <$> n)
  ∧ get_funcref_data (UndefV (FuncRef _ _)) = return NONE
  ∧ get_funcref_data v =
      Fail (^ub "get_funcref_data" (NotCallable (type_of v)))
`

val get_stackref_id_def = Define`
    get_stackref_id (StackRefV s : ^value) : σ option + type_error = return s
  ∧ get_stackref_id (UndefV StackRef) = return NONE
  ∧ get_stackref_id v =
      Fail (^ub "get_stackref_id" (NotType StackRef (type_of v)))
`

val get_threadref_id_def = Define`
    get_threadref_id (ThreadRefV t : ^value) : τ option + type_error = return t
  ∧ get_threadref_id (UndefV ThreadRef) = return NONE
  ∧ get_threadref_id v =
      Fail (^ub "get_threadref_id" (NotType ThreadRef (type_of v)))
`

(* Mutator functions

   Update μVM values in various ways, propagating undefined values and
   returning error conditions if necessary.
*)

val compare_unsigned_def = Define`
  compare_unsigned (v1 : ^value) (v2 : ^value)
                   : ordering option + type_error =
    let ty1 = type_of v1; ty2 = type_of v2 in do
      assert (ty1 = ty2) or_fail ^ub "compare_unsigned" (NotType ty1 ty2);
      n1 <- get_int_as_num "compare_unsigned" v1;
      n2 <- get_int_as_num "compare_unsigned" v2;
      return (num_compare <$> n1 <*> n2)
    od
`

val int_op_def = Define`
  int_op (op : num -> num) (v : ^value) : ^value + type_error = do
    sz <- int_type_size "int_op" (type_of v);
    n <- get_int_as_num "int_op" v;
    return (case n of
            | SOME n => IntV sz (op n MOD 2 ** sz)
            | NONE => UndefV (Int sz))
  od
`

val int_binop_def = Define`
  int_binop (op : num -> num -> num) (v1 : ^value) (v2 : ^value)
            : ^value + type_error =
    let ty1 = type_of v1; ty2 = type_of v2 in do
      sz1 <- int_type_size "int_binop" ty1;
      sz2 <- int_type_size "int_binop" ty2;
      assert (sz1 = sz2) or_fail ^ub "int_binop" (NotType ty1 ty2);
      n1 <- get_int_as_num "int_binop" v1;
      n2 <- get_int_as_num "int_binop" v2;
      return (case (λx y. IntV sz1 (op x y MOD 2 ** sz1)) <$> n1 <*> n2 of
              | SOME x => x
              | NONE => UndefV (Int sz1))
    od
`

val get_iref_def = Define`
    get_iref (RefV t a : ^value) : ^value + type_error =
      return (IRefV REF (do
        a <- a;
        return <|base_ty := t; base := a; offset := 0; ty := t|>
      od))
  ∧ get_iref (UndefV (Ref ty)) = return (UndefV (IRef REF ty))
  ∧ get_iref v = Fail (^ub "get_iref" (NotRef (type_of v)))
`

val map_iref_def = Define`
    map_iref _ _ (UndefV (IRef r _)) = return (UndefV (IRef r (Type ARB)))
  ∧ map_iref _ _ (IRefV r NONE) = return (UndefV (IRef r (Type ARB)))
  ∧ map_iref (msg : string)
             (f : α gen_iaddr -> α gen_iaddr option + type_error)
             (IRefV r (SOME ia) : ^value)
             : ^value + type_error =
      (let mk_iref opt =
         case opt of
         | SOME ia' => IRefV r (SOME ia')
         | NONE => UndefV (IRef r (Type ia.ty))
       in mk_iref <$> f ia)
  ∧ map_iref msg _ v = Fail (^ub msg (NotIRef (type_of v)))
`

val get_member_iaddr_def = Define`
  get_member_iaddr (ia : α gen_iaddr) (n : num)
                   : α gen_iaddr option =
    let members = member_types (SOME n) ia.ty in do
      assert (n < LENGTH members);
      return (ia with <|
        offset updated_by $+ (SUM (MAP max_offset (TAKE n members)));
        ty := EL n members
      |>)
    od
`

val get_field_iref_def = Define`
  get_field_iref (v : ^value) (field : num) : ^value + type_error =
    map_iref "get_field_iref" (return o C get_member_iaddr field) v
`

val get_element_iref_def = Define`
  get_element_iref (irv : ^value) (elv : ^value) : ^value + type_error =
    let get_el ia = do
      el <- get_int_as_num "get_element_iref" elv;
      return (el >>= get_member_iaddr ia)
    od in map_iref "get_element_iref" get_el irv
`

val shift_iaddr_def = Define`
  shift_iaddr (ia : α gen_iaddr) (n : num) : α gen_iaddr + type_error = do
    assert (array_type ia.offset ia.base_ty ia.ty) or_fail
      ^ub "shift_iaddr" (NotType ia.base_ty (Array ia.ty 0));
    (* TODO: Support negative offsets *)
    return (ia with offset updated_by $+ (max_offset ia.ty * n))
  od
`

val shift_iref_def = Define`
  shift_iref (irv : ^value) (offv : ^value) : ^value + type_error =
    let shift ia = do
      off <- get_int_as_num "shift_iref" offv;
      case off of
      | NONE => return NONE
      | SOME n => SOME <$> shift_iaddr ia n
    od in map_iref "shift_iref" shift irv
`
end

val _ = export_theory()

