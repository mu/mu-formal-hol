open HolKernel bossLib

signature monadsLib =
sig
  val define_sum : string * string * hol_type * string * hol_type -> unit

  val define_monad : string * (hol_type -> hol_type) * term * term -> unit 
end

