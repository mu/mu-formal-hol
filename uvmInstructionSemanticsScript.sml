open HolKernel Parse boolLib bossLib

open uvmMemoryTheory
open uvmIRTheory
open uvmValuesTheory
open uvmErrorsTheory
open monadsTheory
open monadsLib
open syntaxSugarTheory
open extraTactics

open alistTheory
open arithmeticTheory
open combinTheory
open listTheory
open numpairTheory
open optionTheory
open pairTheory
open pred_setTheory
open sptreeTheory
open ternaryComparisonsTheory
open lcsymtacs

val _ = new_theory "uvmInstructionSemantics"
val _ = monadsyntax.add_monadsyntax()
val _ = reveal "C"

val _ = Datatype`
  uvm_error_message =
  | TypeMismatch string type_mismatch
  | MemoryError addr mem_error_message
  | OutOfBounds string uvm_type num
  | UndefReference uvm_type
  | UndefHybridSize uvm_type
  | UndefBranch
  | NoSuchFunction func_name
  | FrameNotReady frame_id
  | UncaughtException (value + addr mem_error)
`

val _ = type_abbrev("or_error", ``:α + uvm_error_message error``)
val _ = overload_on("lift_type_error", ``lift_err_msg (UNCURRY TypeMismatch)``)
val _ = overload_on("lift_mem_error", ``lift_err_msg (UNCURRY MemoryError)``)

(* A reg_reader is a function that takes a list of register values. *)
val _ = Datatype `reg_reader = WithRegs (num list) (value list -> α)`

(* An SSA map is a mapping from an SSA variable to a range of registers.
   The registers are assumed to be contiguous.
   The return value of an SSA map is (first register, length - 1).
*)
val _ = type_abbrev("ssa_map", ``:ssavar -> (num # num)``)

val ssa_regs_def = Define`
  ssa_regs (ssa : ssa_map) (v : ssavar) : num list =
    let (offset, size) = ssa v in GENLIST ($+ offset) size
`

val _ = overload_on("pure", ``λv. WithRegs [] (K v)``)

val apply_reg_reader_def = Define`
  apply_reg_reader (WithRegs regs f) (WithRegs regs' g) =
    WithRegs (regs ++ regs') (λxs. 
      f (TAKE (LENGTH regs) xs) (g (DROP (LENGTH regs) xs)))
`

val lift_reg_reader_def = Define`
  lift_reg_reader : (α -> β) -> α reg_reader -> β reg_reader =
    apply_reg_reader o pure
`

val _ = overload_on("APPLICATIVE_FAPPLY", ``apply_reg_reader``)
val _ = overload_on("<$>",                ``lift_reg_reader``)

val sequence_reg_reader_def = Define`
  sequence_reg_reader : α reg_reader list -> α list reg_reader =
    FOLDR (λx xs. CONS <$> x <*> xs) (pure [])
`

val _ = overload_on("sequence", ``sequence_reg_reader``)

val get_def = Define`
  get (ssa : ssa_map) (v : ssavar) : value reg_reader list =
    MAP (λr. WithRegs [r] HD) (ssa_regs ssa v)
`

val getc_def = Define`
    getc ssa (Var v) = get ssa v
  ∧ getc _ (Const _ hd tl) = MAP pure (hd::tl)
`

val run_reg_reader_def = Define`
  run_reg_reader (WithRegs read fn) (regs : mem_region) : α + β error =
    fn <$> mapM (λn. OPTION_MAP FST (lookup n regs)
                       or_fail Impossible "undefined register")
                read
`

val reads_reg_def = Define`
  reads_reg (r : num) (WithRegs regs _) ⇔ MEM r regs
`

(* A well-formed ("ok") reg_reader expects exactly the number of arguments that
   it claims it does, and does not do anything with extra arguments. All
   reg_readers constructed by any method other than directly invoking the
   WithRegs constructor should be well-formed by definition.
*)

val reg_reader_ok_def = Define`
  reg_reader_ok (WithRegs regs fn) ⇔
    ∀vals. fn (TAKE (LENGTH regs) vals) = fn vals
`

val _ = Datatype`
  jumps = <|
    nor_jump : num # (num + value) list;
    exc_jump : (num # (num + value) list) option
  |>
`

(* A _command_ is a message for a μVM-specific action that is outside the
   RISC-V/WMM memory model, such as memory allocation or thread spawning.
   Commands have no ordering relationship to stores; they are evaluated
   immediately and synchronously.
*)
val _ = Datatype`
  command =
  | DoPushStack func_name funcsig (value list) (jumps option)
  | DoPopStack (value resume_values)
  | DoJump num (value list)
  | DoHeapAlloc uvm_type (num option) num
  | DoStackAlloc uvm_type (num option) num
  (* TODO: DoNewStack *)
  | DoGetStack num
  (* TODO: DoSwapStack *)
  | DoNewThread stack_id (value resume_values) num
  | DoThreadExit
`

(* An _operation_ is an atomic instruction in the WMM model. It includes the
   first five operations from the WMM paper (Nm, Ld, St, Com, Rec), along with
   a μVM-specific Cmd operation for actions not defined in WMM (e.g., spawning
   threads).
   
   Background operations, such as DeqSb, are not defined here; they can be
   inserted between foreground operations by the scheduler layer.
*)
val _ = Datatype`
  opn =
  (* Non-memory execution. The reader monad returns an association list of
     newly-assigned register values. *)
  | Nm ((num, value) alist or_error reg_reader)
  (* Load execution. Loads an IRef from the register in the first argument, then
     puts the value at that IRef in the register specified in the second
     argument. See pg 6 of the WMM paper for the semantics of a load. *)
  | Ld (iaddr or_error reg_reader) num
  (* Store execution. Writes the return value of the reader monad to the store
     buffer (sb). Deviates from the WMM spec by NOT clearing the invalidation
     buffer (ib); instead, the scheduler layer may remove ib entries
     arbitrarily. *)
  | St ((iaddr # value) or_error reg_reader)
  (* Commit fence. Blocks unless sb is empty. *)
  | Com
  (* Reconcile fence. Clears the ib. *)
  | Rec
  (* Command execution. Sends a command message to the scheduler layer. *)
  | Cmd (command or_error reg_reader)
`

(* An _operation block_ is a basic block of operations. It is the result of
   compiling the instructions in a μVM bblock into operations and expanding
   any composite-typed parameters into their scalar components.
*)
val _ = Datatype`
  opn_block = <|
    regs : (uvm_type list # num list) list;
    exc_param : num option;
    opns : opn list
  |>
`

val _ = type_abbrev("program", ``:func_name |-> opn_block spt``)

val opn_reads_reg_def = Define`
    opn_reads_reg r (Nm rdr) = reads_reg r rdr
  ∧ opn_reads_reg r (Ld rdr _) = reads_reg r rdr
  ∧ opn_reads_reg r (St rdr) = reads_reg r rdr
  ∧ opn_reads_reg r (Cmd rdr) = reads_reg r rdr
  ∧ opn_reads_reg _ _ = F
`

(* `map_reader_regs reg_map rdr` returns an equivalent-upto-register-renaming
   version of the monadic operation `rdr`, using `reg_map` as the mapping from
   old register names to new.

   `reg_map` must be an injective function for the mapping to make sense.
*)
val map_reader_regs_def = Define`
  map_reader_regs (reg_map : num -> num) (WithRegs regs fn) : α reg_reader =
    WithRegs (MAP reg_map regs) fn
`

(* `map_opn_regs reg_map opn` returns an equivalent-upto-register-renaming
   version of the operation `opn`, using `reg_map` as the mapping from old
   register names to new.

   `reg_map` must be an injective function for the mapping to make sense.
*)
val map_opn_regs_def = Define`
    map_opn_regs reg_map (Nm rdr) =
      Nm (liftM (MAP (reg_map ## I)) <$> map_reader_regs reg_map rdr)
  ∧ map_opn_regs reg_map (Ld rdr dst) =
      Ld (map_reader_regs reg_map rdr) (reg_map dst)
  ∧ map_opn_regs reg_map (St rdr) =
      St (map_reader_regs reg_map rdr)
  ∧ map_opn_regs reg_map (Cmd rdr) =
      (let map_cmd_regs cmd =
         case cmd of
         | DoHeapAlloc ty len dst => DoHeapAlloc ty len (reg_map dst)
         | DoStackAlloc ty len dst => DoStackAlloc ty len (reg_map dst)
         | DoGetStack dst => DoGetStack (reg_map dst)
         | DoNewThread sid rvs dst => DoNewThread sid rvs (reg_map dst)
         | c => c
       in Cmd (liftM map_cmd_regs <$> map_reader_regs reg_map rdr))
  ∧ map_opn_regs _ c = c
`

val eval_expr_def = Define`
    eval_expr (ssa : ssa_map) (Id _ v : expression) : value list or_error reg_reader =
      OK <$> sequence (getc ssa v)
  ∧ eval_expr ssa (BinOp op _ v1 v2) =
      (λv1 v2 op. return <$> lift_type_error (op >>= λop. int_binop op v1 v2))
      <$> HD (getc ssa v1)
      <*> HD (getc ssa v2)
      <*> pure (case op of
                | ADD => return $+
                | SUB => return $-
                | MUL => return $*
                | _ => Fail (NotImplemented "most binops"))
  ∧ eval_expr ssa (CmpOp op _ v1 v2) = (
      if (op = EQ ∨ op = NE) then
        (λv1 v2. return [IntV 1 (if (v1 = v2) ⇔ (op = EQ) then 1 else 0)])
        <$> sequence (getc ssa v1)
        <*> sequence (getc ssa v2)
      else
        (λv1 v2.
          lift_type_error (compare_unsigned v1 v2) >>= λco.
          case co of
          | SOME c => do
              b <- case op of
                   | UGE => return (c = EQUAL ∨ c = GREATER)
                   | UGT => return (c = GREATER)
                   | ULE => return (c = EQUAL ∨ c = LESS)
                   | ULT => return (c = LESS)
                   | _ => Fail (NotImplemented "most cmpops");
              return [IntV 1 (if b then 1 else 0)]
            od
          | NONE => return [UndefV (Int 1)])
        <$> HD (getc ssa v1)
        <*> HD (getc ssa v2))
  ∧ eval_expr ssa (ConvOp REFCAST ty1 ty2 v) =
     (* TODO: handle undefined values here *)
     (let type_mismatch ty1 ty2 =
        Fail (UndefBehavior (TypeMismatch "REFCAST" (NotType ty1 ty2))) in
      case ty1, ty2 of
      | Ref (Type Void), Ref (Type t) =>
          (λv. case v of
               | RefV _ a => return [RefV t a]
               | _ => type_mismatch ty1 (type_of v))
          <$> HD (getc ssa v)
      | Ref (Type t), Ref (Type Void) =>
          (λv. case v of
               | RefV _ a => return [RefV t a]
               | _ => type_mismatch ty1 (type_of v))
          <$> HD (getc ssa v)
      | _ => pure (type_mismatch ty1 ty2))
  ∧ eval_expr _ (ConvOp _ _ _ _) =
      pure (Fail (NotImplemented "non-REFCAST conversions"))
    (* TODO: Select *)
  ∧ eval_expr _ (Select _ _ _ _ _) =
      pure (Fail (NotImplemented "SELECT"))
  ∧ eval_expr ssa (ExtractValue ty off v) =
     (let members = member_types (SOME off) ty;
          sizeof = LENGTH o component_types NONE in
        (λvs. do
          assert (off < LENGTH members)
            or_fail UndefBehavior (OutOfBounds "EXTRACTVALUE" ty off);
          let out = TAKE (sizeof (EL off members))
                         (DROP (SUM (MAP sizeof (TAKE off members))) vs) in
          (assert (¬NULL out)
             or_fail UndefBehavior (OutOfBounds "EXTRACTVALUE" ty off)) *>
          return out
        od) <$> sequence (getc ssa v))
  ∧ eval_expr ssa (InsertValue ty off base el) =
     (let members = member_types (SOME off) ty;
          sizeof = LENGTH o component_types NONE in
        (λvs vs'. do
          assert (off < LENGTH members)
            or_fail UndefBehavior (OutOfBounds "INSERTVALUE" ty off);
          let out =
            (TAKE (SUM (MAP sizeof (TAKE off members))) vs) ++ vs' ++
            (DROP (SUM (MAP sizeof (TAKE (SUC off) members))) vs) in
          (assert (LENGTH vs = LENGTH out)
            or_fail UndefBehavior (OutOfBounds "INSERTVALUE" ty off)) *>
          return out
        od) <$> sequence (getc ssa base)
            <*> sequence (getc ssa el))
  ∧ eval_expr get (ExtractElement ty ity v i) =
      pure (Fail (NotImplemented "EXTRACTELEMENT"))
  ∧ eval_expr get (InsertElement ty ity v1 i v2) =
      pure (Fail (NotImplemented "INSERTELEMENT"))
(*
  ∧ eval_expr get (ExtractElement ty ity v i) = do
      v <- get v; i <- get i;
      assert_type_eq ty (type_of_value v) "EXTRACTELEMENT array/vector";
      assert_type_eq ity (type_of_value i) "EXTRACTELEMENT index";
      off <- get_int_as_num "EXTRACTELEMENT index" i;
      return <$> value_offset_get v off
    od
  ∧ eval_expr get (InsertElement ty ity v1 i v2) = do
      v1 <- get v1; i <- get i; v2 <- get v2;
      assert_type_eq ty (type_of_value v1) "INSERTELEMENT array/vector";
      assert_type_eq ity (type_of_value i) "INSERTELEMENT index";
      off <- get_int_as_num "INSERTELEMENT index" i;
      return <$> value_offset_update v1 off v2
    od
*)
    (* TODO: ShuffleVector *)
  ∧ eval_expr _ (ShuffleVector _ _ _ _ _) =
      pure (Fail (NotImplemented "SHUFFLEVECTOR"))
  ∧ eval_expr ssa (GetIRef _ ref) = 
      (liftM return o lift_type_error o get_iref)
      <$> HD (getc ssa ref)
  ∧ eval_expr ssa (GetFieldIRef _ _ n ref) =
      (liftM return o lift_type_error o C get_field_iref n)
      <$> HD (getc ssa ref)
  ∧ eval_expr ssa (GetElementIRef _ _ _ ref idx) =
      (λrv iv. return <$> lift_type_error (get_element_iref rv iv))
      <$> HD (getc ssa ref)
      <*> HD (getc ssa idx)
  ∧ eval_expr ssa (ShiftIRef _ _ _ ref ofv) =
      (λrv iv. return <$> lift_type_error (shift_iref rv iv))
      <$> HD (getc ssa ref)
      <*> HD (getc ssa ofv)
  ∧ eval_expr ssa (GetVarPartIRef _ _ ref) =
      (λrv. do
        ao <- lift_type_error (get_iref_addr rv);
        ia <- ao or_fail UndefBehavior (UndefReference (type_of rv));
        case ia.ty of
        | Hybrid fts _ => return <$>
            lift_type_error (get_field_iref rv (LENGTH fts))
        | ty =>
            Fail (UndefBehavior (TypeMismatch "GETVARPARTIREF" (NotHybrid ty)))
      od) <$> HD (getc ssa ref)
`

(* μVM instructions are split up into operations before they are executed. *)
val compile_inst_def = Define`
    compile_inst (ssa : ssa_map) (Assign vs e) : opn list =
      [Nm (liftM (CURRY ZIP (vs >>= ssa_regs ssa)) <$> eval_expr ssa e)]
  ∧ compile_inst (ssa : ssa_map) (Load dst _ _ src ord) : opn list =
      (let src_cells = get ssa src;
           dst_cells = ssa_regs ssa dst;
           lds = MAP2 (Ld o lift_reg_reader (λs. do
             ia_opt <- lift_type_error (get_iref_addr s);
             ia_opt or_fail UndefBehavior (UndefReference (type_of s))
           od)) src_cells dst_cells in
       if LENGTH src_cells = LENGTH dst_cells
       then case ord of
       | NOT_ATOMIC => lds
       | RELAXED => lds
       | CONSUME => do ld <- lds; [ld; Rec] od
       | ACQUIRE => do ld <- lds; [ld; Rec] od
       | SEQ_CST => do ld <- lds; [Com; Rec; ld; Rec] od
       | _ => (return o Nm o pure o Fail o Impossible)
                "unsupported memory order for LOAD"
       else (return o Nm o pure o Fail o Impossible)
               "LOAD into SSA variable of wrong type")
  ∧ compile_inst ssa (Store src _ _ dst ord) =
      (let src_cells = getc ssa src;
           dst_cells = get ssa dst;
           sts = MAP2 (λd s. St ((λd s. do
             ia_opt <- lift_type_error (get_iref_addr d);
             ia <- ia_opt or_fail UndefBehavior (UndefReference (type_of d));
             return (ia, s)
           od) <$> d <*> s)) dst_cells src_cells in
       if LENGTH src_cells = LENGTH dst_cells
       then case ord of
       | NOT_ATOMIC => sts
       | RELAXED => sts
       | RELEASE => do st <- sts; [Com; st] od
       | SEQ_CST => do st <- sts; [Com; st] od
       | _ => (return o St o pure o Fail o Impossible)
                "unsupported memory order for STORE"
       else (return o St o pure o Fail o Impossible)
              "STORE into location of wrong type")
  ∧ compile_inst _ (CmpXchg _ _ _ _ _ _ _ _ _ _) =
      [Nm (pure (Fail (NotImplemented "CMPXCHG")))]
  ∧ compile_inst _ (AtomicRMW _ _ _ _ _ _ _) =
      [Nm (pure (Fail (NotImplemented "ATOMICRMW")))]
  ∧ compile_inst _ (Fence _) =
      [Nm (pure (Fail (NotImplemented "FENCE")))]
  ∧ compile_inst ssa (New dst ty) =
      [Cmd (pure (return (DoHeapAlloc ty NONE (FST (ssa dst)))))]
  ∧ compile_inst ssa (Alloca dst ty) =
      [Cmd (pure (return (DoStackAlloc ty NONE (FST (ssa dst)))))]
  ∧ compile_inst ssa (NewHybrid dst ty _ len) =
      [Cmd ((λlenv. do
         n_opt <- lift_type_error (get_int_as_num "NEWHYBRID" lenv);
         n <- n_opt or_fail UndefBehavior (UndefHybridSize ty);
         return (DoHeapAlloc ty (SOME n) (FST (ssa dst)))
       od) <$> HD (getc ssa len))]
  ∧ compile_inst ssa (AllocaHybrid dst ty _ len) =
      [Cmd ((λlenv. do
         n_opt <- lift_type_error (get_int_as_num "ALLOCAHYBRID" lenv);
         n <- n_opt or_fail UndefBehavior (UndefHybridSize ty);
         return (DoStackAlloc ty (SOME n) (FST (ssa dst)))
       od) <$> HD (getc ssa len))]
  ∧ compile_inst ssa (NewThread dst stk tloc nsc) =
      [(* TODO: this*)]
`

val compile_terminst_def = Define`
    compile_terminst (lbl : block_label -> num) (ssa : ssa_map) (Ret vs) =
      [Cmd (return o DoPopStack o NOR <$> sequence (vs >>= getc ssa))]
  ∧ compile_terminst _ ssa (Throw v) =
      [Cmd (return o DoPopStack o EXC [] <$> HD (getc ssa v))]
  ∧ compile_terminst lbl ssa (Call dsts cdata rdata) = (
      let 
        ssas = SND (FOLDL (λ(n, m) dst.
            let len = SUC (SND (ssa dst)) in
            (n + len, m |+ (dst, GENLIST (INL o $+ n) len)))
          (0, FEMPTY) dsts);
        get_dst v =
          case v of
          | Var s => if MEM s dsts
                     then pure (ssas ' s)
                     else sequence (MAP ($<$> INR) (get ssa s))
          | Const _ hd tl => pure (MAP INR (hd::tl));
        fname =
          case cdata.name of
          | INL n => (λref. do
              fn_opt <- lift_type_error (get_funcref_data ref);
              (sig, name) <-
                fn_opt or_fail UndefBehavior (UndefReference (type_of ref));
              (* TODO: Assert sig = cdata.signature *)
              return name
            od) <$> HD (get ssa n)
          | INR name => pure (return name);
        jmps = (λn e. SOME <|
            nor_jump := (lbl (FST rdata.normal_dest), n);
            exc_jump := (λed. lbl (FST ed), e) <$> rdata.exceptional_dest
          |>) <$> (FLAT <$> sequence (MAP get_dst (SND rdata.normal_dest)))
              <*> (FLAT <$> sequence (MAP get_dst
                    (option_CASE rdata.exceptional_dest [] SND)))
      in [Cmd ((λx y z. (λx. DoPushStack x cdata.signature y z) <$> x)
               <$> fname
               <*> sequence (cdata.args >>= getc ssa)
               <*> jmps)])
  ∧ compile_terminst _ ssa (TailCall cdata) =
     (let fname =
        case cdata.name of
        | INL n => (λref. do
            fn_opt <- lift_type_error (get_funcref_data ref);
            (sig, name) <-
              fn_opt or_fail UndefBehavior (UndefReference (type_of ref));
            (* TODO: Assert sig = cdata.signature *)
            return name
          od) <$> HD (get ssa n)
        | INR name => pure (return name)
      in [Cmd ((λx y. (λx. DoPushStack x cdata.signature y NONE) <$> x)
               <$> fname
               <*> sequence (cdata.args >>= getc ssa))])
  ∧ compile_terminst lbl ssa (Branch1 (b, vs)) =
      [Cmd (return <$> (DoJump (lbl b) <$> sequence (vs >>= getc ssa)))]
  ∧ compile_terminst lbl ssa (Branch2 p (b1, vs1) (b2, vs2)) =
      [Cmd ((λbv vs1 vs2. do
        is1_opt <- lift_type_error (get_int1_as_bool bv);
        is1 <- is1_opt or_fail UndefBehavior UndefBranch;
        return (UNCURRY DoJump (
          if is1 then (lbl b1, vs1) else (lbl b2, vs2)))
      od) <$> HD (getc ssa p)
          <*> sequence (vs1 >>= getc ssa)
          <*> sequence (vs2 >>= getc ssa))]
  ∧ compile_terminst lbl ssa (Switch _ p default cases) =
      [Cmd (pure (Fail (NotImplemented "SWITCH")))]
    (*
      [Cmd (HD (getc ssa p) >>= λpv.
         let (b, vs) = (lbl ## I) (option_CASE (ALOOKUP cases pv) default I)
         in DoJump <$> return b <*> sequence (vs >>= getc ssa))]
    *)
  ∧ compile_terminst _ _ (Watchpoint _ _ _) =
      [Cmd (pure (Fail (NotImplemented "WATCHPOINT")))]
  ∧ compile_terminst _ _ (WPBranch _ _ _) =
      [Cmd (pure (Fail (NotImplemented "WPBRANCH")))]
  ∧ compile_terminst _ _ (SwapStack _ _ _ _ _) =
      [Cmd (pure (Fail (NotImplemented "SWAPSTACK")))]
  ∧ compile_terminst _ _ (CommInst _ _ _) =
      [Cmd (pure (Fail (NotImplemented "COMMINST")))]
  ∧ compile_terminst _ _ (ExcClause _ _) =
      [Cmd (pure (Fail (NotImplemented "EXC")))]
`

val offset_and_size_def = Define`
  offset_and_size (types : (ssavar # uvm_type) list) (n : num) : (num # num) =
    (SUM (MAP (max_offset o SND) (TAKE n types)), max_offset (SND (EL n types)))
`

val ssa_map_def = Define`
  ssa_map (types : (ssavar # uvm_type) list) : ssavar -> (num # num) =
    λssa. offset_and_size types (THE (INDEX_OF ssa (MAP FST types)))
`

val cells_of_types_def = Define`
    cells_of_types [] _ = []
  ∧ cells_of_types (ty::tys) n : (uvm_type list # num list) list =
      UNCURRY APPEND ((MAP SND ## cells_of_types tys)
                      (mem_cell_types NONE ty (λn. n, SUC n) n))
`

val compile_block_def = Define`
  compile_block (lbl : block_label -> num) (block : bblock) : opn_block =
    <| regs := cells_of_types (MAP SND (block_types block)) 0;
       exc_param := OPTION_MAP (FST o ssa_map (block_types block)) block.exc;
       opns := (block.body >>= compile_inst (ssa_map (block_types block))) ++
               compile_terminst lbl (ssa_map (block_types block)) block.exit |>
`

val compile_function_def = Define`
  compile_function (func : function) : opn_block spt =
    let lbls = GENLIST (λn. (FST (EL n func.blocks), n)) (LENGTH func.blocks)
    in fromList (MAP (compile_block (THE o ALOOKUP lbls) o SND) func.blocks)
`

val compile_program_def = Define`
  compile_program (env : environment) : program =
    alist_to_fmap(do
      (name, ver) <- env.func_versions;
      let fn = compile_function <$> ALOOKUP env.functions (THE ver)
      in if IS_SOME ver ∧ IS_SOME fn then [(name, THE fn)] else []
    od)
`

val is_jump_cmd_def = Define`
    is_jump_cmd (DoPushStack _ _ _ _) = T
  ∧ is_jump_cmd (DoPopStack _) = T
  ∧ is_jump_cmd (DoJump _ _) = T
  ∧ is_jump_cmd DoThreadExit = T
  ∧ is_jump_cmd _ = F
`

val is_jump_opn_def = Define`
    is_jump_opn (Cmd rdr) =
      (∃cmd regs. join (run_reg_reader rdr regs) = OK cmd ∧ is_jump_cmd cmd)
  ∧ is_jump_opn _ = F
`

val opns_ok_def = Define`
  opns_ok = COMPL NULL
          ∩ COMPL (EXISTS is_jump_opn o FRONT)
          ∩ is_jump_opn o LAST
`

val equiv_renamed_spt_def = Define`
  equiv_renamed_spt (for : num set)
                    (rename : num -> num)
                    (original : α spt)
                    (renamed : α spt)
                    : bool ⇔
    ∀k. k ∈ for ⇒ lookup k original = lookup (rename k) renamed
`

(* -------------------------------------------------------------------------- *)

val apply_reg_reader_thm = store_thm("apply_reg_reader_thm",
  ``apply_reg_reader a b =
    (case a of WithRegs ra fa =>
      case b of WithRegs rb fb =>
        WithRegs (ra ++ rb) (λxs. 
          fa (TAKE (LENGTH ra) xs) (fb (DROP (LENGTH ra) xs))))``,
  CASE_TAC >> CASE_TAC >> rw[apply_reg_reader_def])

val index_find_suc_lemma = prove(
  ``∀xs x x' f n.
      INDEX_FIND n f xs = SOME x' ∧ FST x = SUC (FST x') ∧ SND x = SND x'
    ⇒ INDEX_FIND (SUC n) f xs = SOME x``,
  Induct >> rw[INDEX_FIND_def, PAIR_MAP] >> metis_tac[PAIR, FST, SND])

val index_find_suc_lemma_2 = prove(
  ``∀xs x x' f n.
      INDEX_FIND (SUC n) f xs = SOME x' ∧ FST x' = SUC (FST x) ∧ SND x = SND x'
    ⇒ INDEX_FIND n f xs = SOME x``,
  Induct >> rw[INDEX_FIND_def, PAIR_MAP] >> fs[] >> metis_tac[PAIR, FST, SND])

val index_of_lemma = prove(
  ``∀pre x. MEM x pre ⇒ ∃y. ∀suf. INDEX_OF x (pre ++ suf) = SOME y``,
  Induct >- rw[INDEX_OF_def, INDEX_FIND_def] >>
  ntac 2 strip_tac >> Cases_on `x = h` >> fs[INDEX_OF_def, INDEX_FIND_def] >>
  metis_tac[index_find_suc_lemma, PAIR, FST, SND, ONE])

val offset_and_size_lemma = prove(
  ``∀n pre. n < LENGTH pre
    ⇒ ∃x. ∀suf. offset_and_size (pre ++ suf) n = x``,
  rw[offset_and_size_def, rich_listTheory.EL_APPEND1,
     rich_listTheory.TAKE_APPEND1])

val ssa_map_eq = store_thm("ssa_map_eq",
  ``∀pre s. MEM s (MAP FST pre)
    ⇒ ∀suf1 suf2. ssa_map (pre ++ suf1) s = ssa_map (pre ++ suf2) s``,
  rw[ssa_map_def] >> imp_res_tac index_of_lemma >> fs[offset_and_size_def] >>
  `y < LENGTH pre` suffices_by
    simp[rich_listTheory.EL_APPEND1, rich_listTheory.TAKE_APPEND1] >>
  REPEAT (pop_assum mp_tac) >> qid_spec_tac `y` >> Induct_on `pre` >> rw[] >| [
    fs[INDEX_OF_def, INDEX_FIND_def],

    Cases_on `s = FST h` >> fs[INDEX_OF_def, INDEX_FIND_def] >>
    Cases_on `y` >- fs[INDEX_FIND_def] >> `n < LENGTH pre` suffices_by simp[] >>
    metis_tac[index_find_suc_lemma_2, PAIR, FST, SND, ONE]
  ])

val ssa_map_prefix = store_thm("ssa_map_prefix",
  ``∀pre s. MEM s (MAP FST pre)
    ⇒ ∀suf. ssa_map (pre ++ suf) s = ssa_map pre s``,
  metis_tac[APPEND_EQ_SELF, ssa_map_eq])

val get_ssa_map_prefix = store_thm("get_ssa_map_prefix",
  ``∀pre s. MEM s (MAP FST pre)
    ⇒ ∀suf. get (ssa_map (pre ++ suf)) s = get (ssa_map pre) s``,
  rw[get_def, ssa_regs_def, UNCURRY] >> metis_tac[APPEND_EQ_SELF, ssa_map_eq])

local
  val add_length_lemma = prove(
    ``∀xs f n. (∀ys. f (TAKE (LENGTH xs) ys) = f ys)
               ⇒ ∀ys. f (TAKE (LENGTH xs + n) ys) = f ys``,
    REPEAT strip_tac >>
    Cases_on `LENGTH ys ≤ LENGTH xs` >| [
      simp[TAKE_LENGTH_TOO_LONG],
    
      `TAKE (LENGTH xs) (TAKE (LENGTH xs + n) ys) = TAKE (LENGTH xs) ys`
        suffices_by metis_tac[] >>
      simp[LENGTH_TAKE, TAKE_SUM, TAKE_APPEND1, TAKE_LENGTH_ID_rwt]
    ])

  val drop_take_lemma = prove(
    ``∀xs f n. (∀ys. f (TAKE (LENGTH xs) ys) = f ys)
               ⇒ ∀ys. f (DROP n (TAKE (n + LENGTH xs) ys)) = f (DROP n ys)``,
    REPEAT strip_tac >>
    Cases_on `LENGTH ys ≤ n` >| [
      simp[TAKE_LENGTH_TOO_LONG],
      
      `∀x. DROP n (TAKE n ys ++ x) = x` suffices_by simp[TAKE_SUM] >>
      `LENGTH (TAKE n ys) = n` suffices_by
        metis_tac[prove(``∀a. DROP (LENGTH a) (a++x) = x``, Induct >> fs[])] >>
      simp[]
    ])

  val drop_drop_lemma = prove(
    ``∀xs n n'. DROP n (DROP n' xs) = DROP (n + n') xs``,
    rw[] >> Cases_on `n ≤ LENGTH xs` >| [
      Cases_on `n + n' ≤ LENGTH xs` >>
      simp[rich_listTheory.DROP_DROP, DROP_LENGTH_TOO_LONG],

      simp[DROP_LENGTH_TOO_LONG]
    ])
in
  val pure_reg_reader_ok = store_thm("pure_reg_reader_ok[simp]",
    ``∀x. reg_reader_ok (pure x)``,
    rw[reg_reader_ok_def])

  val apply_reg_reader_ok = store_thm("apply_reg_reader_ok[simp]",
    ``∀a b. reg_reader_ok a ∧ reg_reader_ok b ⇒ reg_reader_ok (a <*> b)``,
    ntac 2 Cases >> rw[reg_reader_ok_def, apply_reg_reader_def] >>
    simp[add_length_lemma, drop_take_lemma])

  val lift_reg_reader_ok = store_thm("lift_reg_reader_ok[simp]",
    ``∀f x. reg_reader_ok x ⇒ reg_reader_ok (f <$> x)``,
    rw[lift_reg_reader_def])

  val get_ok = store_thm("get_ok",
    ``∀v ssa. EVERY reg_reader_ok (get ssa v)``,
    rw[get_def, EVERY_MEM, MEM_MAP] >> simp[reg_reader_ok_def] >> Cases >> rw[])

  val getc_ok = store_thm("getc_ok",
    ``∀v ssa. EVERY reg_reader_ok (getc ssa v)``,
    Cases >> rw[getc_def, get_ok] >> rw[EVERY_MEM, MEM_MAP] >> simp[])
end

val map_opn_regs_id = store_thm("map_opn_regs_id",
  ``map_opn_regs I = I``,
  rw[FUN_EQ_THM] >> Cases_on `x` >> TRY (Cases_on `r`) >>
  rw[map_opn_regs_def, map_reader_regs_def, definition "lift_reg_reader_def",
     ap_left_def, join_left_def, lift_left_def, o_DEF, bind_left_case,
     apply_reg_reader_def, FUN_EQ_THM] >> multi_case_tac >>
  match_mp_tac LIST_EQ >> rw[EL_MAP, PAIR_MAP])

val run_reg_reader_same_lookup = store_thm("run_reg_reader_same_lookup",
  ``∀rdr regs regs'.
      (∀r. lookup r regs = lookup r regs')
    ⇒ run_reg_reader rdr regs = run_reg_reader rdr regs'``,
  Cases >> rw[run_reg_reader_def])

val run_reg_reader_reads_reg_lookup_eq = store_thm(
  "run_reg_reader_reads_reg_lookup_eq",
  ``∀rdr regs regs'.
      (∀r. reads_reg r rdr ⇒
           OPTION_MAP FST (lookup r regs) = OPTION_MAP FST (lookup r regs'))
    ⇒ run_reg_reader rdr regs = run_reg_reader rdr regs'``,
  Cases >>
  rw[reads_reg_def, run_reg_reader_def, o_DEF, lift_left_def, mapM_left_def,
     sequence_left_def, rich_listTheory.FOLDR_MAP] >>
  qid_spec_tac `f` >> Induct_on `l` >> rw[] >> fs[bind_left_monad_assoc])

val map_reader_regs_equiv_renamed = store_thm("map_reader_regs_equiv_renamed",
  ``∀rdr reg_map regs regs'.
      equiv_renamed_spt {r | reads_reg r rdr} reg_map regs regs'
    ⇒ run_reg_reader rdr regs =
      run_reg_reader (map_reader_regs reg_map rdr) regs'``,
  Cases >>
  rw[equiv_renamed_spt_def, run_reg_reader_def, map_reader_regs_def, o_DEF,
     reads_reg_def, lift_left_def, mapM_left_def, sequence_left_def,
     rich_listTheory.FOLDR_MAP] >>
  qid_spec_tac `f` >> Induct_on `l` >> rw[] >> fs[bind_left_monad_assoc])

(*
val partition_compile_block_insert = store_thm("partition_compile_block_insert",
  ``∀block lbl inst n.
      n < LENGTH block.body ⇒
      ∃pre ins post reg_map.
        let insert_inst is = TAKE n is ++ [inst] ++ DROP n is in
        (compile_block lbl block).opns = pre ++ post
      ∧ (compile_block lbl (block with body updated_by insert_inst)).opns =
          pre ++ ins ++ map_reader_regs reg_map post
      ∧ equiv_renamed_spt ($> (LENGTH (compile_block lbl block).regs)) reg_map
          (compile_block lbl block).regs
          (compile_block lbl (block with body updated_by insert_inst)).regs
  ``
*)

(*
val reads_reg_map_reader_regs = store_thm("reads_reg_map_reader_regs",
  ``∀rdr reg_map.
      INJ reg_map {r | reads_reg r rdr} univ(:num)
    ⇒ ∀r. reads_reg r rdr = reads_reg (reg_map r) (map_reader_regs reg_map rdr)``,
  Cases >>
  rw[reads_reg_def, map_reader_regs_def, o_DEF, run_reg_reader_def, INJ_DEF,
     MEM_MAP] >> EQ_TAC >> rw[]
  EQ_TAC >> rw[lookup_insert] >| [
    `∃regs'. ∀n. lookup (reg_map n) regs' = lookup n regs` by cheat >>
    map_every qexists_tac [`regs'`, `c`] >> simp[] >>
    `∀x. (if reg_map x = reg_map r then return c else lookup x regs)
       = (if x = r then return c else lookup x regs)` suffices_by prove_tac[]
    qexists_tac `fromAList (MAP (reg_map ## I) (toAList regs))` >>
    qexists_tac `c` >> rw[lookup_fromAList, ALOOKUP_MAP]
  ]
  
  EQ_TAC >>
  rw[]
  metis_tac[]
*)

val _ = export_theory()

