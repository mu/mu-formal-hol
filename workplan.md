# 2016-09-01

## Short Term

1. *finish memory messages* ✔
2. *turn currently aliased (with `type_abbrev`) types into genuine single-constructor types to stop confusing error messages and type confusions* ✔
3. *refactor code* ✔
4. *memory model to mimic threads in possessing outgoing mailboxes, and allowing for non-instantaneous responses to threads* ✔
5. *check that tail-recursive and iterative implementations of factorial work (in the tests/ directory)* ✔
6. *test a program that updates a global cell* ✔

## Longer Term

1. Write Masters report
    - portfolio (due 31 October?)
        - see below for this
2. Create a multi-threaded semantics
    - create a TSO memory model describing the memory component
    - create a top-level “system” model that unifies threads and the memory and arranges for passing of messages between them
3. Write test programs exercising concurrency
    - pure non-determinism
    - use locks etc to write deterministic concurrent test-case
4. Prove simple optimisations. E.g.:
    - purely memory-independent, e.g.: dead SSA-variable elimination
    - simple memory-related:
        - load-store movement, redundant load and store elimination
