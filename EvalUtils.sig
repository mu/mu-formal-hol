open HolKernel

signature EvalUtils =
sig
  val add_extra_eval_thms : unit -> unit

  val to_env : term -> term

  val start_at : string -> term frag list -> term -> term

  val run_schedule : term frag list -> term -> term

  val run_finite_schedule : term frag list -> term -> term

  val expect_result : term frag list -> term -> thm

  val expect_global_value : string -> term frag list -> term -> thm

  val take_step : term frag list -> term -> term

  val take_steps : term frag list -> term -> term
end

